package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.Brand;
import com.bancopichincha.credito.automotriz.util.BrandUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.concurrent.atomic.AtomicInteger;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@DataJpaTest
public class BrandRepositoryTests {
    private static final AtomicInteger brandCount = new AtomicInteger(0);
    @Autowired
    private BrandRepository brandRepository;

    private String generateBrandName() {
        return String.format("Marca test %d", brandCount.incrementAndGet());
    }

    private Brand getBrand() {
        return BrandUtil.of(generateBrandName());
    }

    private Brand createBrand(final Brand brand) {
        return assertDoesNotThrow(() -> brandRepository.save(brand));
    }

    private Brand findBrandById(final Integer id) {
        return brandRepository.findById(id).orElse(null);
    }

    @Test
    public void givenBrandWhenSaveBrandThenBrandIsFound() {
        final Brand brandSaved = createBrand(getBrand());
        final Brand brandFound = findBrandById(brandSaved.getId());
        assertThat(brandFound, notNullValue());
    }

    @Test
    void shouldCreateBrandWhenSave() {
        final Brand brandSaved = createBrand(getBrand());
        assertThat(brandSaved.getId(), greaterThan(0));
    }

    @Test
    void givenBrandWhenUpdateBrandThenFoundBrandIsEqual() {
        final Brand brandSaved = createBrand(getBrand());
        brandSaved.setName(brandSaved.getName() + "1");
        assertDoesNotThrow(() -> brandRepository.save(brandSaved));
        final Brand brandFound = findBrandById(brandSaved.getId());
        assertThat(brandFound, notNullValue());
        assertThat(brandFound.getId(), is(brandFound.getId()));
        assertThat(brandFound.getName(), is(brandFound.getName()));
    }

    @Test
    public void givenBrandWhenDeleteBrandThenBrandIsNotFound() {
        final Brand brandSaved = createBrand(getBrand());
        assertDoesNotThrow(() -> brandRepository.deleteById(brandSaved.getId()));
        assertThat(findBrandById(brandSaved.getId()), nullValue());
    }
}
