package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.Brand;
import com.bancopichincha.credito.automotriz.domain.Vehicle;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@DataJpaTest
public class VehicleRepositoryTests {
    private static final AtomicInteger plateCount = new AtomicInteger(0);
    private static Brand vehicleBrand;
    @Autowired
    private VehicleRepository vehicleRepository;
    @Autowired
    private BrandRepository brandRepository;

    private String generatePlate() {
        return String.format("XAW-%04d", plateCount.incrementAndGet());
    }

    private Vehicle getVehicle() {
        final Vehicle vehicle = new Vehicle();
        vehicle.setPlate(generatePlate());
        vehicle.setModel("Rav-4");
        vehicle.setChassisNumber("123445");
        vehicle.setType("SUV");
        vehicle.setCylinderCapacity(BigDecimal.valueOf(2.0));
        vehicle.setAppraisal(23000);
        vehicle.setBrand(vehicleBrand);
        return vehicle;
    }

    private Vehicle createVehicle(final Vehicle vehicle) {
        return assertDoesNotThrow(() -> vehicleRepository.save(vehicle));
    }

    private Vehicle findVehicleById(final Integer id) {
        return vehicleRepository.findById(id).orElse(null);
    }

    @BeforeAll
    public static void setup(@Autowired final BrandRepository brandRepository) {
        final Brand brand = new Brand();
        brand.setName("Marca test");
        vehicleBrand = brandRepository.save(brand);
    }

    @Test
    public void givenVehicleWhenSaveVehicleThenVehicleIsFound() {
        final Vehicle vehicleSaved = createVehicle(getVehicle());
        final Vehicle vehicleFound = findVehicleById(vehicleSaved.getId());
        assertThat(vehicleFound, notNullValue());
    }

    @Test
    public void givenVehicleWhenSaveVehicleThenVehicleIsFoundByPlate() {
        final Vehicle vehicleSaved = createVehicle(getVehicle());
        final Vehicle vehicleFound = vehicleRepository.findByPlate(vehicleSaved.getPlate()).orElse(null);
        assertThat(vehicleFound, notNullValue());
    }

    @Test
    public void givenVehicleWhenSaveVehicleThenVehicleIsFoundByModel() {
        final Vehicle vehicle = getVehicle();
        final Vehicle vehiclesSaved = createVehicle(vehicle);
        final List<Vehicle> vehiclesFound = vehicleRepository.findByModel(vehicle.getModel());
        assertThat(vehiclesFound, notNullValue());
        assertThat(
            vehiclesFound.stream().filter(vehiclesSaved::equals).findFirst().orElse(null),
            notNullValue()
        );
    }

    @Test
    public void givenVehicleWhenSaveVehicleThenVehicleIsFoundByBrandId() {
        final Vehicle vehicle = getVehicle();
        final Vehicle vehiclesSaved = createVehicle(vehicle);
        final List<Vehicle> vehiclesFound = vehicleRepository.findByBrandId(vehicle.getBrand().getId());
        assertThat(vehiclesFound, notNullValue());
        assertThat(
            vehiclesFound.stream().filter(vehiclesSaved::equals).findFirst().orElse(null),
            notNullValue()
        );
    }

    @Test
    void shouldCreateVehicleWhenSave() {
        final Vehicle vehicleSaved = createVehicle(getVehicle());
        assertThat(vehicleSaved.getId(), greaterThan(0));
    }

    @Test
    void givenVehicleWhenUpdateVehicleThenFoundVehicleIsEqual() {
        final Vehicle vehicleSaved = createVehicle(getVehicle());
        vehicleSaved.setModel(vehicleSaved.getModel() + "1");
        vehicleSaved.setChassisNumber(vehicleSaved.getChassisNumber() + "1");
        vehicleSaved.setType(vehicleSaved.getType() + "1");
        vehicleSaved.setCylinderCapacity(vehicleSaved.getCylinderCapacity().add(BigDecimal.ONE));
        vehicleSaved.setAppraisal(vehicleSaved.getAppraisal() + 1);
        assertDoesNotThrow(() -> vehicleRepository.save(vehicleSaved));
        final Vehicle vehicleFound = findVehicleById(vehicleSaved.getId());
        assertThat(vehicleFound, notNullValue());
        assertThat(vehicleFound.getId(), is(vehicleFound.getId()));
        assertThat(vehicleFound.getPlate(), is(vehicleFound.getPlate()));
        assertThat(vehicleFound.getModel(), is(vehicleFound.getModel()));
        assertThat(vehicleFound.getChassisNumber(), is(vehicleFound.getChassisNumber()));
        assertThat(vehicleFound.getType(), is(vehicleFound.getType()));
        assertThat(vehicleFound.getCylinderCapacity(), is(vehicleFound.getCylinderCapacity()));
        assertThat(vehicleFound.getAppraisal(), is(vehicleFound.getAppraisal()));
        assertThat(vehicleFound.getBrand(), is(vehicleFound.getBrand()));
    }

    @Test
    public void givenVehicleWhenDeleteVehicleThenVehicleIsNotFound() {
        final Vehicle vehicleSaved = createVehicle(getVehicle());
        assertDoesNotThrow(() -> vehicleRepository.deleteById(vehicleSaved.getId()));
        assertThat(findVehicleById(vehicleSaved.getId()), nullValue());
    }
}
