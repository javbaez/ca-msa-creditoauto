package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.CarDealership;
import com.bancopichincha.credito.automotriz.domain.Customer;
import com.bancopichincha.credito.automotriz.domain.CustomerAssignment;
import com.bancopichincha.credito.automotriz.util.CarDealershipUtil;
import com.bancopichincha.credito.automotriz.util.CustomerUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.concurrent.atomic.AtomicInteger;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@DataJpaTest
public class CustomerAssignmentRepositoryTests {
    private static final AtomicInteger customerCount = new AtomicInteger(0);
    @Autowired
    private CustomerAssignmentRepository customerAssignmentRepository;
    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private CarDealershipRepository carDealershipRepository;

    private CustomerAssignment getCustomerAssignment(final Customer customer, final CarDealership carDealership) {
        final CustomerAssignment customerAssignment = new CustomerAssignment();
        customerAssignment.setCustomer(customer);
        customerAssignment.setCarDealership(carDealership);
        customerAssignment.setAssignmentDate(LocalDate.now());
        return customerAssignment;
    }

    private CustomerAssignment createCustomerAssignment(final CustomerAssignment customerAssignment) {
        return assertDoesNotThrow(() -> customerAssignmentRepository.save(customerAssignment));
    }

    private CustomerAssignment findCustomerAssignmentById(final Integer id) {
        return customerAssignmentRepository.findById(id).orElse(null);
    }

    private String generateCustomerIdNumber() {
        return String.format("2%09d", customerCount.incrementAndGet());
    }

    private Customer createCustomer() {
        return assertDoesNotThrow(() -> customerRepository.save(CustomerUtil.of(generateCustomerIdNumber())));
    }

    private CarDealership createCarDealership() {
        return assertDoesNotThrow(() -> carDealershipRepository.save(CarDealershipUtil.of()));
    }

    @Transactional
    @Test
    public void givenCustomerAssignmentWhenSaveCustomerAssignmentThenCustomerAssignmentIsFound() {
        final CustomerAssignment customerAssignmentSaved = createCustomerAssignment(getCustomerAssignment(
            createCustomer(),
            createCarDealership()
        ));
        final CustomerAssignment customerAssignmentFound = findCustomerAssignmentById(customerAssignmentSaved.getId());
        assertThat(customerAssignmentFound, notNullValue());
    }

    @Transactional
    @Test
    public void givenCustomerAssignmentWhenSaveCustomerAssignmentThenCheckIfCarDealershipHasAssignments() {
        final CustomerAssignment customerAssignmentSaved = createCustomerAssignment(getCustomerAssignment(
            createCustomer(),
            createCarDealership()
        ));
        assertThat(
            customerAssignmentRepository.checkIfCarDealershipHasAssignments(customerAssignmentSaved.getCarDealership().getId()),
            is(true)
        );
    }

    @Transactional
    @Test
    void shouldCreateCustomerAssignmentWhenSave() {
        final CustomerAssignment customerAssignmentSaved = createCustomerAssignment(getCustomerAssignment(
            createCustomer(),
            createCarDealership()
        ));
        assertThat(customerAssignmentSaved.getId(), greaterThan(0));
    }

    @Transactional
    @Test
    void givenCustomerAssignmentWhenUpdateCustomerAssignmentThenFoundCustomerAssignmentIsEqual() {
        final CustomerAssignment customerAssignmentSaved = createCustomerAssignment(getCustomerAssignment(
            createCustomer(),
            createCarDealership()
        ));
        customerAssignmentSaved.setCustomer(createCustomer());
        customerAssignmentSaved.setCarDealership(createCarDealership());
        customerAssignmentSaved.setAssignmentDate(customerAssignmentSaved.getAssignmentDate().plusDays(1));
        assertDoesNotThrow(() -> customerAssignmentRepository.save(customerAssignmentSaved));
        final CustomerAssignment customerAssignmentFound = findCustomerAssignmentById(customerAssignmentSaved.getId());
        assertThat(customerAssignmentFound, notNullValue());
        assertThat(customerAssignmentFound.getId(), is(customerAssignmentSaved.getId()));
        assertThat(customerAssignmentFound.getCustomer(), is(customerAssignmentSaved.getCustomer()));
        assertThat(customerAssignmentFound.getCarDealership(), is(customerAssignmentSaved.getCarDealership()));
        assertThat(customerAssignmentFound.getAssignmentDate(), is(customerAssignmentSaved.getAssignmentDate()));
    }

    @Transactional
    @Test
    public void givenCustomerAssignmentWhenDeleteCustomerAssignmentThenCustomerAssignmentIsNotFound() {
        final CustomerAssignment customerAssignmentSaved = createCustomerAssignment(getCustomerAssignment(
            createCustomer(),
            createCarDealership()
        ));
        assertDoesNotThrow(() -> customerAssignmentRepository.deleteById(customerAssignmentSaved.getId()));
        assertThat(findCustomerAssignmentById(customerAssignmentSaved.getId()), nullValue());
    }
}
