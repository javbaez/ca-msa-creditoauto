package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.Customer;
import com.bancopichincha.credito.automotriz.util.CustomerUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.concurrent.atomic.AtomicInteger;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@DataJpaTest
public class CustomerRepositoryTests {
    private static final AtomicInteger customerCount = new AtomicInteger(0);
    @Autowired
    private CustomerRepository customerRepository;

    private String generateIdNumber() {
        return String.format("1%09d", customerCount.incrementAndGet());
    }

    private Customer getCustomer() {
        return CustomerUtil.of(generateIdNumber());
    }

    private Customer createCustomer(final Customer customer) {
        return assertDoesNotThrow(() -> customerRepository.save(customer));
    }

    private Customer findCustomerById(final Integer id) {
        return customerRepository.findById(id).orElse(null);
    }

    @Test
    public void givenCustomerWhenSaveCustomerThenCustomerIsFound() {
        final Customer customerSaved = createCustomer(getCustomer());
        final Customer customerFound = findCustomerById(customerSaved.getId());
        assertThat(customerFound, notNullValue());
    }

    @Test
    void shouldCreateCustomerWhenSave() {
        final Customer customerSaved = createCustomer(getCustomer());
        assertThat(customerSaved.getId(), greaterThan(0));
    }

    @Test
    void givenCustomerWhenUpdateCustomerThenFoundCustomerIsEqual() {
        final Customer customerSaved = createCustomer(getCustomer());
        customerSaved.setNames(customerSaved.getNames() + "1");
        customerSaved.setLastNames(customerSaved.getLastNames() + "1");
        customerSaved.setAge((byte) (customerSaved.getAge() + 1));
        customerSaved.setBirthday(customerSaved.getBirthday().plusDays(1));
        customerSaved.setAddress(customerSaved.getAddress() + "1");
        customerSaved.setPhone(customerSaved.getPhone().substring(0, customerSaved.getPhone().length() - 1) + "1");
        customerSaved.setSpouseIdNumber(customerSaved.getSpouseIdNumber().substring(0, customerSaved.getSpouseIdNumber().length() - 1) + "1");
        customerSaved.setSpouseNames(customerSaved.getSpouseNames() + "1");
        customerSaved.setCreditSubject(!customerSaved.getCreditSubject());
        assertDoesNotThrow(() -> customerRepository.save(customerSaved));
        final Customer customerFound = findCustomerById(customerSaved.getId());
        assertThat(customerFound, notNullValue());
        assertThat(customerFound.getId(), is(customerSaved.getId()));
        assertThat(customerFound.getIdNumber(), is(customerSaved.getIdNumber()));
        assertThat(customerFound.getNames(), is(customerSaved.getNames()));
        assertThat(customerFound.getLastNames(), is(customerSaved.getLastNames()));
        assertThat(customerFound.getAge(), is(customerSaved.getAge()));
        assertThat(customerFound.getBirthday(), is(customerSaved.getBirthday()));
        assertThat(customerFound.getAddress(), is(customerSaved.getAddress()));
        assertThat(customerFound.getPhone(), is(customerSaved.getPhone()));
        assertThat(customerFound.getCivilStatus(), is(customerSaved.getCivilStatus()));
        assertThat(customerFound.getSpouseIdNumber(), is(customerSaved.getSpouseIdNumber()));
        assertThat(customerFound.getSpouseNames(), is(customerSaved.getSpouseNames()));
        assertThat(customerFound.getCreditSubject(), is(customerSaved.getCreditSubject()));
    }

    @Test
    public void givenCarDealershipWhenDeleteCarDealershipThenCarDealershipIsNotFound() {
        final Customer customerSaved = createCustomer(getCustomer());
        assertDoesNotThrow(() -> customerRepository.deleteById(customerSaved.getId()));
        assertThat(findCustomerById(customerSaved.getId()), nullValue());
    }
}
