package com.bancopichincha.credito.automotriz.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.concurrent.atomic.AtomicInteger;

@DataJpaTest
public class LoadApplicationRepositoryTests {
    private static final AtomicInteger brandCount = new AtomicInteger(0);
    @Autowired
    private LoanApplicationRepository loanApplicationRepository;

    /*private String generateBrandName() {
        return String.format("Marca test %d", brandCount.incrementAndGet());
    }

    private Brand getBrand() {
        final Brand brand = new Brand();
        brand.setName(generateBrandName());
        return brand;
    }

    private Brand createBrand(final Brand brand) {
        return assertDoesNotThrow(() -> brandRepository.save(brand));
    }

    private Brand findBrandById(final Integer id) {
        return brandRepository.findById(id).orElse(null);
    }

    @Test
    public void givenBrandWhenSaveBrandThenBrandIsFound() {
        final Brand brandSaved = createBrand(getBrand());
        final Brand brandFound = findBrandById(brandSaved.getId());
        assertThat(brandFound, notNullValue());
    }

    @Test
    void shouldCreateBrandWhenSave() {
        final Brand brandSaved = createBrand(getBrand());
        assertThat(brandSaved.getId(), greaterThan(0));
    }

    @Test
    void givenBrandWhenUpdateBrandThenFoundBrandIsEqual() {
        final Brand brandSaved = createBrand(getBrand());
        brandSaved.setName(brandSaved.getName() + "1");
        assertDoesNotThrow(() -> brandRepository.save(brandSaved));
        final Brand brandFound = findBrandById(brandSaved.getId());
        assertThat(brandFound, notNullValue());
        assertThat(brandFound.getId(), is(brandFound.getId()));
        assertThat(brandFound.getName(), is(brandFound.getName()));
    }

    @Test
    public void givenBrandWhenDeleteBrandThenBrandIsNotFound() {
        final Brand brandSaved = createBrand(getBrand());
        assertDoesNotThrow(() -> brandRepository.deleteById(brandSaved.getId()));
        assertThat(findBrandById(brandSaved.getId()), nullValue());
    }*/
}
