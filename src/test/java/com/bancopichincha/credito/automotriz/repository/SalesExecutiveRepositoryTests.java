package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.CarDealership;
import com.bancopichincha.credito.automotriz.domain.SalesExecutive;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@DataJpaTest
public class SalesExecutiveRepositoryTests {
    private static CarDealership executiveCarDealership;
    @Autowired
    private SalesExecutiveRepository executiveRepository;

    private SalesExecutive getExecutive(final String idNumber) {
        final SalesExecutive salesExecutive = new SalesExecutive();
        salesExecutive.setIdNumber(idNumber);
        salesExecutive.setNames("LUIS");
        salesExecutive.setLastNames("BORJA");
        salesExecutive.setAge((byte) 39);
        salesExecutive.setAddress("AV. REPUBLICA Y AMAZONAS");
        salesExecutive.setLandline("022123456");
        salesExecutive.setMobile("0983123456");
        salesExecutive.setCarDealership(executiveCarDealership);
        return salesExecutive;
    }

    private SalesExecutive createExecutive(final SalesExecutive salesExecutive) {
        return assertDoesNotThrow(() -> executiveRepository.save(salesExecutive));
    }

    private SalesExecutive findExecutiveById(final Integer id) {
        return executiveRepository.findById(id).orElse(null);
    }

    @BeforeAll
    public static void setup(@Autowired final CarDealershipRepository carDealershipRepository) {
        final CarDealership carDealership = new CarDealership();
        carDealership.setName("Patio de autos");
        carDealership.setAddress("Av. 6 de diciembre y Colón");
        carDealership.setPhone("0984171819");
        carDealership.setPointOfSaleNumber(1);
        executiveCarDealership = carDealershipRepository.save(carDealership);
    }

    @Test
    public void givenExecutiveWhenSaveExecutiveThenExecutiveIsFound() {
        final SalesExecutive executiveSaved = createExecutive(getExecutive("0123456789"));
        final SalesExecutive executiveFound = findExecutiveById(executiveSaved.getId());
        assertThat(executiveFound, notNullValue());
        assertThat(executiveFound, is(executiveSaved));
    }

    @Test
    public void givenExecutiveWhenSaveExecutiveThenExecutiveIsFoundByIdAndCarDealership() {
        final SalesExecutive executiveSaved = createExecutive(getExecutive("1234567890"));
        final SalesExecutive executiveFound = executiveRepository
            .findByIdAndCarDealership(executiveSaved.getId(), executiveSaved.getCarDealership()).orElse(null);
        assertThat(executiveFound, notNullValue());
        assertThat(executiveFound, is(executiveSaved));
    }

    @Test
    public void givenExecutiveWhenSaveExecutiveThenExecutiveIsFoundByCarDealership() {
        final SalesExecutive executive = getExecutive("2345678901");
        final SalesExecutive executivesSaved = createExecutive(executive);
        final List<SalesExecutive> executivesFound = executiveRepository.findByCarDealership(executive.getCarDealership());
        assertThat(executivesFound, notNullValue());
        assertThat(
            executivesFound.stream().filter(executivesSaved::equals).findFirst().orElse(null),
            notNullValue()
        );
    }

    @Test
    public void givenExecutiveWhenSaveExecutiveThenCheckIfExistsByCarDealershipId() {
        final SalesExecutive executive = getExecutive("3456789012");
        createExecutive(executive);
        assertThat(
            executiveRepository.checkIfExistsByCarDealershipId(executive.getCarDealership().getId()),
            is(true)
        );
    }

    @Test
    void shouldCreateExecutiveWhenSave() {
        final SalesExecutive executiveSaved = createExecutive(getExecutive("4567890123"));
        assertThat(executiveSaved.getId(), greaterThan(0));
    }

    @Test
    void givenExecutiveWhenUpdateExecutiveThenFoundExecutiveIsEqual() {
        final SalesExecutive executiveSaved = createExecutive(getExecutive("5678901234"));
        executiveSaved.setNames(executiveSaved.getNames() + "1");
        executiveSaved.setLastNames(executiveSaved.getLastNames() + "1");
        executiveSaved.setAge((byte) (executiveSaved.getAge() + 1));
        executiveSaved.setAddress(executiveSaved.getAddress() + "1");
        executiveSaved.setLandline(executiveSaved.getLandline().substring(0, executiveSaved.getLandline().length() - 1) + "1");
        executiveSaved.setMobile(executiveSaved.getMobile().substring(0, executiveSaved.getMobile().length() - 1) + "1");
        assertDoesNotThrow(() -> executiveRepository.save(executiveSaved));
        final SalesExecutive executiveFound = findExecutiveById(executiveSaved.getId());
        assertThat(executiveFound, notNullValue());
        assertThat(executiveFound.getId(), is(executiveFound.getId()));
        assertThat(executiveFound.getIdNumber(), is(executiveFound.getIdNumber()));
        assertThat(executiveFound.getNames(), is(executiveFound.getNames()));
        assertThat(executiveFound.getLastNames(), is(executiveFound.getLastNames()));
        assertThat(executiveFound.getAge(), is(executiveFound.getAge()));
        assertThat(executiveFound.getAddress(), is(executiveFound.getAddress()));
        assertThat(executiveFound.getLandline(), is(executiveFound.getLandline()));
        assertThat(executiveFound.getMobile(), is(executiveFound.getMobile()));
        assertThat(executiveFound.getCarDealership(), is(executiveFound.getCarDealership()));
    }

    @Test
    public void givenExecutiveWhenDeleteExecutiveThenExecutiveIsNotFound() {
        final SalesExecutive executiveSaved = createExecutive(getExecutive("6789012345"));
        assertDoesNotThrow(() -> executiveRepository.deleteById(executiveSaved.getId()));
        assertThat(findExecutiveById(executiveSaved.getId()), nullValue());
    }
}
