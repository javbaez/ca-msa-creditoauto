package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.CarDealership;
import com.bancopichincha.credito.automotriz.util.CarDealershipUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

@DataJpaTest
public class CarDealershipRepositoryTests {
    @Autowired
    private CarDealershipRepository carDealershipRepository;

    private CarDealership getCarDealership() {
        return CarDealershipUtil.of();
    }

    private CarDealership createCarDealership(final CarDealership carDealership) {
        return assertDoesNotThrow(() -> carDealershipRepository.save(carDealership));
    }

    private CarDealership findCarDealershipById(final Integer id) {
        return carDealershipRepository.findById(id).orElse(null);
    }

    @Test
    public void givenCarDealershipWhenSaveCarDealershipThenCarDealershipIsFound() {
        final CarDealership carDealershipSaved = createCarDealership(getCarDealership());
        final CarDealership carDealershipFound = findCarDealershipById(carDealershipSaved.getId());
        assertThat(carDealershipFound, notNullValue());
    }

    @Test
    public void givenCarDealershipWhenSaveCarDealershipThenCarDealershipIsFoundByIdIn() {
        final CarDealership carDealershipSaved = createCarDealership(getCarDealership());
        final List<CarDealership> carDealershipsFound = carDealershipRepository.findByIdIn(List.of(carDealershipSaved.getId()));
        assertThat(carDealershipsFound, notNullValue());
        assertThat(carDealershipsFound.size(), is(1));
        assertThat(carDealershipsFound.get(0), is(carDealershipSaved));
    }

    @Test
    public void givenCarDealershipWhenSaveCarDealershipThenCarDealershipIsFoundByIdInMap() {
        final CarDealership carDealershipSaved = createCarDealership(getCarDealership());
        final Map<Integer, CarDealership> carDealershipsFound = carDealershipRepository.findByIdInMap(List.of(carDealershipSaved.getId()));
        assertThat(carDealershipsFound, notNullValue());
        assertThat(carDealershipsFound.size(), is(1));
        assertThat(carDealershipsFound.containsKey(carDealershipSaved.getId()), is(true));
        assertThat(carDealershipsFound.get(carDealershipSaved.getId()), is(carDealershipSaved));
    }

    @Test
    void shouldCreateCarDealershipWhenSave() {
        final CarDealership carDealershipSaved = createCarDealership(getCarDealership());
        assertThat(carDealershipSaved.getId(), greaterThan(0));
    }

    @Test
    void givenCarDealershipWhenUpdateCarDealershipThenFoundCarDealershipIsEqual() {
        final CarDealership carDealershipSaved = createCarDealership(getCarDealership());
        carDealershipSaved.setName(carDealershipSaved.getName() + "1");
        carDealershipSaved.setAddress(carDealershipSaved.getAddress() + "1");
        carDealershipSaved.setPhone(carDealershipSaved.getPhone().substring(0, carDealershipSaved.getPhone().length() - 1) + "1");
        carDealershipSaved.setPointOfSaleNumber(carDealershipSaved.getPointOfSaleNumber() + 1);
        assertDoesNotThrow(() -> carDealershipRepository.save(carDealershipSaved));
        final CarDealership carDealershipFound = findCarDealershipById(carDealershipSaved.getId());
        assertThat(carDealershipFound, notNullValue());
        assertThat(carDealershipSaved.getId(), is(carDealershipFound.getId()));
        assertThat(carDealershipSaved.getName(), is(carDealershipFound.getName()));
        assertThat(carDealershipSaved.getAddress(), is(carDealershipFound.getAddress()));
        assertThat(carDealershipSaved.getPhone(), is(carDealershipFound.getPhone()));
        assertThat(carDealershipSaved.getPointOfSaleNumber(), is(carDealershipFound.getPointOfSaleNumber()));
    }

    @Test
    public void givenCarDealershipWhenDeleteCarDealershipThenCarDealershipIsNotFound() {
        final CarDealership carDealershipSaved = createCarDealership(getCarDealership());
        assertDoesNotThrow(() -> carDealershipRepository.deleteById(carDealershipSaved.getId()));
        assertThat(findCarDealershipById(carDealershipSaved.getId()), nullValue());
    }
}
