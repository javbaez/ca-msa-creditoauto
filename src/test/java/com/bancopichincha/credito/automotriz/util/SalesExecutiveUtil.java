package com.bancopichincha.credito.automotriz.util;

import com.bancopichincha.credito.automotriz.domain.SalesExecutive;
import com.bancopichincha.credito.automotriz.service.dto.SalesExecutiveDto;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import static com.bancopichincha.credito.automotriz.util.CarDealershipUtil.expectCarDealership;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SalesExecutiveUtil {
    public static SalesExecutive of(final String idNumber) {
        return of(null, idNumber);
    }

    public static SalesExecutive of(final Integer id, final String idNumber) {
        final SalesExecutive executive = new SalesExecutive();
        executive.setId(id);
        executive.setIdNumber(idNumber);
        executive.setNames("LUIS");
        executive.setLastNames("PEREZ");
        executive.setAge((byte) 30);
        executive.setAddress("AV. AMAZONAS Y REPUBLICA");
        executive.setLandline("022123456");
        executive.setMobile("0993121314");
        executive.setCarDealership(CarDealershipUtil.of(1));
        return executive;
    }

    public static void expectSalesExecutive(final SalesExecutive salesExecutive, final SalesExecutiveDto salesExecutiveDto) {
        assertThat(salesExecutive.getId(), is(salesExecutiveDto.getId()));
        assertThat(salesExecutive.getIdNumber(), is(salesExecutiveDto.getIdNumber()));
        assertThat(salesExecutive.getNames(), is(salesExecutiveDto.getNames()));
        assertThat(salesExecutive.getLastNames(), is(salesExecutiveDto.getLastNames()));
        assertThat(salesExecutive.getAge(), is(salesExecutiveDto.getAge()));
        assertThat(salesExecutive.getAddress(), is(salesExecutiveDto.getAddress()));
        assertThat(salesExecutive.getLandline(), is(salesExecutiveDto.getLandline()));
        assertThat(salesExecutive.getMobile(), is(salesExecutiveDto.getMobile()));
        expectCarDealership(salesExecutive.getCarDealership(), salesExecutiveDto.getCarDealership());
    }
}
