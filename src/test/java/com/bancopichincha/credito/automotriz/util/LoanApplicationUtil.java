package com.bancopichincha.credito.automotriz.util;

import com.bancopichincha.credito.automotriz.domain.LoanApplication;
import com.bancopichincha.credito.automotriz.domain.enums.LoanStatus;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDate;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class LoanApplicationUtil {
    public static LoanApplication of(final Integer id) {
        final LoanApplication loanApplication = new LoanApplication();
        loanApplication.setId(id);
        loanApplication.setDate(LocalDate.now());
        loanApplication.setCustomer(CustomerUtil.of(1, "1000000000"));
        loanApplication.setCarDealership(CarDealershipUtil.of(1));
        loanApplication.setSalesExecutive(SalesExecutiveUtil.of(1, "2000000000"));
        loanApplication.setVehicle(VehicleUtil.of(1, "ABC-1234"));
        loanApplication.setMonthsTerm((byte) 12);
        loanApplication.setInstallments((short) 36);
        loanApplication.setDownPayment(BigDecimal.valueOf(10_000));
        loanApplication.setStatus(LoanStatus.REGISTERED);
        return loanApplication;
    }
}
