package com.bancopichincha.credito.automotriz.util;

import com.bancopichincha.credito.automotriz.domain.Brand;
import com.bancopichincha.credito.automotriz.service.dto.BrandDto;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class BrandUtil {
    public static Brand of(final String name) {
        return of(null, name);
    }

    public static Brand of(final Integer id, final String name) {
        final Brand brand = new Brand();
        brand.setId(id);
        brand.setName(name);
        return brand;
    }

    public static void expectBrand(final Brand brand, final BrandDto brandDto) {
        assertThat(brand.getId(), is(brandDto.getId()));
        assertThat(brand.getName(), is(brandDto.getName()));
    }
}
