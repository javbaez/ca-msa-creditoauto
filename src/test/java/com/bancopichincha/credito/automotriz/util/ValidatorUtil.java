package com.bancopichincha.credito.automotriz.util;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ValidatorUtil {
    /**
     * Validates object.
     * @param entry Entry object.
     * @param groups Validation groups.
     * @return List of errors.
     */
    public static List<ConstraintViolation<Object>> validate(final Object entry, final Class<?>... groups) {
        try (final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory()) {
            final Validator validator = validatorFactory.getValidator();
            final Set<ConstraintViolation<Object>> violations = validator.validate(entry, groups);
            if (!violations.isEmpty()) {
                violations.forEach(v -> System.err.println("v=" + v.getPropertyPath() + "," + v.getMessage()));
                return violations.stream().toList();
            }
            return List.of();
        }
    }
}
