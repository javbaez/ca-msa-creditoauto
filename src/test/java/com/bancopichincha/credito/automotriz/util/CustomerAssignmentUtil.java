package com.bancopichincha.credito.automotriz.util;

import com.bancopichincha.credito.automotriz.domain.CustomerAssignment;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CustomerAssignmentUtil {
    public static CustomerAssignment of(final Integer id) {
        final CustomerAssignment customerAssignment = new CustomerAssignment();
        customerAssignment.setId(id);
        customerAssignment.setCustomer(CustomerUtil.of(1, "1000000000"));
        customerAssignment.setCarDealership(CarDealershipUtil.of(1));
        customerAssignment.setAssignmentDate(LocalDate.now());
        return customerAssignment;
    }
}
