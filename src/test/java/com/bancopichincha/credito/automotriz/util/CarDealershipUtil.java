package com.bancopichincha.credito.automotriz.util;

import com.bancopichincha.credito.automotriz.domain.CarDealership;
import com.bancopichincha.credito.automotriz.service.dto.CarDealershipDto;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CarDealershipUtil {
    public static CarDealership of() {
        return of(null);
    }

    public static CarDealership of(final Integer id) {
        final CarDealership carDealership = new CarDealership();
        carDealership.setId(id);
        carDealership.setName("Patio de autos");
        carDealership.setAddress("Av. 6 de diciembre y Colón");
        carDealership.setPhone("0984171819");
        carDealership.setPointOfSaleNumber(1);
        return carDealership;
    }

    public static void expectCarDealership(final CarDealership carDealership, final CarDealershipDto carDealershipDto) {
        assertThat(carDealership.getId(), is(carDealershipDto.getId()));
        assertThat(carDealership.getName(), is(carDealershipDto.getName()));
        assertThat(carDealership.getAddress(), is(carDealershipDto.getAddress()));
        assertThat(carDealership.getPhone(), is(carDealershipDto.getPhone()));
        assertThat(carDealership.getPointOfSaleNumber(), is(carDealershipDto.getPointOfSaleNumber()));
    }
}
