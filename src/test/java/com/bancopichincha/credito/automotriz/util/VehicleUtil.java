package com.bancopichincha.credito.automotriz.util;

import com.bancopichincha.credito.automotriz.domain.Vehicle;
import com.bancopichincha.credito.automotriz.service.dto.VehicleDto;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

import static com.bancopichincha.credito.automotriz.util.BrandUtil.expectBrand;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class VehicleUtil {
    public static Vehicle of(final String plate) {
        return of(null, plate);
    }

    public static Vehicle of(final Integer id, final String plate) {
        final Vehicle vehicle = new Vehicle();
        vehicle.setId(id);
        vehicle.setPlate(plate);
        vehicle.setModel("Rav-4");
        vehicle.setChassisNumber("123445");
        vehicle.setType("SUV");
        vehicle.setCylinderCapacity(BigDecimal.valueOf(2.0));
        vehicle.setAppraisal(23000);
        vehicle.setBrand(BrandUtil.of(1, "Marca test"));
        return vehicle;
    }

    public static void expectVehicle(final Vehicle vehicle, final VehicleDto vehicleDto) {
        assertThat(vehicle.getId(), is(vehicleDto.getId()));
        assertThat(vehicle.getPlate(), is(vehicleDto.getPlate()));
        assertThat(vehicle.getModel(), is(vehicleDto.getModel()));
        assertThat(vehicle.getChassisNumber(), is(vehicleDto.getChassisNumber()));
        assertThat(vehicle.getType(), is(vehicleDto.getType()));
        assertThat(vehicle.getCylinderCapacity(), is(vehicleDto.getCylinderCapacity()));
        assertThat(vehicle.getAppraisal(), is(vehicleDto.getAppraisal()));
        assertThat(vehicle.getId(), is(vehicleDto.getId()));
        expectBrand(vehicle.getBrand(), vehicleDto.getBrand());
    }
}
