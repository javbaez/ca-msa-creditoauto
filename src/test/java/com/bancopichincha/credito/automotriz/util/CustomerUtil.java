package com.bancopichincha.credito.automotriz.util;

import com.bancopichincha.credito.automotriz.domain.Customer;
import com.bancopichincha.credito.automotriz.domain.enums.CivilStatus;
import com.bancopichincha.credito.automotriz.service.dto.CustomerDto;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class CustomerUtil {
    public static Customer of(final String idNumber) {
        return of(null, idNumber);
    }

    public static Customer of(final Integer id, final String idNumber) {
        final Customer customer = new Customer();
        customer.setId(id);
        customer.setIdNumber(idNumber);
        customer.setNames("PEDRO");
        customer.setLastNames("MARTINEZ");
        customer.setAge((byte) 18);
        customer.setBirthday(LocalDate.of(2005, 1, 1));
        customer.setAddress("GONZALEZ SUAREZ Y CORUÑA");
        customer.setPhone("0990543210");
        customer.setCivilStatus(CivilStatus.MARRIED);
        customer.setSpouseIdNumber("1309742417");
        customer.setSpouseNames("LUISA DELGADO");
        customer.setCreditSubject(true);
        return customer;
    }

    public static void expectCustomer(final Customer customer, final CustomerDto customerDto) {
        assertThat(customer.getId(), is(customerDto.getId()));
        assertThat(customer.getIdNumber(), is(customerDto.getIdNumber()));
        assertThat(customer.getNames(), is(customerDto.getNames()));
        assertThat(customer.getLastNames(), is(customerDto.getLastNames()));
        assertThat(customer.getAge(), is(customerDto.getAge()));
        assertThat(customer.getBirthday(), is(customerDto.getBirthday()));
        assertThat(customer.getAddress(), is(customerDto.getAddress()));
        assertThat(customer.getPhone(), is(customerDto.getPhone()));
        assertThat(customer.getCivilStatus(), is(customerDto.getCivilStatus()));
        assertThat(customer.getSpouseIdNumber(), is(customerDto.getSpouseIdNumber()));
        assertThat(customer.getSpouseNames(), is(customerDto.getSpouseNames()));
        assertThat(customer.getCreditSubject(), is(customerDto.getCreditSubject()));
    }
}
