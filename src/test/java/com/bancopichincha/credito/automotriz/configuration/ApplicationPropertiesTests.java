package com.bancopichincha.credito.automotriz.configuration;

import com.bancopichincha.credito.automotriz.util.ValidatorUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SpringBootTest
public class ApplicationPropertiesTests {
    @Autowired
    private ApplicationProperties applicationProperties;

    @Test
    public void shouldValidateApplicationProperties() {
        assertThat(ValidatorUtil.validate(applicationProperties).size(), is(0));
    }
}
