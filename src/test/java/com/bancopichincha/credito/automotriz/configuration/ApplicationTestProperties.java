package com.bancopichincha.credito.automotriz.configuration;

import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

@Component
@ConfigurationProperties(prefix = "test")
@Data
@Validated
public class ApplicationTestProperties {
    @NotNull
    private String baseUrl;
}
