package com.bancopichincha.credito.automotriz.configuration;

import com.bancopichincha.credito.automotriz.domain.Brand;
import com.bancopichincha.credito.automotriz.domain.CarDealership;
import com.bancopichincha.credito.automotriz.domain.Customer;
import com.bancopichincha.credito.automotriz.domain.SalesExecutive;
import com.bancopichincha.credito.automotriz.repository.BrandRepository;
import com.bancopichincha.credito.automotriz.repository.CarDealershipRepository;
import com.bancopichincha.credito.automotriz.repository.CustomerRepository;
import com.bancopichincha.credito.automotriz.repository.SalesExecutiveRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.event.ContextRefreshedEvent;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class ApplicationDataLoaderTests {
    @MockBean
    private ContextRefreshedEvent contextRefreshedEvent;
    @MockBean
    private ApplicationProperties appProperties;
    @MockBean
    private BrandRepository brandRepository;
    @MockBean
    private CustomerRepository customerRepository;
    @MockBean
    private CarDealershipRepository carDealershipRepository;
    @MockBean
    private SalesExecutiveRepository salesExecutiveRepository;
    private ApplicationDataLoader applicationDataLoader;
    private final List<Brand> brands = new ArrayList<>();
    private final List<Customer> customers = new ArrayList<>();
    private final List<CarDealership> carDealerships = new ArrayList<>();
    private final List<SalesExecutive> salesExecutives = new ArrayList<>();

    @BeforeEach
    public void setup() {
        final ApplicationProperties.Csv applicationPropertiesCsv = new ApplicationProperties.Csv();
        final ApplicationProperties.Csv.Filenames filenames = new ApplicationProperties.Csv.Filenames();
        applicationPropertiesCsv.setPath("db/migration");
        filenames.setBrands("brands.csv");
        filenames.setCustomers("customers.csv");
        filenames.setCarDealerships("car-dealerships.csv");
        filenames.setSalesExecutives("sales-executives.csv");
        applicationPropertiesCsv.setFilenames(filenames);
        brands.clear();
        customers.clear();
        carDealerships.clear();
        salesExecutives.clear();
        when(appProperties.getCsv()).thenReturn(applicationPropertiesCsv);
        when(brandRepository.count()).thenReturn(0L);
        when(brandRepository.save(any(Brand.class)))
            .then(answer -> {
                final Brand brand = answer.getArgument(0);
                brands.add(brand);
                return brand;
            });
        when(customerRepository.count()).thenReturn(0L);
        when(customerRepository.save(any(Customer.class)))
            .then(answer -> {
                final Customer customer = answer.getArgument(0);
                customers.add(customer);
                return customer;
            });
        when(carDealershipRepository.count()).thenReturn(0L);
        when(carDealershipRepository.save(any(CarDealership.class)))
            .then(answer -> {
                final CarDealership carDealership = answer.getArgument(0);
                carDealerships.add(carDealership);
                return carDealership;
            });
        when(salesExecutiveRepository.count()).thenReturn(0L);
        when(salesExecutiveRepository.save(any(SalesExecutive.class)))
            .then(answer -> {
                final SalesExecutive salesExecutive = answer.getArgument(0);
                salesExecutives.add(salesExecutive);
                return salesExecutive;
            });
        applicationDataLoader = new ApplicationDataLoader(
            appProperties,
            brandRepository,
            customerRepository,
            carDealershipRepository,
            salesExecutiveRepository
        );
    }

    @Test
    public void shouldLoadApplicationData() {
        applicationDataLoader.onApplicationEvent(contextRefreshedEvent);
        assertThat(applicationDataLoader.isAlreadySetup(), is(true));
        assertThat(brands.size(), is(8));
        assertThat(customers.size(), is(20));
        assertThat(carDealerships.size(), is(10));
        assertThat(salesExecutives.size(), is(20));
    }
}
