package com.bancopichincha.credito.automotriz.service.dto.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import lombok.Getter;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.Mock;

import java.lang.annotation.Annotation;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Getter
abstract class ValidatorTests<A extends Annotation, T, V extends ConstraintValidator<A, T>> {
    private A annotation;
    @Mock
    private ConstraintValidatorContext constraintValidatorContext;
    private V validator;

    protected abstract V constructValidator();

    protected abstract A mockAnnotation();

    @BeforeEach
    public void setup() {
        annotation = mockAnnotation();
        validator = constructValidator();
        validator.initialize(annotation);
    }

    protected void shouldPassValidation(final T value) {
        assertThat(validator.isValid(value, constraintValidatorContext), is(true));
    }

    protected void shouldFailValidation(final T value) {
        assertThat(validator.isValid(value, constraintValidatorContext), is(false));
    }
}
