package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.domain.Vehicle;
import com.bancopichincha.credito.automotriz.exception.*;
import com.bancopichincha.credito.automotriz.repository.BrandRepository;
import com.bancopichincha.credito.automotriz.repository.LoanApplicationRepository;
import com.bancopichincha.credito.automotriz.repository.VehicleRepository;
import com.bancopichincha.credito.automotriz.service.dto.BrandDto;
import com.bancopichincha.credito.automotriz.service.dto.VehicleDto;
import com.bancopichincha.credito.automotriz.service.impl.VehicleServiceImpl;
import com.bancopichincha.credito.automotriz.service.mapper.VehicleMapper;
import com.bancopichincha.credito.automotriz.util.VehicleUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.math.BigDecimal;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class VehicleServiceTests {
    private static final Integer ID = 1;
    private static final String PLATE = "ABC-1234";
    @MockBean
    private VehicleMapper vehicleMapper;
    @MockBean
    private VehicleRepository vehicleRepository;
    @MockBean
    private BrandRepository brandRepository;
    @MockBean
    private LoanApplicationRepository loanApplicationRepository;
    @InjectMocks
    @Autowired
    private VehicleServiceImpl vehicleService;

    private Vehicle getVehicle(final Integer id, final String plate) {
        return VehicleUtil.of(id, plate);
    }

    private VehicleDto getVehicleDto(final Vehicle vehicle) {
        final VehicleDto vehicleDto = new VehicleDto();
        final BrandDto brandDto = new BrandDto();
        vehicleDto.setId(vehicle.getId());
        vehicleDto.setPlate(vehicle.getPlate());
        vehicleDto.setModel(vehicle.getModel());
        vehicleDto.setChassisNumber(vehicle.getChassisNumber());
        vehicleDto.setType(vehicle.getType());
        vehicleDto.setCylinderCapacity(vehicle.getCylinderCapacity());
        vehicleDto.setAppraisal(vehicle.getAppraisal());
        brandDto.setId(vehicle.getBrand().getId());
        brandDto.setName(vehicle.getBrand().getName());
        vehicleDto.setBrand(brandDto);
        return vehicleDto;
    }

    private void expectVehicleDto(final boolean validateId, final VehicleDto inputDto, final VehicleDto resultDto) {
        if (validateId) {
            assertThat(resultDto.getId(), is(inputDto.getId()));
        }
        assertThat(resultDto.getPlate(), is(inputDto.getPlate()));
        assertThat(resultDto.getModel(), is(inputDto.getModel()));
        assertThat(resultDto.getChassisNumber(), is(inputDto.getChassisNumber()));
        assertThat(resultDto.getType(), is(inputDto.getType()));
        assertThat(resultDto.getCylinderCapacity(), is(inputDto.getCylinderCapacity()));
        assertThat(resultDto.getAppraisal(), is(inputDto.getAppraisal()));
        assertThat(resultDto.getBrand().getId(), is(inputDto.getBrand().getId()));
    }

    @Test
    public void shouldFindVehicle() {
        final Vehicle vehicle = getVehicle(ID, PLATE);
        final VehicleDto vehicleDto = getVehicleDto(vehicle);
        when(vehicleMapper.vehicleToVehicleDto(any(Vehicle.class))).thenReturn(vehicleDto);
        when(vehicleRepository.findById(any(Integer.class))).thenReturn(Optional.of(vehicle));
        final VehicleDto vehicleDtoFound = assertDoesNotThrow(() -> vehicleService.findById(vehicle.getId()));
        assertThat(vehicleDtoFound, notNullValue());
        assertThat(vehicleDtoFound.getId(), is(vehicle.getId()));
    }

    private void shouldCreateVehicle(
        final Vehicle vehicle,
        final boolean findBrand,
        final boolean findVehicleByPlate
    ) {
        final VehicleDto vehicleDto = getVehicleDto(vehicle);
        when(vehicleMapper.vehicleDtoToVehicle(any(VehicleDto.class))).thenReturn(vehicle);
        when(vehicleMapper.vehicleToVehicleDto(any(Vehicle.class))).thenReturn(vehicleDto);
        when(brandRepository.findById(any(Integer.class))).thenReturn(
            findBrand ?
                Optional.of(vehicle.getBrand()) :
                Optional.empty()
        );
        when(vehicleRepository.findByPlate(any(String.class))).thenReturn(
            findVehicleByPlate ?
                Optional.of(vehicle) :
                Optional.empty()
        );
        when(vehicleRepository.save(any(Vehicle.class))).thenReturn(vehicle);
        Class<? extends Throwable> exceptionClass = null;
        if (!findBrand) {
            exceptionClass = BrandNotFoundException.class;
        } else if (findVehicleByPlate) {
            exceptionClass = VehiclePlateExistsException.class;
        }
        if (exceptionClass != null) {
            assertThrows(exceptionClass, () -> vehicleService.create(vehicleDto));
        } else {
            final VehicleDto vehicleDtoCreated = assertDoesNotThrow(() -> vehicleService.create(vehicleDto));
            expectVehicleDto(false, vehicleDto, vehicleDtoCreated);
        }
    }

    @Test
    public void shouldCreateVehicle() {
        shouldCreateVehicle(getVehicle(null, PLATE), true, false);
    }

    @Test
    public void shouldThrowBrandNotFoundExceptionWhenCreateVehicle() {
        shouldCreateVehicle(getVehicle(null, PLATE), false, true);
    }

    @Test
    public void shouldThrowVehiclePlateExistsExceptionWhenCreateVehicle() {
        shouldCreateVehicle(getVehicle(null, PLATE), true, false);
    }

    private void shouldUpdateVehicle(
        final Vehicle vehicle,
        final boolean findVehicle,
        final boolean findBrand,
        final boolean checkIfVehicleIsReserved,
        final boolean findVehicleByPlate
    ) {
        final VehicleDto vehicleDto = getVehicleDto(vehicle);
        vehicleDto.setPlate(vehicleDto.getPlate().substring(0, vehicleDto.getPlate().length() - 1) + "1");
        vehicleDto.setModel(vehicleDto.getModel() + "1");
        vehicleDto.setChassisNumber(vehicleDto.getChassisNumber() + "1");
        vehicleDto.setCylinderCapacity(vehicleDto.getCylinderCapacity().add(BigDecimal.ONE));
        vehicleDto.setAppraisal(vehicleDto.getAppraisal() + 1);
        vehicleDto.getBrand().setId(vehicleDto.getBrand().getId() + 1);
        when(vehicleMapper.vehicleToVehicleDto(any(Vehicle.class))).thenReturn(vehicleDto);
        doNothing().when(vehicleMapper)
            .updateVehicleFromVehicleDto(any(Vehicle.class), any(VehicleDto.class));
        when(vehicleRepository.findById(any(Integer.class))).thenReturn(
            findVehicle ?
                Optional.of(vehicle) :
                Optional.empty()
        );
        when(brandRepository.findById(any(Integer.class))).thenReturn(
            findBrand ?
                Optional.of(vehicle.getBrand()) :
                Optional.empty()
        );
        when(loanApplicationRepository.checkIfVehicleIsReserved(any(Integer.class)))
            .thenReturn(checkIfVehicleIsReserved);
        when(vehicleRepository.findByPlate(any(String.class))).thenReturn(
            findVehicleByPlate ?
                Optional.of(getVehicle(2, "XYZ-1234")) :
                Optional.empty()
        );
        when(vehicleRepository.save(any(Vehicle.class))).thenReturn(vehicle);
        Class<? extends Throwable> exceptionClass = null;
        if (!findVehicle) {
            exceptionClass = VehicleNotFoundException.class;
        } else if (!findBrand) {
            exceptionClass = BrandNotFoundException.class;
        } else if (checkIfVehicleIsReserved) {
            exceptionClass = VehicleReservedForLoanException.class;
        } else if (findVehicleByPlate) {
            exceptionClass = VehiclePlateExistsException.class;
        }
        if (exceptionClass != null) {
            assertThrows(exceptionClass, () -> vehicleService.update(vehicleDto));
        } else {
            final VehicleDto vehicleDtoUpdated = assertDoesNotThrow(() -> vehicleService.update(vehicleDto));
            expectVehicleDto(true, vehicleDto, vehicleDtoUpdated);
        }
    }

    @Test
    public void shouldUpdateVehicle() {
        shouldUpdateVehicle(getVehicle(ID, PLATE), true, true, false, false);
    }

    @Test
    public void shouldThrowVehicleNotFoundExceptionWhenUpdateVehicle() {
        shouldUpdateVehicle(getVehicle(ID, PLATE), false, true, false, false);
    }

    @Test
    public void shouldThrowBrandNotFoundExceptionWhenUpdateVehicle() {
        shouldUpdateVehicle(getVehicle(ID, PLATE), true, false, false, false);
    }

    @Test
    public void shouldThrowVehiclePlateExistsExceptionWhenUpdateVehicle() {
        shouldUpdateVehicle(getVehicle(ID, PLATE), true, true, false, true);
    }

    @Test
    public void shouldThrowVehicleReservedForLoanExceptionWhenUpdateVehicle() {
        shouldUpdateVehicle(getVehicle(ID, PLATE), true, true, true, false);
    }

    private void shouldDeleteVehicle(final Vehicle vehicle, final boolean checkIfVehicleIsReserved) {
        final VehicleDto vehicleDto = getVehicleDto(vehicle);
        when(vehicleMapper.vehicleToVehicleDto(any(Vehicle.class))).thenReturn(vehicleDto);
        when(loanApplicationRepository.checkIfVehicleIsReserved(any(Integer.class))).thenReturn(checkIfVehicleIsReserved);
        when(vehicleRepository.findById(any(Integer.class))).thenReturn(Optional.of(vehicle));
        doNothing().when(vehicleRepository).deleteById(any(Integer.class));
        if (checkIfVehicleIsReserved) {
            assertThrows(VehicleLoanApplicationExistsException.class, () -> vehicleService.delete(vehicle.getId()));
        } else {
            final VehicleDto vehicleDtoDeleted = assertDoesNotThrow(() -> vehicleService.delete(vehicle.getId()));
            assertThat(vehicleDtoDeleted, notNullValue());
            assertThat(vehicleDtoDeleted.getId(), is(vehicle.getId()));
        }
    }

    @Test
    public void shouldDeleteVehicle() {
        shouldDeleteVehicle(getVehicle(ID, PLATE), false);
    }

    @Test
    public void shouldThrowVehicleLoanApplicationExistsExceptionWhenDeleteVehicle() {
        shouldDeleteVehicle(getVehicle(ID, PLATE), true);
    }
}
