package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.Customer;
import com.bancopichincha.credito.automotriz.service.dto.CustomerDto;
import com.bancopichincha.credito.automotriz.util.CustomerUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static com.bancopichincha.credito.automotriz.util.CustomerUtil.expectCustomer;

@SpringBootTest
public class CustomerMapperTests {
    @Autowired
    private CustomerMapper customerMapper;

    @Test
    public void givenCustomerWhenMapCustomerThenCustomerDto() {
        final Customer customer = CustomerUtil.of(1, "1000000000");
        final CustomerDto customerDto = customerMapper.customerToCustomerDto(customer);
        expectCustomer(customer, customerDto);
    }
}
