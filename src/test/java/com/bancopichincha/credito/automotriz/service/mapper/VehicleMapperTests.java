package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.Vehicle;
import com.bancopichincha.credito.automotriz.service.dto.VehicleDto;
import com.bancopichincha.credito.automotriz.util.VehicleUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

import static com.bancopichincha.credito.automotriz.util.VehicleUtil.expectVehicle;

@SpringBootTest
public class VehicleMapperTests {
    private static final Integer ID = 1;
    private static final String PLATE = "ABC-1234";
    @Autowired
    private VehicleMapper vehicleMapper;

    @Test
    public void givenVehicleWhenMapVehicleThenVehicleDto() {
        final Vehicle vehicle = VehicleUtil.of(ID, PLATE);
        final VehicleDto vehicleDto = vehicleMapper.vehicleToVehicleDto(vehicle);
        expectVehicle(vehicle, vehicleDto);
    }

    @Test
    public void givenVehicleDtoWhenMapVehicleDtoThenVehicle() {
        final VehicleDto vehicleDto = vehicleMapper.vehicleToVehicleDto(VehicleUtil.of(ID, PLATE));
        final Vehicle vehicle = vehicleMapper.vehicleDtoToVehicle(vehicleDto);
        expectVehicle(vehicle, vehicleDto);
    }

    @Test
    public void givenVehicleAndDtoWhenUpdateVehicleWithDtoThenMatchVehicleWithDto() {
        final Vehicle vehicle = VehicleUtil.of(ID, PLATE);
        final VehicleDto vehicleDto = vehicleMapper.vehicleToVehicleDto(vehicle);
        vehicleDto.setId(vehicleDto.getId() + 1);
        vehicleDto.setPlate(vehicleDto.getPlate() + "1");
        vehicleDto.setModel(vehicleDto.getModel() + "1");
        vehicleDto.setChassisNumber(vehicleDto.getChassisNumber() + "1");
        vehicleDto.setType(vehicleDto.getType() + "1");
        vehicleDto.setCylinderCapacity(vehicleDto.getCylinderCapacity().add(BigDecimal.ONE));
        vehicleDto.setAppraisal(vehicleDto.getAppraisal() + 1);
        vehicleMapper.updateVehicleFromVehicleDto(vehicle, vehicleDto);
        expectVehicle(vehicle, vehicleDto);
    }
}
