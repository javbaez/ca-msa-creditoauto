package com.bancopichincha.credito.automotriz.service.dto.validator;

import com.bancopichincha.credito.automotriz.service.dto.validation.annotation.Age;
import com.bancopichincha.credito.automotriz.service.dto.validation.validator.AgeValidator;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import static org.mockito.Mockito.when;

@SpringBootTest
public class AgeValidatorTests extends ValidatorTests<Age, Byte, AgeValidator> {
    private static final byte AGE_MIN = 13;
    private static final byte AGE_MAX = 115;
    @Mock
    private Age annotation;

    @Override
    protected AgeValidator constructValidator() {
        return new AgeValidator();
    }

    @Override
    protected Age mockAnnotation() {
        when(annotation.min()).thenReturn(AGE_MIN);
        when(annotation.max()).thenReturn(AGE_MAX);
        return annotation;
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(bytes = {AGE_MIN, AGE_MIN + 1, AGE_MAX / 2 - 1, AGE_MAX / 2, AGE_MAX / 2 + 1, AGE_MAX - 1, AGE_MAX})
    public void shouldPassAgeValidation(final Byte age) {
        shouldPassValidation(age);
    }

    @ParameterizedTest
    @ValueSource(bytes = {AGE_MIN - 1, AGE_MAX + 1})
    public void shouldFailAgeValidation(final Byte age) {
        shouldFailValidation(age);
    }
}
