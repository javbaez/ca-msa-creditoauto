package com.bancopichincha.credito.automotriz.service.dto;

import com.bancopichincha.credito.automotriz.service.dto.validation.group.Create;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.CreateLoan;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.Update;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.UpdateLoan;
import com.bancopichincha.credito.automotriz.util.ValidatorUtil;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SpringBootTest
public class SalesExecutiveDtoTests {
    private void shouldValidateSalesExecutiveDto(
        final Integer id,
        final String idNumber,
        final String names,
        final String lastNames,
        final String address,
        final String landline,
        final String mobile,
        final Byte age,
        final Integer carDealershipId,
        final Class<?> validationGroup,
        final int numberOfErrors
    ) {
        final SalesExecutiveDto salesExecutiveDto = new SalesExecutiveDto();
        salesExecutiveDto.setId(id);
        salesExecutiveDto.setIdNumber(idNumber);
        salesExecutiveDto.setNames(names);
        salesExecutiveDto.setLastNames(lastNames);
        salesExecutiveDto.setAge(age);
        salesExecutiveDto.setAddress(address);
        salesExecutiveDto.setLandline(landline);
        salesExecutiveDto.setMobile(mobile);
        salesExecutiveDto.setCarDealershipId(carDealershipId);
        assertThat(ValidatorUtil.validate(salesExecutiveDto, validationGroup).size(), is(numberOfErrors));
    }

    @ParameterizedTest
    @CsvSource({
        "1707943039,PEDRO,GONZALEZ,QUITUMBE ÑAN Y AMARU ÑAN,022123456,0973123456,20,1",
        "1309537262,RODRIGO,PIEDRAITA,12 DE OCTUBRE Y PATRIA,022890123,0973890123,60,7",
        "1900150507,KARINA,RUEDA,ROBLES Y ULPIANO PAEZ,023098765,0983098765,26,10",
        "1306901719,MANUELA,SANCHEZ,RUMIPAMBA Y BURGEOIS,023654321,0983654321,33,4"
    })
    public void shouldPassSalesExecutiveCreateValidation(
        final String idNumber,
        final String names,
        final String lastNames,
        final String address,
        final String landline,
        final String mobile,
        final Byte age,
        final Integer carDealershipId
    ) {
        shouldValidateSalesExecutiveDto(
            null,
            idNumber,
            names,
            lastNames,
            address,
            landline,
            mobile,
            age,
            carDealershipId,
            Create.class,
            0
        );
    }

    @ParameterizedTest
    @CsvSource({
        "1,1707943039,PEDRO,GONZALEZ,QUITUMBE ÑAN Y AMARU ÑAN,022123456,0973123456,20,1",
        "2,1309537262,RODRIGO,PIEDRAITA,12 DE OCTUBRE Y PATRIA,022890123,0973890123,60,7",
        "3,1900150507,KARINA,RUEDA,ROBLES Y ULPIANO PAEZ,023098765,0983098765,26,10",
        "4,1306901719,MANUELA,SANCHEZ,RUMIPAMBA Y BURGEOIS,023654321,0983654321,33,4"
    })
    public void shouldPassSalesExecutiveDtoUpdateValidation(
        final Integer id,
        final String idNumber,
        final String names,
        final String lastNames,
        final String address,
        final String landline,
        final String mobile,
        final Byte age,
        final Integer carDealershipId
    ) {
        shouldValidateSalesExecutiveDto(
            id,
            idNumber,
            names,
            lastNames,
            address,
            landline,
            mobile,
            age,
            carDealershipId,
            Update.class,
            0
        );
    }

    @ParameterizedTest
    @CsvSource(value = {
        "null,null,null,null,null,null,null,null",
        ",,,,,,,",
        " , , , , , , , ",
        "123456789,null,null,null,12345678,123456789,12,null",
        "A,null,null,null,1234567890,12345678910,116,null",
        "?,null,null,null,A,B,null,null",
        "null,NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN,null,null,null,null,null,null",
        "null,null,AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,null,null,null,null,null",
        "null,null,null,DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD,null,null,null,null",
    }, nullValues = {"null"})
    public void shouldFailSalesExecutiveDtoCreateValidation(
        final String idNumber,
        final String names,
        final String lastNames,
        final String address,
        final String landline,
        final String mobile,
        final Byte age,
        final Integer carDealershipId
    ) {
        shouldValidateSalesExecutiveDto(
            null,
            idNumber,
            names,
            lastNames,
            address,
            landline,
            mobile,
            age,
            carDealershipId,
            Create.class,
            8
        );
    }

    @ParameterizedTest
    @CsvSource(value = {
        "null,null,null,null,null,null,null,null,null",
        ",,,,,,,,",
        " , , , , , , , , ",
        "0,123456789,null,null,null,12345678,123456789,12,null",
        "-1,A,null,null,null,1234567890,12345678910,116,null",
        "-2,?,null,null,null,A,B,null,null",
        "null,null,NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN,null,null,null,null,null,null",
        "null,null,null,AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,null,null,null,null,null",
        "null,null,null,null,DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD,null,null,null,null",
    }, nullValues = {"null"})
    public void shouldFailSalesExecutiveDtoUpdateValidation(
        final Integer id,
        final String idNumber,
        final String names,
        final String lastNames,
        final String address,
        final String landline,
        final String mobile,
        final Byte age,
        final Integer carDealershipId
    ) {
        shouldValidateSalesExecutiveDto(
            id,
            idNumber,
            names,
            lastNames,
            address,
            landline,
            mobile,
            age,
            carDealershipId,
            Update.class,
            9
        );
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5})
    public void shouldPassSalesExecutiveDtoCreateLoanValidation(final Integer id) {
        shouldValidateSalesExecutiveDto(
            id,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            CreateLoan.class,
            0
        );
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5})
    public void shouldPassSalesExecutiveDtoUpdateLoanValidation(final Integer id) {
        shouldValidateSalesExecutiveDto(
            id,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            UpdateLoan.class,
            0
        );
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(ints = {0, -1, -2, -3, -5})
    public void shouldFailSalesExecutiveDtoCreateLoanValidation(final Integer id) {
        shouldValidateSalesExecutiveDto(
            id,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            CreateLoan.class,
            1
        );
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(ints = {0, -1, -2, -3, -5})
    public void shouldFailSalesExecutiveDtoUpdateLoanValidation(final Integer id) {
        shouldValidateSalesExecutiveDto(
            id,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            UpdateLoan.class,
            1
        );
    }
}
