package com.bancopichincha.credito.automotriz.service.dto;

import com.bancopichincha.credito.automotriz.service.dto.validation.group.*;
import com.bancopichincha.credito.automotriz.util.ValidatorUtil;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SpringBootTest
public class BrandDtoTests {
    private void shouldValidateBrandDto(
        final Integer id,
        final String name,
        final Class<?> validationGroup,
        final int numberOfErrors
    ) {
        final BrandDto brandDto = new BrandDto();
        brandDto.setId(id);
        brandDto.setName(name);
        assertThat(ValidatorUtil.validate(brandDto, validationGroup).size(), is(numberOfErrors));
    }

    @ParameterizedTest
    @ValueSource(strings = {"Chevrolet", "KIA", "Hyundai", "Toyota", "Renault"})
    public void shouldPassBrandDtoCreateValidation(final String name) {
        shouldValidateBrandDto(null, name, Create.class, 0);
    }

    @ParameterizedTest
    @CsvSource({
        "1,Chevrolet",
        "2,KIA",
        "3,Hyundai",
        "4,Toyota",
        "5,Renault"
    })
    public void shouldPassBrandDtoUpdateValidation(final Integer id, final String name) {
        shouldValidateBrandDto(id, name, Update.class, 0);
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(strings = {"", " ", "NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN"})
    public void shouldFailBrandDtoCreateValidation(final String name) {
        shouldValidateBrandDto(null, name, Create.class, 1);
    }

    @ParameterizedTest
    @CsvSource(value = {
        "null,null",
        ",",
        " , ",
        "0,NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN"
    }, nullValues = {"null"})
    public void shouldFailBrandDtoUpdateValidation(final Integer id, final String name) {
        shouldValidateBrandDto(id, name, Update.class, 2);
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5})
    public void shouldPassBrandDtoCreateVehicleValidation(final Integer id) {
        shouldValidateBrandDto(id, null, CreateVehicle.class, 0);
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5})
    public void shouldPassBrandDtoUpdateVehicleValidation(final Integer id) {
        shouldValidateBrandDto(id, null, UpdateVehicle.class, 0);
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(ints = {0, -1, -2, -3, -5})
    public void shouldFailBrandDtoCreateVehicleValidation(final Integer id) {
        shouldValidateBrandDto(id, null, CreateVehicle.class, 1);
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(ints = {0, -1, -2, -3, -5})
    public void shouldFailBrandDtoUpdateVehicleValidation(final Integer id) {
        shouldValidateBrandDto(id, null, UpdateVehicle.class, 1);
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5})
    public void shouldPassBrandDtoCreateLoanValidation(final Integer id) {
        shouldValidateBrandDto(id, null, CreateLoan.class, 0);
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5})
    public void shouldPassBrandDtoUpdateLoanValidation(final Integer id) {
        shouldValidateBrandDto(id, null, UpdateLoan.class, 0);
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(ints = {0, -1, -2, -3, -5})
    public void shouldFailBrandDtoCreateLoanValidation(final Integer id) {
        shouldValidateBrandDto(id, null, CreateLoan.class, 1);
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(ints = {0, -1, -2, -3, -5})
    public void shouldFailBrandDtoUpdateLoanValidation(final Integer id) {
        shouldValidateBrandDto(id, null, UpdateLoan.class, 1);
    }
}
