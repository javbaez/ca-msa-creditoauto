package com.bancopichincha.credito.automotriz.service.dto;

import com.bancopichincha.credito.automotriz.domain.enums.LoanStatus;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.CreateLoan;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.UpdateLoan;
import com.bancopichincha.credito.automotriz.service.mapper.CarDealershipMapper;
import com.bancopichincha.credito.automotriz.service.mapper.CustomerMapper;
import com.bancopichincha.credito.automotriz.service.mapper.SalesExecutiveMapper;
import com.bancopichincha.credito.automotriz.service.mapper.VehicleMapper;
import com.bancopichincha.credito.automotriz.util.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@SpringBootTest
public class LoanApplicationDtoTests {
    @Autowired
    private CustomerMapper customerMapper;
    @Autowired
    private CarDealershipMapper carDealershipMapper;
    @Autowired
    private SalesExecutiveMapper salesExecutiveMapper;
    @Autowired
    private VehicleMapper vehicleMapper;

    private void shouldValidateLoanApplicationDto(
        final Integer id,
        final LocalDate date,
        final Integer customerId,
        final Integer carDealershipId,
        final Integer salesExecutiveId,
        final Integer vehicleId,
        final Byte monthsTerm,
        final Short installments,
        final BigDecimal downPayment,
        final String status,
        final String observation,
        final Class<?> validationGroup,
        final int minNumberOfErrors,
        final int maxNumberOfErrors
    ) {
        final LoanApplicationDto loanApplicationDto = new LoanApplicationDto();
        loanApplicationDto.setId(id);
        loanApplicationDto.setDate(date);
        loanApplicationDto.setCustomer(
            customerId != null ?
                customerMapper.customerToCustomerDto(CustomerUtil.of(1, "1000000000")) :
                null
        );
        loanApplicationDto.setCarDealership(
            carDealershipId != null ?
                carDealershipMapper.carDealershipToCarDealershipDto(CarDealershipUtil.of(carDealershipId)) :
                null
        );
        loanApplicationDto.setSalesExecutive(
            salesExecutiveId != null ?
                salesExecutiveMapper.salesExecutiveToSalesExecutiveDto(SalesExecutiveUtil.of(salesExecutiveId, "2000000000")) :
                null
        );
        loanApplicationDto.setVehicle(
            vehicleId != null ?
                vehicleMapper.vehicleToVehicleDto(VehicleUtil.of(vehicleId, "ABC-1234")) :
                null
        );
        loanApplicationDto.setMonthsTerm(monthsTerm);
        loanApplicationDto.setInstallments(installments);
        loanApplicationDto.setDownPayment(downPayment);
        try {
            loanApplicationDto.setStatus(status != null ? LoanStatus.of(status) : null);
        } catch (final IllegalArgumentException e) {
            loanApplicationDto.setStatus(null);
        }
        loanApplicationDto.setObservation(observation);
        assertThat(
            ValidatorUtil.validate(loanApplicationDto, validationGroup).size(),
            allOf(greaterThanOrEqualTo(minNumberOfErrors), lessThanOrEqualTo(maxNumberOfErrors))
        );
    }

    @ParameterizedTest
    @CsvSource(value = {
        "2100-01-01,1,1,1,1,3,3,500.00,R,null",
        "2100-01-02,2,2,2,2,4,4,500.01,R,null",
        "2100-01-03,3,3,3,3,60,240,1500.00,D,null",
        "2100-01-04,4,4,4,4,59,239,3000.00,C,Cancelado por test",
    }, nullValues = {"null"})
    public void shouldPassLoanApplicationDtoCreateValidation(
        final LocalDate date,
        final Integer customerId,
        final Integer carDealershipId,
        final Integer salesExecutiveId,
        final Integer vehicleId,
        final Byte monthsTerm,
        final Short installments,
        final BigDecimal downPayment,
        final String status,
        final String observation
    ) {
        shouldValidateLoanApplicationDto(
            null,
            date,
            customerId,
            carDealershipId,
            salesExecutiveId,
            vehicleId,
            monthsTerm,
            installments,
            downPayment,
            status,
            observation,
            CreateLoan.class,
            0,
            0
        );
    }

    @ParameterizedTest
    @CsvSource(value = {
        "1,2100-01-01,1,1,1,1,3,3,500.00,R,null",
        "2,2100-01-02,2,2,2,2,4,4,500.01,R,null",
        "3,2100-01-03,3,3,3,3,60,240,1500.00,D,null",
        "4,2100-01-04,4,4,4,4,59,239,3000.00,C,Cancelado por test",
    }, nullValues = {"null"})
    public void shouldPassLoanApplicationDtoUpdateValidation(
        final Integer id,
        final LocalDate date,
        final Integer customerId,
        final Integer carDealershipId,
        final Integer salesExecutiveId,
        final Integer vehicleId,
        final Byte monthsTerm,
        final Short installments,
        final BigDecimal downPayment,
        final String status,
        final String observation
    ) {
        shouldValidateLoanApplicationDto(
            id,
            date,
            customerId,
            carDealershipId,
            salesExecutiveId,
            vehicleId,
            monthsTerm,
            installments,
            downPayment,
            status,
            observation,
            UpdateLoan.class,
            0,
            0
        );
    }

    @ParameterizedTest
    @CsvSource(value = {
        "null,null,null,null,null,null,null,null,null,null",
        ",,,,,,,,,",
        " , , , , , , , , , ",
        "2010-01-01,null,null,null,null,2,2,499.99,X,null",
        "2023-01-01,null,null,null,null,61,241,0.0,Y,null",
        "null,null,null,null,null,null,null,null,null,OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
    }, nullValues = {"null"})
    public void shouldFailLoanApplicationDtoCreateValidation(
        final LocalDate date,
        final Integer customerId,
        final Integer carDealershipId,
        final Integer salesExecutiveId,
        final Integer vehicleId,
        final Byte monthsTerm,
        final Short installments,
        final BigDecimal downPayment,
        final String status,
        final String observation
    ) {
        shouldValidateLoanApplicationDto(
            null,
            date,
            customerId,
            carDealershipId,
            salesExecutiveId,
            vehicleId,
            monthsTerm,
            installments,
            downPayment,
            status,
            observation,
            CreateLoan.class,
            9,
            10
        );
    }

    @ParameterizedTest
    @CsvSource(value = {
        "null,null,null,null,null,null,null,null,null,null,null",
        ",,,,,,,,,,",
        " , , , , , , , , , , ",
        "0,2010-01-01,null,null,null,null,2,2,499.99,X,null",
        "-1,2023-01-01,null,null,null,null,61,241,0.0,Y,null",
        "-2,null,null,null,null,null,null,null,null,null,OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
    }, nullValues = {"null"})
    public void shouldFailLoanApplicationDtoCreateValidation(
        final Integer id,
        final LocalDate date,
        final Integer customerId,
        final Integer carDealershipId,
        final Integer salesExecutiveId,
        final Integer vehicleId,
        final Byte monthsTerm,
        final Short installments,
        final BigDecimal downPayment,
        final String status,
        final String observation
    ) {
        shouldValidateLoanApplicationDto(
            id,
            date,
            customerId,
            carDealershipId,
            salesExecutiveId,
            vehicleId,
            monthsTerm,
            installments,
            downPayment,
            status,
            observation,
            UpdateLoan.class,
            10,
            11
        );
    }
}
