package com.bancopichincha.credito.automotriz.service.dto;

import com.bancopichincha.credito.automotriz.service.dto.validation.group.CreateAssignment;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.UpdateAssignment;
import com.bancopichincha.credito.automotriz.service.mapper.CarDealershipMapper;
import com.bancopichincha.credito.automotriz.service.mapper.CustomerMapper;
import com.bancopichincha.credito.automotriz.util.CarDealershipUtil;
import com.bancopichincha.credito.automotriz.util.CustomerUtil;
import com.bancopichincha.credito.automotriz.util.ValidatorUtil;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SpringBootTest
public class CustomerAssignmentDtoTests {
    @Autowired
    private CustomerMapper customerMapper;
    @Autowired
    private CarDealershipMapper carDealershipMapper;

    private void shouldValidateCustomerAssignmentDto(
        final Integer id,
        final Integer customerId,
        final Integer carDealershipId,
        final LocalDate assignmentDate,
        final Class<?> validationGroup,
        final int numberOfErrors
    ) {
        final CustomerAssignmentDto customerAssignmentDto = new CustomerAssignmentDto();
        customerAssignmentDto.setId(id);
        customerAssignmentDto.setCustomer(
            customerId != null ?
                customerMapper.customerToCustomerDto(CustomerUtil.of(customerId, "1000000000")) :
                null
        );
        customerAssignmentDto.setCarDealership(
            carDealershipId != null ?
                carDealershipMapper.carDealershipToCarDealershipDto(CarDealershipUtil.of(carDealershipId)) :
                null
        );
        customerAssignmentDto.setAssignmentDate(assignmentDate);
        assertThat(ValidatorUtil.validate(customerAssignmentDto, validationGroup).size(), is(numberOfErrors));
    }

    @ParameterizedTest
    @CsvSource({
        "1,1,2023-05-01",
        "2,2,2023-05-02",
        "3,3,2023-05-03",
        "4,4,2023-05-04",
        "5,5,2023-05-05"
    })
    public void shouldPassCustomerAssignmentDtoCreateValidation(
        final Integer customerId,
        final Integer carDealershipId,
        final LocalDate assignmentDate
    ) {
        shouldValidateCustomerAssignmentDto(
            null,
            customerId,
            carDealershipId,
            assignmentDate,
            CreateAssignment.class,
            0
        );
    }

    @ParameterizedTest
    @CsvSource({
        "1,1,1,2023-05-01",
        "2,2,2,2023-05-02",
        "3,3,3,2023-05-03",
        "4,4,4,2023-05-04",
        "5,5,5,2023-05-05"
    })
    public void shouldPassCustomerAssignmentDtoUpdateValidation(
        final Integer id,
        final Integer customerId,
        final Integer carDealershipId,
        final LocalDate assignmentDate
    ) {
        shouldValidateCustomerAssignmentDto(
            id,
            customerId,
            carDealershipId,
            assignmentDate,
            UpdateAssignment.class,
            0
        );
    }

    @ParameterizedTest
    @CsvSource(value = {
        "null,null,null",
        ",,",
        " , , ",
        "null,null,2100-01-01"
    }, nullValues = {"null"})
    public void shouldFailCustomerAssignmentDtoCreateValidation(
        final Integer customerId,
        final Integer carDealershipId,
        final LocalDate assignmentDate
    ) {
        shouldValidateCustomerAssignmentDto(
            null,
            customerId,
            carDealershipId,
            assignmentDate,
            CreateAssignment.class,
            3
        );
    }

    @ParameterizedTest
    @CsvSource(value = {
        "null,null,null,null",
        ",,,",
        " , , , ",
        "0,null,null,2100-01-01"
    }, nullValues = {"null"})
    public void shouldFailCustomerAssignmentDtoUpdateValidation(
        final Integer id,
        final Integer customerId,
        final Integer carDealershipId,
        final LocalDate assignmentDate
    ) {
        shouldValidateCustomerAssignmentDto(
            id,
            customerId,
            carDealershipId,
            assignmentDate,
            UpdateAssignment.class,
            4
        );
    }
}
