package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.CarDealership;
import com.bancopichincha.credito.automotriz.service.dto.CarDealershipDto;
import com.bancopichincha.credito.automotriz.util.CarDealershipUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static com.bancopichincha.credito.automotriz.util.CarDealershipUtil.expectCarDealership;

@SpringBootTest
public class CarDealershipMapperTests {
    private static final Integer ID = 1;
    @Autowired
    private CarDealershipMapper carDealershipMapper;

    @Test
    public void givenCarDealershipWhenMapCarDealershipThenCarDealershipDto() {
        final CarDealership carDealership = CarDealershipUtil.of(ID);
        final CarDealershipDto carDealershipDto = carDealershipMapper.carDealershipToCarDealershipDto(carDealership);
        expectCarDealership(carDealership, carDealershipDto);
    }

    @Test
    public void givenCarDealershipDtoWhenMapCarDealershipDtoThenCarDealership() {
        final CarDealershipDto carDealershipDto = carDealershipMapper
            .carDealershipToCarDealershipDto(CarDealershipUtil.of(ID));
        final CarDealership carDealership = carDealershipMapper.carDealershipDtoToCarDealership(carDealershipDto);
        expectCarDealership(carDealership, carDealershipDto);
    }

    @Test
    public void givenCarDealershipAndDtoWhenUpdateCarDealershipWithDtoThenMatchCarDealershipWithDto() {
        final CarDealership carDealership = CarDealershipUtil.of(ID);
        final CarDealershipDto carDealershipDto = carDealershipMapper.carDealershipToCarDealershipDto(carDealership);
        carDealershipDto.setId(carDealershipDto.getId() + 1);
        carDealershipDto.setName(carDealershipDto.getName() + "1");
        carDealershipDto.setAddress(carDealershipDto.getAddress() + "1");
        carDealershipDto.setPhone(carDealershipDto.getPhone().substring(0, carDealershipDto.getPhone().length() - 1) + "1");
        carDealershipDto.setPointOfSaleNumber(carDealershipDto.getPointOfSaleNumber() + 1);
        carDealershipMapper.updateCarDealershipFromCarDealershipDto(carDealership, carDealershipDto);
        expectCarDealership(carDealership, carDealershipDto);
    }
}
