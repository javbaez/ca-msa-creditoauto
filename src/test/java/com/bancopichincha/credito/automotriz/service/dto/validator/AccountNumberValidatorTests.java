package com.bancopichincha.credito.automotriz.service.dto.validator;

import com.bancopichincha.credito.automotriz.service.dto.validation.annotation.AccountNumber;
import com.bancopichincha.credito.automotriz.service.dto.validation.validator.AccountNumberValidator;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Random;

import static org.mockito.Mockito.when;

@SpringBootTest
public class AccountNumberValidatorTests extends ValidatorTests<AccountNumber, String, AccountNumberValidator> {
    private static final int ACCOUNT_NUMBER_LENGTH = 5;
    @Mock
    private AccountNumber annotation;

    private static List<String> accountNumbers() {
        return new Random()
            .ints(
                5,
                (int) Math.pow(10, ACCOUNT_NUMBER_LENGTH - 1),
                Integer.parseInt("9".repeat(ACCOUNT_NUMBER_LENGTH))
            )
            .boxed()
            .map(String::valueOf)
            .toList();
    }

    @Override
    protected AccountNumberValidator constructValidator() {
        return new AccountNumberValidator();
    }

    @Override
    protected AccountNumber mockAnnotation() {
        when(annotation.length()).thenReturn(ACCOUNT_NUMBER_LENGTH);
        return annotation;
    }

    @ParameterizedTest
    @NullSource
    @MethodSource("accountNumbers")
    public void shouldPassAccountNumberValidation(final String accountNumber) {
        shouldPassValidation(accountNumber);
    }

    @ParameterizedTest
    @ValueSource(strings = {"", " ", "A", "1", "12", "123", "1234"})
    public void shouldFailAccountNumberValidation(final String accountNumber) {
        shouldFailValidation(accountNumber);
    }
}
