package com.bancopichincha.credito.automotriz.service.dto.validator;

import com.bancopichincha.credito.automotriz.service.dto.validation.annotation.Plate;
import com.bancopichincha.credito.automotriz.service.dto.validation.validator.PlateValidator;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class PlateValidatorTests extends ValidatorTests<Plate, String, PlateValidator> {
    @Mock
    private Plate annotation;

    @Override
    protected PlateValidator constructValidator() {
        return new PlateValidator();
    }

    @Override
    protected Plate mockAnnotation() {
        return annotation;
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(strings = {"ABC-0123", "XYZ-1234", "AB012C", "XY123Z"})
    public void shouldPassPlateValidation(final String plate) {
        shouldPassValidation(plate);
    }

    @ParameterizedTest
    @ValueSource(strings = {"", " ", "?", "ABC123", "ABC-123", "ABC0123", "AB01C", "XY123"})
    public void shouldFailPlateValidation(final String plate) {
        shouldFailValidation(plate);
    }
}
