package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.SalesExecutive;
import com.bancopichincha.credito.automotriz.service.dto.SalesExecutiveDto;
import com.bancopichincha.credito.automotriz.util.SalesExecutiveUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static com.bancopichincha.credito.automotriz.util.SalesExecutiveUtil.expectSalesExecutive;

@SpringBootTest
public class SalesExecutiveMapperTests {
    @Autowired
    private SalesExecutiveMapper salesExecutiveMapper;

    @Test
    public void givenSalesExecutiveWhenMapSalesExecutiveThenSalesExecutiveDto() {
        final SalesExecutive salesExecutive = SalesExecutiveUtil.of(1, "1000000000");
        final SalesExecutiveDto salesExecutiveDto = salesExecutiveMapper
            .salesExecutiveToSalesExecutiveDto(salesExecutive);
        expectSalesExecutive(salesExecutive, salesExecutiveDto);
    }
}
