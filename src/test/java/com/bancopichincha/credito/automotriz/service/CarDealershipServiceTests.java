package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.domain.CarDealership;
import com.bancopichincha.credito.automotriz.exception.CarDealershipNotFoundException;
import com.bancopichincha.credito.automotriz.exception.CarDealershipReferenceToCustomerAssignmentException;
import com.bancopichincha.credito.automotriz.exception.CarDealershipReferenceToSalesExecutiveException;
import com.bancopichincha.credito.automotriz.repository.CarDealershipRepository;
import com.bancopichincha.credito.automotriz.repository.CustomerAssignmentRepository;
import com.bancopichincha.credito.automotriz.repository.SalesExecutiveRepository;
import com.bancopichincha.credito.automotriz.service.dto.CarDealershipDto;
import com.bancopichincha.credito.automotriz.service.impl.CarDealershipServiceImpl;
import com.bancopichincha.credito.automotriz.service.mapper.CarDealershipMapper;
import com.bancopichincha.credito.automotriz.util.CarDealershipUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class CarDealershipServiceTests {
    private static final Integer ID = 1;
    @MockBean
    private CarDealershipMapper carDealershipMapper;
    @MockBean
    private CarDealershipRepository carDealershipRepository;
    @MockBean
    private SalesExecutiveRepository salesExecutiveRepository;
    @MockBean
    private CustomerAssignmentRepository customerAssignmentRepository;
    @InjectMocks
    @Autowired
    private CarDealershipServiceImpl carDealershipService;

    private CarDealership getCarDealership(final Integer id) {
        return CarDealershipUtil.of(id);
    }

    private CarDealershipDto getCarDealershipDto(final CarDealership carDealership) {
        final CarDealershipDto carDealershipDto = new CarDealershipDto();
        carDealershipDto.setId(carDealership.getId());
        carDealershipDto.setName(carDealership.getName());
        carDealershipDto.setAddress(carDealership.getAddress());
        carDealershipDto.setPhone(carDealership.getPhone());
        carDealershipDto.setPointOfSaleNumber(carDealership.getPointOfSaleNumber());
        return carDealershipDto;
    }

    private void expectCarDealershipDto(final boolean validateId, final CarDealershipDto inputDto, final CarDealershipDto resultDto) {
        if (validateId) {
            assertThat(resultDto.getId(), is(inputDto.getId()));
        }
        assertThat(resultDto.getName(), is(inputDto.getName()));
        assertThat(resultDto.getAddress(), is(inputDto.getAddress()));
        assertThat(resultDto.getPhone(), is(inputDto.getPhone()));
        assertThat(resultDto.getPointOfSaleNumber(), is(inputDto.getPointOfSaleNumber()));
    }

    @Test
    public void shouldFindCarDealership() {
        final CarDealership carDealership = getCarDealership(ID);
        final CarDealershipDto carDealershipDto = getCarDealershipDto(carDealership);
        when(carDealershipMapper.carDealershipToCarDealershipDto(any(CarDealership.class)))
            .thenReturn(carDealershipDto);
        when(carDealershipRepository.findById(any(Integer.class))).thenReturn(Optional.of(carDealership));
        final CarDealershipDto carDealershipDtoFound = assertDoesNotThrow(() -> carDealershipService.findById(carDealership.getId()));
        assertThat(carDealershipDtoFound, notNullValue());
        assertThat(carDealershipDtoFound.getId(), is(carDealership.getId()));
    }

    @Test
    public void shouldCreateCarDealership() {
        final CarDealership carDealership = getCarDealership(null);
        final CarDealershipDto carDealershipDto = getCarDealershipDto(carDealership);
        when(carDealershipMapper.carDealershipDtoToCarDealership(any(CarDealershipDto.class)))
            .thenReturn(carDealership);
        when(carDealershipMapper.carDealershipToCarDealershipDto(any(CarDealership.class)))
            .thenReturn(carDealershipDto);
        when(carDealershipRepository.save(any(CarDealership.class))).thenReturn(carDealership);
        final CarDealershipDto carDealershipDtoCreated = assertDoesNotThrow(() -> carDealershipService.create(carDealershipDto));
        expectCarDealershipDto(false, carDealershipDto, carDealershipDtoCreated);
    }

    private void shouldUpdateCarDealership(final CarDealership carDealership, final boolean findCarDealership) {
        final CarDealershipDto carDealershipDto = getCarDealershipDto(carDealership);
        carDealershipDto.setName(carDealershipDto.getName() + "1");
        carDealershipDto.setAddress(carDealershipDto.getAddress() + "1");
        carDealershipDto.setPhone(carDealershipDto.getPhone().substring(0, carDealershipDto.getPhone().length() - 1) + "1");
        carDealershipDto.setPointOfSaleNumber(carDealershipDto.getPointOfSaleNumber() + 1);
        doNothing().when(carDealershipMapper)
            .updateCarDealershipFromCarDealershipDto(any(CarDealership.class), any(CarDealershipDto.class));
        when(carDealershipMapper.carDealershipToCarDealershipDto(any(CarDealership.class)))
            .thenReturn(carDealershipDto);
        when(carDealershipRepository.findById(any(Integer.class))).thenReturn(
            findCarDealership ?
                Optional.of(carDealership) :
                Optional.empty()
        );
        when(carDealershipRepository.save(any(CarDealership.class))).thenReturn(carDealership);
        if (!findCarDealership) {
            assertThrows(CarDealershipNotFoundException.class, () -> carDealershipService.update(carDealershipDto));
        } else {
            final CarDealershipDto carDealershipDtoUpdated =
                assertDoesNotThrow(() -> carDealershipService.update(carDealershipDto));
            expectCarDealershipDto(true, carDealershipDto, carDealershipDtoUpdated);
        }
    }

    @Test
    public void shouldUpdateCarDealership() {
        shouldUpdateCarDealership(getCarDealership(ID), true);
    }

    @Test
    public void shouldThrowCarDealershipNotFoundExceptionWhenUpdateCarDealership() {
        shouldUpdateCarDealership(getCarDealership(ID), false);
    }

    private void shouldDeleteCarDealership(
        final CarDealership carDealership,
        final boolean findCarDealership,
        final boolean checkIfExistsByCarDealershipId,
        final boolean checkIfCarDealershipHasAssignments
    ) {
        final CarDealershipDto carDealershipDto = getCarDealershipDto(carDealership);
        when(carDealershipMapper.carDealershipToCarDealershipDto(any(CarDealership.class)))
            .thenReturn(carDealershipDto);
        when(salesExecutiveRepository.checkIfExistsByCarDealershipId(any(Integer.class)))
            .thenReturn(checkIfExistsByCarDealershipId);
        when(customerAssignmentRepository.checkIfCarDealershipHasAssignments(any(Integer.class)))
            .thenReturn(checkIfCarDealershipHasAssignments);
        when(carDealershipRepository.findById(any(Integer.class))).thenReturn(
            findCarDealership ?
                Optional.of(carDealership) :
                Optional.empty()
        );
        doNothing().when(carDealershipRepository).deleteById(any(Integer.class));
        Class<? extends Throwable> exceptionClass = null;
        if (!findCarDealership) {
            exceptionClass = CarDealershipNotFoundException.class;
        } else if (checkIfExistsByCarDealershipId) {
            exceptionClass = CarDealershipReferenceToSalesExecutiveException.class;
        } else if (checkIfCarDealershipHasAssignments) {
            exceptionClass = CarDealershipReferenceToCustomerAssignmentException.class;
        }
        if (exceptionClass != null) {
            assertThrows(exceptionClass, () -> carDealershipService.delete(carDealership.getId()));
        } else {
            final CarDealershipDto carDealershipDtoDeleted = assertDoesNotThrow(() -> carDealershipService.delete(carDealership.getId()));
            assertThat(carDealershipDtoDeleted, notNullValue());
            assertThat(carDealershipDtoDeleted.getId(), is(carDealership.getId()));
        }
    }

    @Test
    public void shouldDeleteCarDealership() {
        shouldDeleteCarDealership(getCarDealership(ID), false, false, false);
    }

    @Test
    public void shouldThrowCarDealershipNotFoundExceptionWhenDeleteCarDealership() {
        shouldDeleteCarDealership(getCarDealership(ID), true, false, false);
    }

    @Test
    public void shouldThrowCarDealershipReferenceToSalesExecutiveExceptionWhenDeleteCarDealership() {
        shouldDeleteCarDealership(getCarDealership(ID), false, true, false);
    }

    @Test
    public void shouldThrowCarDealershipReferenceToCustomerAssignmentExceptionWhenDeleteCarDealership() {
        shouldDeleteCarDealership(getCarDealership(ID), false, false, true);
    }
}
