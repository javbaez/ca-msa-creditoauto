package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.Brand;
import com.bancopichincha.credito.automotriz.service.dto.BrandDto;
import com.bancopichincha.credito.automotriz.util.BrandUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static com.bancopichincha.credito.automotriz.util.BrandUtil.expectBrand;

@SpringBootTest
public class BrandMapperTests {
    @Autowired
    private BrandMapper brandMapper;

    @Test
    public void givenBrandWhenMapBrandThenBrandDto() {
        final Brand brand = BrandUtil.of(1, "Marca test");
        final BrandDto brandDto = brandMapper.brandToBrandDto(brand);
        expectBrand(brand, brandDto);
    }
}
