package com.bancopichincha.credito.automotriz.service.dto;

import com.bancopichincha.credito.automotriz.domain.enums.CivilStatus;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.*;
import com.bancopichincha.credito.automotriz.util.ValidatorUtil;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@SpringBootTest
public class CustomerDtoTests {
    private void shouldValidateCustomerDto(
        final Integer id,
        final String idNumber,
        final String names,
        final String lastNames,
        final Byte age,
        final LocalDate birthday,
        final String address,
        final String phone,
        final String civilStatus,
        final String spouseIdNumber,
        final String spouseNames,
        final Boolean creditSubject,
        final Class<?> validationGroup,
        final int minNumberOfErrors,
        final int maxNumberOfErrors
    ) {
        final CustomerDto customerDto = new CustomerDto();
        customerDto.setId(id);
        customerDto.setIdNumber(idNumber);
        customerDto.setNames(names);
        customerDto.setLastNames(lastNames);
        customerDto.setAge(age);
        customerDto.setBirthday(birthday);
        customerDto.setAddress(address);
        customerDto.setPhone(phone);
        try {
            customerDto.setCivilStatus(civilStatus != null ? CivilStatus.of(civilStatus) : null);
        } catch (final IllegalArgumentException e) {
            customerDto.setCivilStatus(null);
        }
        customerDto.setSpouseIdNumber(spouseIdNumber);
        customerDto.setSpouseNames(spouseNames);
        customerDto.setCreditSubject(creditSubject);
        assertThat(
            ValidatorUtil.validate(customerDto, validationGroup).size(),
            allOf(greaterThanOrEqualTo(minNumberOfErrors), lessThanOrEqualTo(maxNumberOfErrors))
        );
    }

    @ParameterizedTest
    @CsvSource(value = {
        "1705251732,PEDRO,PEREZ,18,2005-01-01,AV. 6 DE DICIEMBRE Y COLON,0980123456,S,null,null,FALSE",
        "1713311494,LUIS,SALAZAR,45,1978-03-03,REPUBLICA Y ELOY ALFARO,0982345678,C,0915916753,MARIA CAMPOS,TRUE",
        "1802584068,RAFAEL,PATIÑO,65,1958-05-05,SHYRIS Y JAPON,0991678901,D,null,null,TRUE",
        "1712664570,PATRICIA,AYALA,79,1944-05-05,AV. AMAZONAS Y PEREIRA,0992321098,V,null,null,TRUE"
    }, nullValues = {"null"})
    public void shouldPassCustomerDtoCreateValidation(
        final String idNumber,
        final String names,
        final String lastNames,
        final Byte age,
        final LocalDate birthday,
        final String address,
        final String phone,
        final String civilStatus,
        final String spouseIdNumber,
        final String spouseNames,
        final Boolean creditSubject
    ) {
        shouldValidateCustomerDto(
            null,
            idNumber,
            names,
            lastNames,
            age,
            birthday,
            address,
            phone,
            civilStatus,
            spouseIdNumber,
            spouseNames,
            creditSubject,
            Create.class,
            0,
            0
        );
    }

    @ParameterizedTest
    @CsvSource(value = {
        "1,1705251732,PEDRO,PEREZ,18,2005-01-01,AV. 6 DE DICIEMBRE Y COLON,0980123456,S,null,null,FALSE",
        "2,1713311494,LUIS,SALAZAR,45,1978-03-03,REPUBLICA Y ELOY ALFARO,0982345678,C,0915916753,MARIA CAMPOS,TRUE",
        "3,1802584068,RAFAEL,PATIÑO,65,1958-05-05,SHYRIS Y JAPON,0991678901,D,null,null,TRUE",
        "4,1712664570,PATRICIA,AYALA,79,1944-05-05,AV. AMAZONAS Y PEREIRA,0992321098,V,null,null,TRUE"
    }, nullValues = {"null"})
    public void shouldPassCustomerDtoUpdateValidation(
        final Integer id,
        final String idNumber,
        final String names,
        final String lastNames,
        final Byte age,
        final LocalDate birthday,
        final String address,
        final String phone,
        final String civilStatus,
        final String spouseIdNumber,
        final String spouseNames,
        final Boolean creditSubject
    ) {
        shouldValidateCustomerDto(
            id,
            idNumber,
            names,
            lastNames,
            age,
            birthday,
            address,
            phone,
            civilStatus,
            spouseIdNumber,
            spouseNames,
            creditSubject,
            Update.class,
            0,
            0
        );
    }

    @ParameterizedTest
    @CsvSource(value = {
        "null,null,null,null,null,null,null,null,null,null,null",
        ",,,,,,,,,,",
        " , , , , , , , , , , ",
        "123456789,null,null,12,2100-01-01,null,A,C,123456789,null,null",
        "A,null,null,116,null,null,12345678,C,A,null,null",
        "?,null,null,null,null,null,12345678910,C,?,null,null",
        "null,NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN,null,null,null,null,null,null,null,null,null",
        "null,null,AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,null,null,null,null,null,null,null,null",
        "null,null,null,null,null,DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD,null,null,null,null,null",
        "null,null,null,null,null,null,null,C,null,SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS,null"
    }, nullValues = {"null"})
    public void shouldFailCustomerDtoCreateValidation(
        final String idNumber,
        final String names,
        final String lastNames,
        final Byte age,
        final LocalDate birthday,
        final String address,
        final String phone,
        final String civilStatus,
        final String spouseIdNumber,
        final String spouseNames,
        final Boolean creditSubject
    ) {
        shouldValidateCustomerDto(
            null,
            idNumber,
            names,
            lastNames,
            age,
            birthday,
            address,
            phone,
            civilStatus,
            spouseIdNumber,
            spouseNames,
            creditSubject,
            Create.class,
            9,
            11
        );
    }

    @ParameterizedTest
    @CsvSource(value = {
        "null,null,null,null,null,null,null,null,null,null,null,null",
        ",,,,,,,,,,,",
        " , , , , , , , , , , , ",
        "0,123456789,null,null,12,2100-01-01,null,A,C,123456789,null,null",
        "-1,A,null,null,116,null,null,12345678,C,A,null,null",
        "-2,?,null,null,null,null,null,12345678910,C,?,null,null",
        "null,null,NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN,null,null,null,null,null,null,null,null,null",
        "null,null,null,AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,null,null,null,null,null,null,null,null",
        "null,null,null,null,null,null,DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD,null,null,null,null,null",
        "null,null,null,null,null,null,null,null,C,null,SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS,null"
    }, nullValues = {"null"})
    public void shouldFailCustomerDtoUpdateValidation(
        final Integer id,
        final String idNumber,
        final String names,
        final String lastNames,
        final Byte age,
        final LocalDate birthday,
        final String address,
        final String phone,
        final String civilStatus,
        final String spouseIdNumber,
        final String spouseNames,
        final Boolean creditSubject
    ) {
        shouldValidateCustomerDto(
            id,
            idNumber,
            names,
            lastNames,
            age,
            birthday,
            address,
            phone,
            civilStatus,
            spouseIdNumber,
            spouseNames,
            creditSubject,
            Update.class,
            9,
            11
        );
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5})
    public void shouldPassCustomerDtoCreateAssignmentValidation(final Integer id) {
        shouldValidateCustomerDto(
            id,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            CreateAssignment.class,
            0,
            0
        );
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5})
    public void shouldPassCustomerDtoUpdateAssignmentValidation(final Integer id) {
        shouldValidateCustomerDto(
            id,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            UpdateAssignment.class,
            0,
            0
        );
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(ints = {0, -1, -2, -3, -5})
    public void shouldFailCustomerDtoCreateAssignmentValidation(final Integer id) {
        shouldValidateCustomerDto(
            id,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            CreateAssignment.class,
            1,
            1
        );
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(ints = {0, -1, -2, -3, -5})
    public void shouldFailCustomerDtoUpdateAssignmentValidation(final Integer id) {
        shouldValidateCustomerDto(
            id,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            UpdateAssignment.class,
            1,
            1
        );
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5})
    public void shouldPassCustomerDtoCreateLoanValidation(final Integer id) {
        shouldValidateCustomerDto(
            id,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            CreateLoan.class,
            0,
            0
        );
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5})
    public void shouldPassCustomerDtoUpdateLoanValidation(final Integer id) {
        shouldValidateCustomerDto(
            id,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            UpdateLoan.class,
            0,
            0
        );
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(ints = {0, -1, -2, -3, -5})
    public void shouldFailCustomerDtoCreateLoanValidation(final Integer id) {
        shouldValidateCustomerDto(
            id,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            CreateLoan.class,
            1,
            1
        );
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(ints = {0, -1, -2, -3, -5})
    public void shouldFailCustomerDtoUpdateLoanValidation(final Integer id) {
        shouldValidateCustomerDto(
            id,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            UpdateLoan.class,
            1,
            1
        );
    }
}
