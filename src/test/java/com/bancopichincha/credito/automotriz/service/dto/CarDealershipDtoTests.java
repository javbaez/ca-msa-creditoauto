package com.bancopichincha.credito.automotriz.service.dto;

import com.bancopichincha.credito.automotriz.service.dto.validation.group.Create;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.CreateLoan;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.Update;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.UpdateLoan;
import com.bancopichincha.credito.automotriz.util.ValidatorUtil;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SpringBootTest
public class CarDealershipDtoTests {
    private void shouldValidateCarDealershipDto(
        final Integer id,
        final String name,
        final String address,
        final String phone,
        final Integer pointOfSaleNumber,
        final Class<?> validationGroup,
        final int numberOfErrors
    ) {
        final CarDealershipDto carDealershipDto = new CarDealershipDto();
        carDealershipDto.setId(id);
        carDealershipDto.setName(name);
        carDealershipDto.setAddress(address);
        carDealershipDto.setPhone(phone);
        carDealershipDto.setPointOfSaleNumber(pointOfSaleNumber);
        assertThat(ValidatorUtil.validate(carDealershipDto, validationGroup).size(), is(numberOfErrors));
    }

    @ParameterizedTest
    @CsvSource({
        "AUTOVENTURA,AV. OSWALDO GUAYASAMIN,022370034,1234",
        "CABRERA AUTOS,AV. 10 DE AGOSTO Y MARIANA DE JESUS,0983351392,2345",
        "ESPINOSA AUTOS,AV. GRAL ENRIQUEZ Y GRUPO DAVALOS,0984207925,3456"
    })
    public void shouldPassCarDealershipDtoCreateValidation(
        final String name,
        final String address,
        final String phone,
        final Integer pointOfSaleNumber
    ) {
        shouldValidateCarDealershipDto(null, name, address, phone, pointOfSaleNumber, Create.class, 0);
    }

    @ParameterizedTest
    @CsvSource({
        "1,AUTOVENTURA,AV. OSWALDO GUAYASAMIN,022370034,1234",
        "2,CABRERA AUTOS,AV. 10 DE AGOSTO Y MARIANA DE JESUS,0983351392,2345",
        "3,ESPINOSA AUTOS,AV. GRAL ENRIQUEZ Y GRUPO DAVALOS,0984207925,3456"
    })
    public void shouldPassCarDealershipDtoUpdateValidation(
        final Integer id,
        final String name,
        final String address,
        final String phone,
        final Integer pointOfSaleNumber
    ) {
        shouldValidateCarDealershipDto(id, name, address, phone, pointOfSaleNumber, Update.class, 0);
    }

    @ParameterizedTest
    @CsvSource(value = {
        "null,null,null,null",
        ",,,",
        " , , , ",
        "null,null,12345678,0",
        "null,null,12345678910,-1",
        "null,null,A,1000001",
        "NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN,null,null,null",
        "null,DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD,null,null",
    }, nullValues = {"null"})
    public void shouldFailCarDealershipDtoCreateValidation(
        final String name,
        final String address,
        final String phone,
        final Integer pointOfSaleNumber
    ) {
        shouldValidateCarDealershipDto(null, name, address, phone, pointOfSaleNumber, Create.class, 4);
    }

    @ParameterizedTest
    @CsvSource(value = {
        "null,null,null,null,null",
        ",,,,",
        " , , , , ",
        "0,null,null,12345678,0",
        "-1,null,null,12345678910,-1",
        "-2,null,null,A,1000001",
        "null,NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN,null,null,null",
        "null,null,DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD,null,null",
    }, nullValues = {"null"})
    public void shouldFailCarDealershipDtoUpdateValidation(
        final Integer id,
        final String name,
        final String address,
        final String phone,
        final Integer pointOfSaleNumber
    ) {
        shouldValidateCarDealershipDto(id, name, address, phone, pointOfSaleNumber, Update.class, 5);
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5})
    public void shouldPassCarDealershipDtoCreateLoanValidation(final Integer id) {
        shouldValidateCarDealershipDto(id, null, null, null, null, CreateLoan.class, 0);
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5})
    public void shouldPassCarDealershipDtoUpdateLoanValidation(final Integer id) {
        shouldValidateCarDealershipDto(id, null, null, null, null, UpdateLoan.class, 0);
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(ints = {0, -1, -2, -3, -5})
    public void shouldFailCarDealershipDtoCreateLoanValidation(final Integer id) {
        shouldValidateCarDealershipDto(id, null, null, null, null, CreateLoan.class, 1);
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(ints = {0, -1, -2, -3, -5})
    public void shouldFailCarDealershipDtoUpdateLoanValidation(final Integer id) {
        shouldValidateCarDealershipDto(id, null, null, null, null, UpdateLoan.class, 1);
    }
}
