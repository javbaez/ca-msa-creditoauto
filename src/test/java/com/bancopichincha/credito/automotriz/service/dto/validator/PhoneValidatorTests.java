package com.bancopichincha.credito.automotriz.service.dto.validator;

import com.bancopichincha.credito.automotriz.service.dto.validation.annotation.Phone;
import com.bancopichincha.credito.automotriz.service.dto.validation.validator.PhoneValidator;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Random;

import static org.mockito.Mockito.when;

@SpringBootTest
public class PhoneValidatorTests extends ValidatorTests<Phone, String, PhoneValidator> {
    private static final int PHONE_MIN_LENGTH = 9;
    private static final int PHONE_MAX_LENGTH = 10;
    @Mock
    private Phone annotation;

    private static List<String> phones() {
        return new Random()
            .longs(
                5,
                (long) Math.pow(10, PHONE_MIN_LENGTH - 1),
                Long.parseLong("9".repeat(PHONE_MAX_LENGTH))
            )
            .boxed()
            .map(String::valueOf)
            .toList();
    }

    @Override
    protected PhoneValidator constructValidator() {
        return new PhoneValidator();
    }

    @Override
    protected Phone mockAnnotation() {
        when(annotation.min()).thenReturn(PHONE_MIN_LENGTH);
        when(annotation.max()).thenReturn(PHONE_MAX_LENGTH);
        return annotation;
    }

    @ParameterizedTest
    @NullSource
    @MethodSource("phones")
    public void shouldPassPhoneValidation(final String phone) {
        shouldPassValidation(phone);
    }

    @ParameterizedTest
    @ValueSource(strings = {"", " ", "A", "1", "12", "123", "1234", "12345", "123456", "1234567", "12345678"})
    public void shouldFailPhoneValidation(final String phone) {
        shouldFailValidation(phone);
    }
}
