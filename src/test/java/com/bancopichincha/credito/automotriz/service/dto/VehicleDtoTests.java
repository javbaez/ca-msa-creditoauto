package com.bancopichincha.credito.automotriz.service.dto;

import com.bancopichincha.credito.automotriz.service.dto.validation.group.CreateLoan;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.CreateVehicle;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.UpdateLoan;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.UpdateVehicle;
import com.bancopichincha.credito.automotriz.service.mapper.BrandMapper;
import com.bancopichincha.credito.automotriz.util.BrandUtil;
import com.bancopichincha.credito.automotriz.util.ValidatorUtil;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@SpringBootTest
public class VehicleDtoTests {
    @Autowired
    private BrandMapper brandMapper;

    private void shouldValidateVehicleDto(
        final Integer id,
        final String plate,
        final String model,
        final String chassisNumber,
        final String type,
        final BigDecimal cylinderCapacity,
        final Integer appraisal,
        final Integer brandId,
        final Class<?> validationGroup,
        final int minNumberOfErrors,
        final int maxNumberOfErrors
    ) {
        final VehicleDto vehicleDto = new VehicleDto();
        vehicleDto.setId(id);
        vehicleDto.setPlate(plate);
        vehicleDto.setModel(model);
        vehicleDto.setChassisNumber(chassisNumber);
        vehicleDto.setType(type);
        vehicleDto.setCylinderCapacity(cylinderCapacity);
        vehicleDto.setAppraisal(appraisal);
        vehicleDto.setBrand(
            brandId != null ?
                brandMapper.brandToBrandDto(BrandUtil.of(brandId, "Marca test")) :
                null
        );
        assertThat(
            ValidatorUtil.validate(vehicleDto, validationGroup).size(),
            allOf(greaterThanOrEqualTo(minNumberOfErrors), lessThanOrEqualTo(maxNumberOfErrors))
        );
    }

    @ParameterizedTest
    @CsvSource({
        "PBG-1234,Rav-4,123445,SUV,2.0,23000,1",
        "ABC-0123,Fiesta,234234,SEDAN,1.6,5000,2",
        "XYZ-9876,F-150,56456546,CAMIONETA,5.7,25000,3",
    })
    public void shouldPassVehicleDtoCreateValidation(
        final String plate,
        final String model,
        final String chassisNumber,
        final String type,
        final BigDecimal cylinderCapacity,
        final Integer appraisal,
        final Integer brandId
    ) {
        shouldValidateVehicleDto(
            null,
            plate,
            model,
            chassisNumber,
            type,
            cylinderCapacity,
            appraisal,
            brandId,
            CreateVehicle.class,
            0,
            0
        );
    }

    @ParameterizedTest
    @CsvSource({
        "1,PBG-1234,Rav-4,123445,SUV,2.0,23000,1",
        "2,ABC-0123,Fiesta,234234,SEDAN,1.6,5000,2",
        "3,XYZ-9876,F-150,56456546,CAMIONETA,5.7,25000,3",
    })
    public void shouldPassVehicleDtoUpdateValidation(
        final Integer id,
        final String plate,
        final String model,
        final String chassisNumber,
        final String type,
        final BigDecimal cylinderCapacity,
        final Integer appraisal,
        final Integer brandId
    ) {
        shouldValidateVehicleDto(
            id,
            plate,
            model,
            chassisNumber,
            type,
            cylinderCapacity,
            appraisal,
            brandId,
            UpdateVehicle.class,
            0,
            0
        );
    }

    @ParameterizedTest
    @CsvSource(value = {
        "null,null,null,null,null,null,null",
        ",,,,,,",
        " , , , , , , ",
        "ABC123,null,null,null,0.0,0,null",
        "ABC1234,null,null,null,20.01,100000001,null",
        "ABC-123,null,null,null,null,null,null",
        "null,MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM,null,null,null,null,null",
        "null,null,CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC,null,null,null,null",
        "null,null,null,TTTTTTTTTTTTTTTTTTTTTTTTTT,null,null,null"
    }, nullValues = {"null"})
    public void shouldFailVehicleDtoCreateValidation(
        final String plate,
        final String model,
        final String chassisNumber,
        final String type,
        final BigDecimal cylinderCapacity,
        final Integer appraisal,
        final Integer brandId
    ) {
        shouldValidateVehicleDto(
            null,
            plate,
            model,
            chassisNumber,
            type,
            cylinderCapacity,
            appraisal,
            brandId,
            CreateVehicle.class,
            6,
            7
        );
    }

    @ParameterizedTest
    @CsvSource(value = {
        "null,null,null,null,null,null,null,null",
        ",,,,,,,",
        " , , , , , , , ",
        "0,ABC123,null,null,null,0.0,0,null",
        "-1,ABC1234,null,null,null,20.01,100000001,null",
        "-2,ABC-123,null,null,null,null,null,null",
        "null,null,MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM,null,null,null,null,null",
        "null,null,null,CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC,null,null,null,null",
        "null,null,null,null,TTTTTTTTTTTTTTTTTTTTTTTTTT,null,null,null"
    }, nullValues = {"null"})
    public void shouldFailVehicleDtoUpdateValidation(
        final Integer id,
        final String plate,
        final String model,
        final String chassisNumber,
        final String type,
        final BigDecimal cylinderCapacity,
        final Integer appraisal,
        final Integer brandId
    ) {
        shouldValidateVehicleDto(
            id,
            plate,
            model,
            chassisNumber,
            type,
            cylinderCapacity,
            appraisal,
            brandId,
            UpdateVehicle.class,
            7,
            8
        );
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5})
    public void shouldPassVehicleDtoCreateLoanValidation(final Integer id) {
        shouldValidateVehicleDto(
            id,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            CreateLoan.class,
            0,
            0
        );
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 2, 3, 4, 5})
    public void shouldPassVehicleDtoUpdateLoanValidation(final Integer id) {
        shouldValidateVehicleDto(
            id,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            UpdateLoan.class,
            0,
            0
        );
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(ints = {0, -1, -2, -3, -5})
    public void shouldFailVehicleDtoCreateLoanValidation(final Integer id) {
        shouldValidateVehicleDto(
            id,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            CreateLoan.class,
            1,
            1
        );
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(ints = {0, -1, -2, -3, -5})
    public void shouldFailVehicleDtoUpdateLoanValidation(final Integer id) {
        shouldValidateVehicleDto(
            id,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            UpdateLoan.class,
            1,
            1
        );
    }
}
