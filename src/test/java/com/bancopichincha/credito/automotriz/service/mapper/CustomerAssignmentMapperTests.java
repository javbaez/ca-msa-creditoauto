package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.CustomerAssignment;
import com.bancopichincha.credito.automotriz.service.dto.CustomerAssignmentDto;
import com.bancopichincha.credito.automotriz.util.CustomerAssignmentUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static com.bancopichincha.credito.automotriz.util.CarDealershipUtil.expectCarDealership;
import static com.bancopichincha.credito.automotriz.util.CustomerUtil.expectCustomer;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SpringBootTest
public class CustomerAssignmentMapperTests {
    private static final Integer ID = 1;
    @Autowired
    private CustomerAssignmentMapper customerAssignmentMapper;

    private void expectCustomerAssignment(
        final CustomerAssignment customerAssignment,
        final CustomerAssignmentDto customerAssignmentDto
    ) {
        assertThat(customerAssignment.getId(), is(customerAssignmentDto.getId()));
        expectCustomer(customerAssignment.getCustomer(), customerAssignmentDto.getCustomer());
        expectCarDealership(customerAssignment.getCarDealership(), customerAssignmentDto.getCarDealership());
        assertThat(customerAssignment.getAssignmentDate(), is(customerAssignmentDto.getAssignmentDate()));
    }

    @Test
    public void givenCustomerAssignmentWhenMapCustomerAssignmentThenCustomerAssignmentDto() {
        final CustomerAssignment customerAssignment = CustomerAssignmentUtil.of(ID);
        final CustomerAssignmentDto customerAssignmentDto = customerAssignmentMapper
            .customerAssignmentToCustomerAssignmentDto(customerAssignment);
        expectCustomerAssignment(customerAssignment, customerAssignmentDto);
    }

    @Test
    public void givenCustomerAssignmentDtoWhenMapCustomerAssignmentDtoThenCustomerAssignment() {
        final CustomerAssignmentDto customerAssignmentDto = customerAssignmentMapper
            .customerAssignmentToCustomerAssignmentDto(CustomerAssignmentUtil.of(ID));
        final CustomerAssignment customerAssignment = customerAssignmentMapper
            .customerAssignmentDtoToCustomerAssignment(customerAssignmentDto);
        expectCustomerAssignment(customerAssignment, customerAssignmentDto);
    }

    @Test
    public void givenCustomerAssignmentAndDtoWhenUpdateCustomerAssignmentWithDtoThenMatchCustomerAssignmentWithDto() {
        final CustomerAssignment customerAssignment = CustomerAssignmentUtil.of(ID);
        final CustomerAssignmentDto customerAssignmentDto = customerAssignmentMapper
            .customerAssignmentToCustomerAssignmentDto(customerAssignment);
        customerAssignmentDto.setId(customerAssignmentDto.getId() + 1);
        customerAssignmentDto.setAssignmentDate(customerAssignmentDto.getAssignmentDate().plusDays(1));
        customerAssignmentMapper.updateCustomerAssignmentFromCustomerAssignmentDto(customerAssignment, customerAssignmentDto);
        expectCustomerAssignment(customerAssignment, customerAssignmentDto);
    }
}
