package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.LoanApplication;
import com.bancopichincha.credito.automotriz.domain.enums.LoanStatus;
import com.bancopichincha.credito.automotriz.service.dto.LoanApplicationDto;
import com.bancopichincha.credito.automotriz.util.LoanApplicationUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

import static com.bancopichincha.credito.automotriz.util.CarDealershipUtil.expectCarDealership;
import static com.bancopichincha.credito.automotriz.util.CustomerUtil.expectCustomer;
import static com.bancopichincha.credito.automotriz.util.SalesExecutiveUtil.expectSalesExecutive;
import static com.bancopichincha.credito.automotriz.util.VehicleUtil.expectVehicle;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SpringBootTest
public class LoanApplicationMapperTests {
    private static final Integer ID = 1;
    @Autowired
    private LoanApplicationMapper loanApplicationMapper;

    private void expectLoanApplication(
        final LoanApplication loanApplication,
        final LoanApplicationDto loanApplicationDto
    ) {
        assertThat(loanApplication.getId(), is(loanApplicationDto.getId()));
        assertThat(loanApplication.getDate(), is(loanApplicationDto.getDate()));
        expectCustomer(loanApplication.getCustomer(), loanApplicationDto.getCustomer());
        expectCarDealership(loanApplication.getCarDealership(), loanApplicationDto.getCarDealership());
        expectSalesExecutive(loanApplication.getSalesExecutive(), loanApplicationDto.getSalesExecutive());
        expectVehicle(loanApplication.getVehicle(), loanApplicationDto.getVehicle());
        assertThat(loanApplication.getMonthsTerm(), is(loanApplicationDto.getMonthsTerm()));
        assertThat(loanApplication.getInstallments(), is(loanApplicationDto.getInstallments()));
        assertThat(loanApplication.getDownPayment(), is(loanApplicationDto.getDownPayment()));
        assertThat(loanApplication.getObservation(), is(loanApplicationDto.getObservation()));
        assertThat(loanApplication.getStatus(), is(loanApplicationDto.getStatus()));
    }

    @Test
    public void givenLoanApplicationWhenMapLoanApplicationThenLoanApplicationDto() {
        final LoanApplication loanApplication = LoanApplicationUtil.of(ID);
        final LoanApplicationDto loanApplicationDto = loanApplicationMapper
            .loanApplicationToLoanApplicationDto(loanApplication);
        expectLoanApplication(loanApplication, loanApplicationDto);
    }

    @Test
    public void givenLoanApplicationDtoWhenMapLoanApplicationDtoThenLoanApplication() {
        final LoanApplicationDto loanApplicationDto = loanApplicationMapper
            .loanApplicationToLoanApplicationDto(LoanApplicationUtil.of(ID));
        final LoanApplication loanApplication = loanApplicationMapper
            .loanApplicationDtoToLoanApplication(loanApplicationDto);
        expectLoanApplication(loanApplication, loanApplicationDto);
    }

    @Test
    public void givenLoanApplicationAndDtoWhenUpdateLoanApplicationWithDtoThenMatchLoanApplicationWithDto() {
        final LoanApplication loanApplication = LoanApplicationUtil.of(ID);
        final LoanApplicationDto loanApplicationDto = loanApplicationMapper
            .loanApplicationToLoanApplicationDto(loanApplication);
        loanApplicationDto.setId(loanApplicationDto.getId() + 1);
        loanApplicationDto.setDate(loanApplicationDto.getDate().plusDays(1));
        loanApplicationDto.setMonthsTerm((byte) (loanApplicationDto.getMonthsTerm() + 1));
        loanApplicationDto.setInstallments((short) (loanApplicationDto.getInstallments() + 1));
        loanApplicationDto.setDownPayment(loanApplicationDto.getDownPayment().add(BigDecimal.ONE));
        loanApplicationDto.setObservation(loanApplicationDto.getObservation() + "1");
        loanApplicationDto.setStatus(LoanStatus.CANCELLED);
        loanApplicationMapper.updateLoanApplicationFromLoanApplicationDto(loanApplication, loanApplicationDto);
        expectLoanApplication(loanApplication, loanApplicationDto);
    }
}
