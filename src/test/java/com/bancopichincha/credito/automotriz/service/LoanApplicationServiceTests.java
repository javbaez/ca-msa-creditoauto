package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.domain.CarDealership;
import com.bancopichincha.credito.automotriz.domain.LoanApplication;
import com.bancopichincha.credito.automotriz.domain.enums.LoanStatus;
import com.bancopichincha.credito.automotriz.exception.*;
import com.bancopichincha.credito.automotriz.repository.*;
import com.bancopichincha.credito.automotriz.service.dto.*;
import com.bancopichincha.credito.automotriz.service.impl.LoanApplicationServiceImpl;
import com.bancopichincha.credito.automotriz.service.mapper.LoanApplicationMapper;
import com.bancopichincha.credito.automotriz.service.mapper.SalesExecutiveMapper;
import com.bancopichincha.credito.automotriz.util.LoanApplicationUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class LoanApplicationServiceTests {
    private static final Integer ID = 1;
    @MockBean
    private LoanApplicationMapper loanApplicationMapper;
    @MockBean
    private SalesExecutiveMapper salesExecutiveMapper;
    @MockBean
    private LoanApplicationRepository loanApplicationRepository;
    @MockBean
    private CustomerRepository customerRepository;
    @MockBean
    private CarDealershipRepository carDealershipRepository;
    @MockBean
    private SalesExecutiveRepository salesExecutiveRepository;
    @MockBean
    private VehicleRepository vehicleRepository;
    @InjectMocks
    @Autowired
    private LoanApplicationServiceImpl loanApplicationService;

    private LoanApplication getLoanApplication(final Integer id) {
        return LoanApplicationUtil.of(id);
    }

    private LoanApplicationDto getLoanApplicationDto(final LoanApplication loanApplication) {
        final LoanApplicationDto loanApplicationDto = new LoanApplicationDto();
        final CustomerDto customerDto = new CustomerDto();
        final CarDealershipDto carDealershipDto = new CarDealershipDto();
        final SalesExecutiveDto salesExecutiveDto = new SalesExecutiveDto();
        final VehicleDto vehicleDto = new VehicleDto();
        customerDto.setId(loanApplication.getCustomer().getId());
        carDealershipDto.setId(loanApplication.getCarDealership().getId());
        salesExecutiveDto.setId(loanApplication.getSalesExecutive().getId());
        vehicleDto.setId(loanApplication.getVehicle().getId());
        loanApplicationDto.setId(loanApplication.getId());
        loanApplicationDto.setCustomer(customerDto);
        loanApplicationDto.setCarDealership(carDealershipDto);
        loanApplicationDto.setSalesExecutive(salesExecutiveDto);
        loanApplicationDto.setVehicle(vehicleDto);
        loanApplicationDto.setDate(loanApplication.getDate());
        loanApplicationDto.setMonthsTerm(loanApplication.getMonthsTerm());
        loanApplicationDto.setInstallments(loanApplication.getInstallments());
        loanApplicationDto.setDownPayment(loanApplication.getDownPayment());
        loanApplicationDto.setObservation(loanApplication.getObservation());
        loanApplicationDto.setStatus(loanApplication.getStatus());
        return loanApplicationDto;
    }

    private void expectLoanApplicationDto(
        final boolean validateId,
        final LoanApplicationDto inputDto,
        final LoanApplicationDto resultDto
    ) {
        if (validateId) {
            assertThat(resultDto.getId(), is(inputDto.getId()));
        }
        assertThat(resultDto.getCustomer().getId(), is(inputDto.getCustomer().getId()));
        assertThat(resultDto.getCarDealership().getId(), is(inputDto.getCarDealership().getId()));
        assertThat(resultDto.getSalesExecutive().getId(), is(inputDto.getSalesExecutive().getId()));
        assertThat(resultDto.getVehicle().getId(), is(inputDto.getVehicle().getId()));
        assertThat(resultDto.getDate(), is(inputDto.getDate()));
        assertThat(resultDto.getMonthsTerm(), is(inputDto.getMonthsTerm()));
        assertThat(resultDto.getInstallments(), is(inputDto.getInstallments()));
        assertThat(resultDto.getDownPayment(), is(inputDto.getDownPayment()));
        assertThat(resultDto.getObservation(), is(inputDto.getObservation()));
        assertThat(resultDto.getStatus(), is(inputDto.getStatus()));
    }

    @Test
    public void shouldFindLoanApplication() {
        final LoanApplication loanApplication = getLoanApplication(ID);
        final LoanApplicationDto loanApplicationDto = getLoanApplicationDto(loanApplication);
        when(loanApplicationMapper.loanApplicationToLoanApplicationDto(any(LoanApplication.class)))
            .thenReturn(loanApplicationDto);
        when(loanApplicationRepository.findById(any(Integer.class))).thenReturn(Optional.of(loanApplication));
        final LoanApplicationDto loanApplicationDtoFound =
            assertDoesNotThrow(() -> loanApplicationService.findById(loanApplication.getId()));
        assertThat(loanApplicationDtoFound, notNullValue());
        assertThat(loanApplicationDtoFound.getId(), is(loanApplication.getId()));
    }

    private void shouldCreateLoanApplication(
        final LoanApplication loanApplication,
        final boolean findCustomer,
        final boolean checkIfCustomerHasValidApplicationByDate,
        final boolean findVehicle,
        final boolean checkIfVehicleIsReserved,
        final boolean findCarDealership,
        final boolean findSalesExecutive
    ) {
        final LoanApplicationDto loanApplicationDto = getLoanApplicationDto(loanApplication);
        when(loanApplicationMapper.loanApplicationDtoToLoanApplication(any(LoanApplicationDto.class)))
            .thenReturn(loanApplication);
        when(loanApplicationMapper.loanApplicationToLoanApplicationDto(any(LoanApplication.class)))
            .thenReturn(loanApplicationDto);
        when(customerRepository.findById(any(Integer.class))).thenReturn(
            findCustomer ?
                Optional.of(loanApplication.getCustomer()) :
                Optional.empty()
        );
        when(loanApplicationRepository
            .checkIfCustomerHasValidApplicationByDate(any(Integer.class), any(LocalDate.class)))
            .thenReturn(checkIfCustomerHasValidApplicationByDate);
        when(vehicleRepository.findById(any(Integer.class))).thenReturn(
            findVehicle ?
                Optional.of(loanApplication.getVehicle()) :
                Optional.empty()
        );
        when(loanApplicationRepository.checkIfVehicleIsReserved(any(Integer.class))).thenReturn(checkIfVehicleIsReserved);
        when(carDealershipRepository.findById(any(Integer.class))).thenReturn(
            findCarDealership ?
                Optional.of(loanApplication.getCarDealership()) :
                Optional.empty()
        );
        when(salesExecutiveRepository.findByIdAndCarDealership(any(Integer.class), any(CarDealership.class)))
            .thenReturn(
                findSalesExecutive ?
                    Optional.of(loanApplication.getSalesExecutive()) :
                    Optional.empty()
            );
        when(loanApplicationRepository.save(any(LoanApplication.class))).thenReturn(loanApplication);
        Class<? extends Throwable> exceptionClass = null;
        if (!findCustomer) {
            exceptionClass = CustomerNotFoundException.class;
        } else if (checkIfCustomerHasValidApplicationByDate) {
            exceptionClass = LoanApplicationExistsException.class;
        } else if (!findVehicle) {
            exceptionClass = VehicleNotFoundException.class;
        } else if (checkIfVehicleIsReserved) {
            exceptionClass = VehicleReservedForAnotherLoanException.class;
        } else if (!findCarDealership) {
            exceptionClass = CarDealershipNotFoundException.class;
        } else if (!findSalesExecutive) {
            exceptionClass = SalesExecutiveNotFoundException.class;
        }
        if (exceptionClass != null) {
            assertThrows(exceptionClass, () -> loanApplicationService.create(loanApplicationDto));
        } else {
            final LoanApplicationDto loanApplicationDtoCreated =
                assertDoesNotThrow(() -> loanApplicationService.create(loanApplicationDto));
            expectLoanApplicationDto(false, loanApplicationDto, loanApplicationDtoCreated);
        }
    }

    @Test
    public void shouldCreateLoanApplication() {
        shouldCreateLoanApplication(
            getLoanApplication(null),
            true,
            false,
            true,
            false,
            true,
            true
        );
    }

    @Test
    public void shouldThrowCustomerNotFoundExceptionWhenCreateLoanApplication() {
        shouldCreateLoanApplication(
            getLoanApplication(null),
            false,
            false,
            true,
            false,
            true,
            true
        );
    }

    @Test
    public void shouldThrowLoanApplicationExistsExceptionWhenCreateLoanApplication() {
        shouldCreateLoanApplication(
            getLoanApplication(null),
            true,
            true,
            true,
            false,
            true,
            true
        );
    }

    @Test
    public void shouldThrowVehicleNotFoundExceptionWhenCreateLoanApplication() {
        shouldCreateLoanApplication(
            getLoanApplication(null),
            true,
            false,
            false,
            false,
            true,
            true
        );
    }

    @Test
    public void shouldThrowVehicleReservedForAnotherLoanExceptionWhenCreateLoanApplication() {
        shouldCreateLoanApplication(
            getLoanApplication(null),
            true,
            false,
            true,
            true,
            true,
            true
        );
    }

    @Test
    public void shouldThrowCarDealershipNotFoundExceptionWhenCreateLoanApplication() {
        shouldCreateLoanApplication(
            getLoanApplication(null),
            true,
            false,
            true,
            false,
            false,
            true
        );
    }

    @Test
    public void shouldThrowSalesExecutiveNotFoundExceptionWhenCreateLoanApplication() {
        shouldCreateLoanApplication(
            getLoanApplication(null),
            true,
            false,
            true,
            false,
            true,
            false
        );
    }

    private void shouldUpdateLoanApplication(
        final LoanApplication loanApplication,
        final boolean findLoanApplication,
        final boolean findCustomer,
        final boolean findVehicle,
        final boolean checkIfVehicleIsReserved,
        final boolean findCarDealership,
        final boolean findSalesExecutive
    ) {
        final LoanApplicationDto loanApplicationDto = getLoanApplicationDto(loanApplication);
        loanApplicationDto.setDate(loanApplicationDto.getDate().plusDays(1));
        loanApplicationDto.setMonthsTerm((byte) (loanApplicationDto.getMonthsTerm() + 1));
        loanApplicationDto.setInstallments((short) (loanApplicationDto.getInstallments() + 1));
        loanApplicationDto.setDownPayment(loanApplicationDto.getDownPayment().add(BigDecimal.ONE));
        loanApplicationDto.setObservation(loanApplicationDto.getObservation() + "1");
        loanApplicationDto.setStatus(LoanStatus.CANCELLED);
        loanApplicationDto.getCustomer().setId(loanApplicationDto.getCustomer().getId() + 1);
        loanApplicationDto.getCarDealership().setId(loanApplicationDto.getCarDealership().getId() + 1);
        loanApplicationDto.getSalesExecutive().setId(loanApplicationDto.getSalesExecutive().getId() + 1);
        loanApplicationDto.getVehicle().setId(loanApplicationDto.getVehicle().getId() + 1);
        when(loanApplicationMapper.loanApplicationToLoanApplicationDto(any(LoanApplication.class)))
            .thenReturn(loanApplicationDto);
        doNothing().when(loanApplicationMapper)
            .updateLoanApplicationFromLoanApplicationDto(any(LoanApplication.class), any(LoanApplicationDto.class));
        when(loanApplicationRepository.findById(any(Integer.class))).thenReturn(
            findLoanApplication ?
                Optional.of(loanApplication) :
                Optional.empty()
        );
        when(customerRepository.findById(any(Integer.class))).thenReturn(
            findCustomer ?
                Optional.of(loanApplication.getCustomer()) :
                Optional.empty()
        );
        when(vehicleRepository.findById(any(Integer.class))).thenReturn(
            findVehicle ?
                Optional.of(loanApplication.getVehicle()) :
                Optional.empty()
        );
        when(loanApplicationRepository.checkIfVehicleIsReserved(any(Integer.class))).thenReturn(checkIfVehicleIsReserved);
        when(carDealershipRepository.findById(any(Integer.class))).thenReturn(
            findCarDealership ?
                Optional.of(loanApplication.getCarDealership()) :
                Optional.empty()
        );
        when(salesExecutiveRepository.findByIdAndCarDealership(any(Integer.class), any(CarDealership.class)))
            .thenReturn(
                findSalesExecutive ?
                    Optional.of(loanApplication.getSalesExecutive()) :
                    Optional.empty()
            );
        when(loanApplicationRepository.save(any(LoanApplication.class))).thenReturn(loanApplication);
        Class<? extends Throwable> exceptionClass = null;
        if (!findLoanApplication) {
            exceptionClass = LoanApplicationNotFoundException.class;
        } if (!findCustomer) {
            exceptionClass = CustomerNotFoundException.class;
        } if (!findVehicle) {
            exceptionClass = VehicleNotFoundException.class;
        } else if (checkIfVehicleIsReserved) {
            exceptionClass = VehicleReservedForAnotherLoanException.class;
        } else if (!findCarDealership) {
            exceptionClass = CarDealershipNotFoundException.class;
        } else if (!findSalesExecutive) {
            exceptionClass = SalesExecutiveNotFoundException.class;
        }
        if (exceptionClass != null) {
            assertThrows(exceptionClass, () -> loanApplicationService.update(loanApplicationDto));
        } else {
            final LoanApplicationDto loanApplicationDtoUpdated =
                assertDoesNotThrow(() -> loanApplicationService.update(loanApplicationDto));
            expectLoanApplicationDto(true, loanApplicationDto, loanApplicationDtoUpdated);
        }
    }

    @Test
    public void shouldUpdateLoanApplication() {
        shouldUpdateLoanApplication(
            getLoanApplication(ID),
            true,
            true,
            true,
            false,
            true,
            true
        );
    }

    @Test
    public void shouldThrowLoanApplicationNotFoundExceptionWhenUpdateLoanApplication() {
        shouldUpdateLoanApplication(
            getLoanApplication(ID),
            false,
            true,
            true,
            false,
            true,
            true
        );
    }

    @Test
    public void shouldThrowCustomerNotFoundExceptionWhenUpdateLoanApplication() {
        shouldUpdateLoanApplication(
            getLoanApplication(ID),
            true,
            false,
            true,
            false,
            true,
            true
        );
    }

    @Test
    public void shouldThrowVehicleReservedForAnotherLoanExceptionWhenUpdateLoanApplication() {
        shouldUpdateLoanApplication(
            getLoanApplication(ID),
            true,
            true,
            true,
            true,
            true,
            true
        );
    }

    @Test
    public void shouldThrowVehicleNotFoundExceptionWhenUpdateLoanApplication() {
        shouldUpdateLoanApplication(
            getLoanApplication(ID),
            true,
            true,
            false,
            false,
            true,
            true
        );
    }

    @Test
    public void shouldThrowCarDealershipNotFoundExceptionWhenUpdateLoanApplication() {
        shouldUpdateLoanApplication(
            getLoanApplication(ID),
            true,
            true,
            true,
            false,
            false,
            true
        );
    }

    @Test
    public void shouldThrowSalesExecutiveNotFoundExceptionWhenUpdateLoanApplication() {
        shouldUpdateLoanApplication(
            getLoanApplication(ID),
            true,
            true,
            true,
            false,
            true,
            false
        );
    }

    private void shouldDeleteLoanApplication(final LoanApplication loanApplication, final boolean findLoanApplication) {
        final LoanApplicationDto loanApplicationDto = getLoanApplicationDto(loanApplication);
        when(loanApplicationMapper.loanApplicationToLoanApplicationDto(any(LoanApplication.class)))
            .thenReturn(loanApplicationDto);
        when(loanApplicationRepository.findById(any(Integer.class))).thenReturn(
            findLoanApplication ?
                Optional.of(loanApplication) :
                Optional.empty()
        );
        doNothing().when(loanApplicationRepository).deleteById(any(Integer.class));
        if (!findLoanApplication) {
            assertThrows(LoanApplicationNotFoundException.class, () -> loanApplicationService.delete(loanApplication.getId()));
        } else {
            final LoanApplicationDto loanApplicationDtoDeleted =
                assertDoesNotThrow(() -> loanApplicationService.delete(loanApplication.getId()));
            assertThat(loanApplicationDtoDeleted, notNullValue());
            assertThat(loanApplicationDtoDeleted.getId(), is(loanApplication.getId()));
        }
    }

    @Test
    public void shouldDeleteLoanApplication() {
        shouldDeleteLoanApplication(getLoanApplication(ID), true);
    }

    @Test
    public void shouldThrowLoanApplicationNotFoundExceptionWhenDeleteLoanApplication() {
        shouldDeleteLoanApplication(getLoanApplication(ID), false);
    }
}
