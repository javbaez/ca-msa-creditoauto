package com.bancopichincha.credito.automotriz.service.dto.validator;

import com.bancopichincha.credito.automotriz.service.dto.validation.annotation.IdNumber;
import com.bancopichincha.credito.automotriz.service.dto.validation.validator.IdNumberValidator;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Random;

@SpringBootTest
public class IdNumberValidatorTests extends ValidatorTests<IdNumber, String, IdNumberValidator> {
    private static final int ID_NUMBER_LENGTH = 10;
    @Mock
    private IdNumber annotation;

    private static List<String> idNumbers() {
        return new Random()
            .longs(
                5,
                (long) Math.pow(10, ID_NUMBER_LENGTH - 1),
                Long.parseLong("9".repeat(ID_NUMBER_LENGTH))
            )
            .boxed()
            .map(String::valueOf)
            .toList();
    }

    @Override
    protected IdNumberValidator constructValidator() {
        return new IdNumberValidator();
    }

    @Override
    protected IdNumber mockAnnotation() {
        return annotation;
    }

    @ParameterizedTest
    @NullSource
    @MethodSource("idNumbers")
    public void shouldPassIdNumberValidation(final String idNumber) {
        shouldPassValidation(idNumber);
    }

    @ParameterizedTest
    @ValueSource(strings = {"", " ", "A", "1", "12", "123", "1234", "12345", "123456", "1234567", "12345678", "123456789"})
    public void shouldFailIdNumberValidation(final String idNumber) {
        shouldFailValidation(idNumber);
    }
}
