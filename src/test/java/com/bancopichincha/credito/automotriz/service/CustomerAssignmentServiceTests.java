package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.domain.CustomerAssignment;
import com.bancopichincha.credito.automotriz.exception.CarDealershipNotFoundException;
import com.bancopichincha.credito.automotriz.exception.CustomerAssignmentNotFoundException;
import com.bancopichincha.credito.automotriz.exception.CustomerNotFoundException;
import com.bancopichincha.credito.automotriz.repository.CarDealershipRepository;
import com.bancopichincha.credito.automotriz.repository.CustomerAssignmentRepository;
import com.bancopichincha.credito.automotriz.repository.CustomerRepository;
import com.bancopichincha.credito.automotriz.service.dto.CarDealershipDto;
import com.bancopichincha.credito.automotriz.service.dto.CustomerAssignmentDto;
import com.bancopichincha.credito.automotriz.service.dto.CustomerDto;
import com.bancopichincha.credito.automotriz.service.impl.CustomerAssignmentServiceImpl;
import com.bancopichincha.credito.automotriz.service.mapper.CustomerAssignmentMapper;
import com.bancopichincha.credito.automotriz.util.CustomerAssignmentUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class CustomerAssignmentServiceTests {
    private static final Integer ID = 1;
    @MockBean
    private CustomerAssignmentMapper customerAssignmentMapper;
    @MockBean
    private CustomerAssignmentRepository customerAssignmentRepository;
    @MockBean
    private CustomerRepository customerRepository;
    @MockBean
    private CarDealershipRepository carDealershipRepository;
    @InjectMocks
    @Autowired
    private CustomerAssignmentServiceImpl customerAssignmentService;

    private CustomerAssignment getCustomerAssignment(final Integer id) {
        return CustomerAssignmentUtil.of(id);
    }

    private CustomerAssignmentDto getCustomerAssignmentDto(final CustomerAssignment customerAssignment) {
        final CustomerAssignmentDto customerAssignmentDto = new CustomerAssignmentDto();
        final CustomerDto customerDto = new CustomerDto();
        final CarDealershipDto carDealershipDto = new CarDealershipDto();
        customerDto.setId(customerAssignment.getCustomer().getId());
        carDealershipDto.setId(customerAssignment.getCarDealership().getId());
        customerAssignmentDto.setId(customerAssignment.getId());
        customerAssignmentDto.setCustomer(customerDto);
        customerAssignmentDto.setCarDealership(carDealershipDto);
        customerAssignmentDto.setAssignmentDate(customerAssignment.getAssignmentDate());
        return customerAssignmentDto;
    }

    private void expectCustomerAssignmentDto(
        final boolean validateId,
        final CustomerAssignmentDto inputDto,
        final CustomerAssignmentDto resultDto
    ) {
        if (validateId) {
            assertThat(resultDto.getId(), is(inputDto.getId()));
        }
        assertThat(resultDto.getCustomer().getId(), is(inputDto.getCustomer().getId()));
        assertThat(resultDto.getCarDealership().getId(), is(inputDto.getCarDealership().getId()));
        assertThat(resultDto.getAssignmentDate(), is(inputDto.getAssignmentDate()));
    }

    @Test
    public void shouldFindCustomerAssignment() {
        final CustomerAssignment customerAssignment = getCustomerAssignment(ID);
        final CustomerAssignmentDto customerAssignmentDto = getCustomerAssignmentDto(customerAssignment);
        when(customerAssignmentMapper.customerAssignmentToCustomerAssignmentDto(any(CustomerAssignment.class)))
            .thenReturn(customerAssignmentDto);
        when(customerAssignmentRepository.findById(any(Integer.class))).thenReturn(Optional.of(customerAssignment));
        final CustomerAssignmentDto customerAssignmentDtoFound =
            assertDoesNotThrow(() -> customerAssignmentService.findById(customerAssignment.getId()));
        assertThat(customerAssignmentDtoFound, notNullValue());
        assertThat(customerAssignmentDtoFound.getId(), is(customerAssignment.getId()));
    }

    private void shouldCreateCustomerAssignment(
        final CustomerAssignment customerAssignment,
        final boolean findCustomer,
        final boolean findCarDealership
    ) {
        final CustomerAssignmentDto customerAssignmentDto = getCustomerAssignmentDto(customerAssignment);
        when(customerAssignmentMapper
            .customerAssignmentDtoToCustomerAssignment(any(CustomerAssignmentDto.class)))
            .thenReturn(customerAssignment);
        when(customerAssignmentMapper
            .customerAssignmentToCustomerAssignmentDto(any(CustomerAssignment.class)))
            .thenReturn(customerAssignmentDto);
        when(customerRepository.findById(any(Integer.class))).thenReturn(
            findCustomer ?
                Optional.of(customerAssignment.getCustomer()) :
                Optional.empty()
        );
        when(carDealershipRepository.findById(any(Integer.class))).thenReturn(
            findCarDealership ?
                Optional.of(customerAssignment.getCarDealership()) :
                Optional.empty()
        );
        when(customerAssignmentRepository.save(any(CustomerAssignment.class))).thenReturn(customerAssignment);
        Class<? extends Throwable> exceptionClass = null;
        if (!findCustomer) {
            exceptionClass = CustomerNotFoundException.class;
        } else if (!findCarDealership) {
            exceptionClass = CarDealershipNotFoundException.class;
        }
        if (exceptionClass != null) {
            assertThrows(exceptionClass, () -> customerAssignmentService.create(customerAssignmentDto));
        } else {
            final CustomerAssignmentDto customerAssignmentDtoCreated =
                assertDoesNotThrow(() -> customerAssignmentService.create(customerAssignmentDto));
            expectCustomerAssignmentDto(false, customerAssignmentDto, customerAssignmentDtoCreated);
        }
    }

    @Test
    public void shouldCreateCustomerAssignment() {
        shouldCreateCustomerAssignment(getCustomerAssignment(null), true, true);
    }

    @Test
    public void shouldThrowCustomerNotFoundExceptionWhenCreateCustomerAssignment() {
        shouldCreateCustomerAssignment(getCustomerAssignment(null), false, true);
    }

    @Test
    public void shouldThrowCarDealershipNotFoundExceptionWhenCreateCustomerAssignment() {
        shouldCreateCustomerAssignment(getCustomerAssignment(null), true, false);
    }

    private void shouldUpdateCustomerAssignment(
        final CustomerAssignment customerAssignment,
        final boolean findCustomerAssignment,
        final boolean findCustomer,
        final boolean findCarDealership
    ) {
        final CustomerAssignmentDto customerAssignmentDto = getCustomerAssignmentDto(customerAssignment);
        customerAssignmentDto.setAssignmentDate(customerAssignmentDto.getAssignmentDate().plusDays(1));
        customerAssignmentDto.getCustomer().setId(customerAssignmentDto.getCustomer().getId() + 1);
        customerAssignmentDto.getCarDealership().setId(customerAssignmentDto.getCarDealership().getId() + 1);
        when(customerAssignmentMapper
            .customerAssignmentDtoToCustomerAssignment(any(CustomerAssignmentDto.class)))
            .thenReturn(customerAssignment);
        when(customerAssignmentMapper
            .customerAssignmentToCustomerAssignmentDto(any(CustomerAssignment.class)))
            .thenReturn(customerAssignmentDto);
        when(customerAssignmentRepository.findById(any(Integer.class))).thenReturn(
            findCustomerAssignment ?
                Optional.of(customerAssignment) :
                Optional.empty()
        );
        when(customerRepository.findById(any(Integer.class))).thenReturn(
            findCustomer ?
                Optional.of(customerAssignment.getCustomer()) :
                Optional.empty()
        );
        when(carDealershipRepository.findById(any(Integer.class))).thenReturn(
            findCarDealership ?
                Optional.of(customerAssignment.getCarDealership()) :
                Optional.empty()
        );
        when(customerAssignmentRepository.save(any(CustomerAssignment.class))).thenReturn(customerAssignment);
        Class<? extends Throwable> exceptionClass = null;
        if (!findCustomerAssignment) {
            exceptionClass = CustomerAssignmentNotFoundException.class;
        } else if (!findCustomer) {
            exceptionClass = CustomerNotFoundException.class;
        } else if (!findCarDealership) {
            exceptionClass = CarDealershipNotFoundException.class;
        }
        if (exceptionClass != null) {
            assertThrows(exceptionClass, () -> customerAssignmentService.update(customerAssignmentDto));
        } else {
            final CustomerAssignmentDto customerAssignmentDtoUpdated =
                assertDoesNotThrow(() -> customerAssignmentService.update(customerAssignmentDto));
            expectCustomerAssignmentDto(true, customerAssignmentDto, customerAssignmentDtoUpdated);
        }
    }

    @Test
    public void shouldUpdateCustomerAssignment() {
        shouldUpdateCustomerAssignment(getCustomerAssignment(ID), true, true, true);
    }

    @Test
    public void shouldThrowCustomerAssignmentNotFoundExceptionWhenUpdateCustomerAssignment() {
        shouldUpdateCustomerAssignment(getCustomerAssignment(ID), false, true, true);
    }

    @Test
    public void shouldThrowCustomerNotFoundExceptionWhenUpdateCustomerAssignment() {
        shouldUpdateCustomerAssignment(getCustomerAssignment(ID), true, false, true);
    }

    @Test
    public void shouldThrowCarDealershipNotFoundExceptionWhenUpdateCustomerAssignment() {
        shouldUpdateCustomerAssignment(getCustomerAssignment(ID), true, true, false);
    }

    private void shouldDeleteCustomerAssignment(final CustomerAssignment customerAssignment, final boolean findCustomerAssignment) {
        final CustomerAssignmentDto customerAssignmentDto = getCustomerAssignmentDto(customerAssignment);
        when(customerAssignmentMapper
            .customerAssignmentToCustomerAssignmentDto(any(CustomerAssignment.class)))
            .thenReturn(customerAssignmentDto);
        when(customerAssignmentRepository.findById(any(Integer.class))).thenReturn(
            findCustomerAssignment ?
                Optional.of(customerAssignment) :
                Optional.empty()
        );
        doNothing().when(customerAssignmentRepository).deleteById(any(Integer.class));
        if (!findCustomerAssignment) {
            assertThrows(
                CustomerAssignmentNotFoundException.class,
                () -> customerAssignmentService.delete(customerAssignment.getId())
            );
        } else {
            final CustomerAssignmentDto customerAssignmentDtoDeleted =
                assertDoesNotThrow(() -> customerAssignmentService.delete(customerAssignment.getId()));
            assertThat(customerAssignmentDtoDeleted, notNullValue());
            assertThat(customerAssignmentDtoDeleted.getId(), is(customerAssignment.getId()));
        }
    }

    @Test
    public void shouldDeleteCustomerAssignment() {
        shouldDeleteCustomerAssignment(getCustomerAssignment(ID), true);
    }

    @Test
    public void shouldThrowCustomerAssignmentNotFoundExceptionWhenDeleteCustomerAssignment() {
        shouldDeleteCustomerAssignment(getCustomerAssignment(ID), false);
    }
}
