package com.bancopichincha.credito.automotriz.domain;

import com.bancopichincha.credito.automotriz.util.CarDealershipUtil;
import com.bancopichincha.credito.automotriz.util.CustomerUtil;
import com.bancopichincha.credito.automotriz.util.ValidatorUtil;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SpringBootTest
public class CustomerAssignmentTests {
    private void shouldValidateCustomerAssignment(
        final Integer customerId,
        final Integer carDealershipId,
        final LocalDate assignmentDate,
        final int numberOfErrors
    ) {
        final CustomerAssignment customerAssignment = new CustomerAssignment();
        customerAssignment.setCustomer(customerId != null ? CustomerUtil.of(customerId, "1000000000") : null);
        customerAssignment.setCarDealership(carDealershipId != null ? CarDealershipUtil.of(carDealershipId) : null);
        customerAssignment.setAssignmentDate(assignmentDate);
        assertThat(ValidatorUtil.validate(customerAssignment).size(), is(numberOfErrors));
    }

    @ParameterizedTest
    @CsvSource({
        "1,1,2023-05-01",
        "2,2,2023-05-02",
        "3,3,2023-05-03",
        "4,4,2023-05-04",
        "5,5,2023-05-05"
    })
    public void shouldPassCustomerAssignmentValidation(
        final Integer customerId,
        final Integer carDealershipId,
        final LocalDate assignmentDate
    ) {
        shouldValidateCustomerAssignment(customerId, carDealershipId, assignmentDate, 0);
    }

    @ParameterizedTest
    @CsvSource(value = {
        "null,null,null",
        ",,",
        " , , ",
        "null,null,2100-01-01"
    }, nullValues = {"null"})
    public void shouldFailCustomerAssignmentValidation(
        final Integer customerId,
        final Integer carDealershipId,
        final LocalDate assignmentDate
    ) {
        shouldValidateCustomerAssignment(customerId, carDealershipId, assignmentDate, 3);
    }
}
