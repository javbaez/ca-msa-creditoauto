package com.bancopichincha.credito.automotriz.domain;

import com.bancopichincha.credito.automotriz.util.CarDealershipUtil;
import com.bancopichincha.credito.automotriz.util.ValidatorUtil;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SpringBootTest
public class SalesExecutiveTests {
    private void shouldValidateSalesExecutive(
        final String idNumber,
        final String names,
        final String lastNames,
        final String address,
        final String landline,
        final String mobile,
        final Byte age,
        final Integer carDealershipId,
        final int numberOfErrors
    ) {
        final SalesExecutive salesExecutive = new SalesExecutive();
        salesExecutive.setIdNumber(idNumber);
        salesExecutive.setNames(names);
        salesExecutive.setLastNames(lastNames);
        salesExecutive.setAge(age);
        salesExecutive.setAddress(address);
        salesExecutive.setLandline(landline);
        salesExecutive.setMobile(mobile);
        salesExecutive.setCarDealership(carDealershipId != null ? CarDealershipUtil.of(carDealershipId) : null);
        assertThat(ValidatorUtil.validate(salesExecutive).size(), is(numberOfErrors));
    }

    @ParameterizedTest
    @CsvSource({
        "1707943039,PEDRO,GONZALEZ,QUITUMBE ÑAN Y AMARU ÑAN,022123456,0973123456,20,1",
        "1309537262,RODRIGO,PIEDRAITA,12 DE OCTUBRE Y PATRIA,022890123,0973890123,60,7",
        "1900150507,KARINA,RUEDA,ROBLES Y ULPIANO PAEZ,023098765,0983098765,26,10",
        "1306901719,MANUELA,SANCHEZ,RUMIPAMBA Y BURGEOIS,023654321,0983654321,33,4"
    })
    public void shouldPassSalesExecutiveValidation(
        final String idNumber,
        final String names,
        final String lastNames,
        final String address,
        final String landline,
        final String mobile,
        final Byte age,
        final Integer carDealershipId
    ) {
        shouldValidateSalesExecutive(
            idNumber,
            names,
            lastNames,
            address,
            landline,
            mobile,
            age,
            carDealershipId,
            0
        );
    }

    @ParameterizedTest
    @CsvSource(value = {
        "null,null,null,null,null,null,null,null",
        ",,,,,,,",
        " , , , , , , , ",
        "123456789,null,null,null,12345678,123456789,12,null",
        "A,null,null,null,1234567890,12345678910,116,null",
        "?,null,null,null,A,B,null,null",
        "null,NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN,null,null,null,null,null,null",
        "null,null,AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,null,null,null,null,null",
        "null,null,null,DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD,null,null,null,null",
    }, nullValues = {"null"})
    public void shouldFailSalesExecutiveValidation(
        final String idNumber,
        final String names,
        final String lastNames,
        final String address,
        final String landline,
        final String mobile,
        final Byte age,
        final Integer carDealershipId
    ) {
        shouldValidateSalesExecutive(
            idNumber,
            names,
            lastNames,
            address,
            landline,
            mobile,
            age,
            carDealershipId,
            8
        );
    }
}
