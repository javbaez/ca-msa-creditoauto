package com.bancopichincha.credito.automotriz.domain;

import com.bancopichincha.credito.automotriz.util.ValidatorUtil;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SpringBootTest
public class CarDealershipTests {
    private void shouldValidateCarDealership(
        final String name,
        final String address,
        final String phone,
        final Integer pointOfSaleNumber,
        final int numberOfErrors
    ) {
        final CarDealership carDealership = new CarDealership();
        carDealership.setName(name);
        carDealership.setAddress(address);
        carDealership.setPhone(phone);
        carDealership.setPointOfSaleNumber(pointOfSaleNumber);
        assertThat(ValidatorUtil.validate(carDealership).size(), is(numberOfErrors));
    }

    @ParameterizedTest
    @CsvSource({
        "AUTOVENTURA,AV. OSWALDO GUAYASAMIN,022370034,1234",
        "CABRERA AUTOS,AV. 10 DE AGOSTO Y MARIANA DE JESUS,0983351392,2345",
        "ESPINOSA AUTOS,AV. GRAL ENRIQUEZ Y GRUPO DAVALOS,0984207925,3456"
    })
    public void shouldPassCarDealershipValidation(
        final String name,
        final String address,
        final String phone,
        final Integer pointOfSaleNumber
    ) {
        shouldValidateCarDealership(name, address, phone, pointOfSaleNumber, 0);
    }

    @ParameterizedTest
    @CsvSource(value = {
        "null,null,null,0",
        ",,,0",
        ",,A,-1",
        ",,12345678,-2",
        ",,12345678910,-3",
        " , , ,1000001",
        "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,,,0",
        ",BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB,,0"
    }, nullValues = {"null"})
    public void shouldFailCarDealershipValidation(
        final String name,
        final String address,
        final String phone,
        final Integer pointOfSaleNumber
    ) {
        shouldValidateCarDealership(name, address, phone, pointOfSaleNumber, 4);
    }
}
