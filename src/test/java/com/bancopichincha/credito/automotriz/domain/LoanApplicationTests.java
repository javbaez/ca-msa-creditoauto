package com.bancopichincha.credito.automotriz.domain;

import com.bancopichincha.credito.automotriz.domain.enums.LoanStatus;
import com.bancopichincha.credito.automotriz.util.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@SpringBootTest
public class LoanApplicationTests {
    private void shouldValidateLoanApplication(
        final LocalDate date,
        final Integer customerId,
        final Integer carDealershipId,
        final Integer salesExecutiveId,
        final Integer vehicleId,
        final Byte monthsTerm,
        final Short installments,
        final BigDecimal downPayment,
        final String status,
        final String observation,
        final int minNumberOfErrors,
        final int maxNumberOfErrors
    ) {
        final LoanApplication loanApplication = new LoanApplication();
        loanApplication.setDate(date);
        loanApplication.setCustomer(customerId != null ? CustomerUtil.of(1, "1000000000") : null);
        loanApplication.setCarDealership(carDealershipId != null ? CarDealershipUtil.of(carDealershipId) : null);
        loanApplication.setSalesExecutive(salesExecutiveId != null ? SalesExecutiveUtil.of(salesExecutiveId, "2000000000") : null);
        loanApplication.setVehicle(vehicleId != null ? VehicleUtil.of(vehicleId, "ABC-1234") : null);
        loanApplication.setMonthsTerm(monthsTerm);
        loanApplication.setInstallments(installments);
        loanApplication.setDownPayment(downPayment);
        try {
            loanApplication.setStatus(status != null ? LoanStatus.of(status) : null);
        } catch (final IllegalArgumentException e) {
            loanApplication.setStatus(null);
        }
        loanApplication.setObservation(observation);
        assertThat(
            ValidatorUtil.validate(loanApplication).size(),
            allOf(greaterThanOrEqualTo(minNumberOfErrors), lessThanOrEqualTo(maxNumberOfErrors))
        );
    }

    @ParameterizedTest
    @CsvSource(value = {
        "2100-01-01,1,1,1,1,3,3,500.00,R,null",
        "2100-01-02,2,2,2,2,4,4,500.01,R,null",
        "2100-01-03,3,3,3,3,60,240,1500.00,D,null",
        "2100-01-04,4,4,4,4,59,239,3000.00,C,Cancelado por test",
    }, nullValues = {"null"})
    public void shouldPassLoanApplicationValidation(
        final LocalDate date,
        final Integer customerId,
        final Integer carDealershipId,
        final Integer salesExecutiveId,
        final Integer vehicleId,
        final Byte monthsTerm,
        final Short installments,
        final BigDecimal downPayment,
        final String status,
        final String observation
    ) {
        shouldValidateLoanApplication(
            date,
            customerId,
            carDealershipId,
            salesExecutiveId,
            vehicleId,
            monthsTerm,
            installments,
            downPayment,
            status,
            observation,
            0,
            0
        );
    }

    @ParameterizedTest
    @CsvSource(value = {
        "null,null,null,null,null,null,null,null,null,null",
        ",,,,,,,,,",
        " , , , , , , , , , ",
        "2010-01-01,null,null,null,null,2,2,499.99,X,null",
        "2023-01-01,null,null,null,null,61,241,0.0,Y,null",
        "null,null,null,null,null,null,null,null,null,OOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO",
    }, nullValues = {"null"})
    public void shouldFailLoanApplicationValidation(
        final LocalDate date,
        final Integer customerId,
        final Integer carDealershipId,
        final Integer salesExecutiveId,
        final Integer vehicleId,
        final Byte monthsTerm,
        final Short installments,
        final BigDecimal downPayment,
        final String status,
        final String observation
    ) {
        shouldValidateLoanApplication(
            date,
            customerId,
            carDealershipId,
            salesExecutiveId,
            vehicleId,
            monthsTerm,
            installments,
            downPayment,
            status,
            observation,
            9,
            10
        );
    }
}
