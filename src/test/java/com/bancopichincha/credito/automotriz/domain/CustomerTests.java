package com.bancopichincha.credito.automotriz.domain;

import com.bancopichincha.credito.automotriz.domain.enums.CivilStatus;
import com.bancopichincha.credito.automotriz.util.ValidatorUtil;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@SpringBootTest
public class CustomerTests {
    private void shouldValidateCustomer(
        final String idNumber,
        final String names,
        final String lastNames,
        final Byte age,
        final LocalDate birthday,
        final String address,
        final String phone,
        final String civilStatus,
        final String spouseIdNumber,
        final String spouseNames,
        final Boolean creditSubject,
        final int minNumberOfErrors,
        final int maxNumberOfErrors
    ) {
        final Customer customer = new Customer();
        customer.setIdNumber(idNumber);
        customer.setNames(names);
        customer.setLastNames(lastNames);
        customer.setAge(age);
        customer.setBirthday(birthday);
        customer.setAddress(address);
        customer.setPhone(phone);
        try {
            customer.setCivilStatus(civilStatus != null ? CivilStatus.of(civilStatus) : null);
        } catch (final IllegalArgumentException e) {
            customer.setCivilStatus(null);
        }
        customer.setSpouseIdNumber(spouseIdNumber);
        customer.setSpouseNames(spouseNames);
        customer.setCreditSubject(creditSubject);
        assertThat(
            ValidatorUtil.validate(customer).size(),
            allOf(greaterThanOrEqualTo(minNumberOfErrors), lessThanOrEqualTo(maxNumberOfErrors))
        );
    }

    @ParameterizedTest
    @CsvSource(value = {
        "1705251732,PEDRO,PEREZ,18,2005-01-01,AV. 6 DE DICIEMBRE Y COLON,0980123456,S,null,null,FALSE",
        "1713311494,LUIS,SALAZAR,45,1978-03-03,REPUBLICA Y ELOY ALFARO,0982345678,C,0915916753,MARIA CAMPOS,TRUE",
        "1802584068,RAFAEL,PATIÑO,65,1958-05-05,SHYRIS Y JAPON,0991678901,D,null,null,TRUE",
        "1712664570,PATRICIA,AYALA,79,1944-05-05,AV. AMAZONAS Y PEREIRA,0992321098,V,null,null,TRUE"
    }, nullValues = {"null"})
    public void shouldPassCustomerValidation(
        final String idNumber,
        final String names,
        final String lastNames,
        final Byte age,
        final LocalDate birthday,
        final String address,
        final String phone,
        final String civilStatus,
        final String spouseIdNumber,
        final String spouseNames,
        final Boolean creditSubject
    ) {
        shouldValidateCustomer(
            idNumber,
            names,
            lastNames,
            age,
            birthday,
            address,
            phone,
            civilStatus,
            spouseIdNumber,
            spouseNames,
            creditSubject,
            0,
            0
        );
    }

    @ParameterizedTest
    @CsvSource(value = {
        "null,null,null,null,null,null,null,null,null,null,null",
        ",,,,,,,,,,",
        " , , , , , , , , , , ",
        "123456789,null,null,12,2100-01-01,null,A,C,123456789,null,null",
        "A,null,null,116,null,null,12345678,C,A,null,null",
        "?,null,null,null,null,null,12345678910,C,?,null,null",
        "null,NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN,null,null,null,null,null,null,null,null,null",
        "null,null,AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA,null,null,null,null,null,null,null,null",
        "null,null,null,null,null,DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD,null,null,null,null,null",
        "null,null,null,null,null,null,null,C,null,SSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSSS,null"
    }, nullValues = {"null"})
    public void shouldFailCustomerValidation(
        final String idNumber,
        final String names,
        final String lastNames,
        final Byte age,
        final LocalDate birthday,
        final String address,
        final String phone,
        final String civilStatus,
        final String spouseIdNumber,
        final String spouseNames,
        final Boolean creditSubject
    ) {
        shouldValidateCustomer(
            idNumber,
            names,
            lastNames,
            age,
            birthday,
            address,
            phone,
            civilStatus,
            spouseIdNumber,
            spouseNames,
            creditSubject,
            9,
            11
        );
    }
}
