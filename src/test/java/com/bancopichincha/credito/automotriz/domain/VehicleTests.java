package com.bancopichincha.credito.automotriz.domain;

import com.bancopichincha.credito.automotriz.util.BrandUtil;
import com.bancopichincha.credito.automotriz.util.ValidatorUtil;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@SpringBootTest
public class VehicleTests {
    private void shouldValidateVehicle(
        final String plate,
        final String model,
        final String chassisNumber,
        final String type,
        final BigDecimal cylinderCapacity,
        final Integer appraisal,
        final Integer brandId,
        final int minNumberOfErrors,
        final int maxNumberOfErrors
    ) {
        final Vehicle vehicle = new Vehicle();
        vehicle.setPlate(plate);
        vehicle.setModel(model);
        vehicle.setChassisNumber(chassisNumber);
        vehicle.setType(type);
        vehicle.setCylinderCapacity(cylinderCapacity);
        vehicle.setAppraisal(appraisal);
        vehicle.setBrand(brandId != null ? BrandUtil.of(brandId, "Marca test") : null);
        assertThat(
            ValidatorUtil.validate(vehicle).size(),
            allOf(greaterThanOrEqualTo(minNumberOfErrors), lessThanOrEqualTo(maxNumberOfErrors))
        );
    }

    @ParameterizedTest
    @CsvSource({
        "PBG-1234,Rav-4,123445,SUV,2.0,23000,1",
        "ABC-0123,Fiesta,234234,SEDAN,1.6,5000,2",
        "XYZ-9876,F-150,56456546,CAMIONETA,5.7,25000,3",
    })
    public void shouldPassVehicleValidation(
        final String plate,
        final String model,
        final String chassisNumber,
        final String type,
        final BigDecimal cylinderCapacity,
        final Integer appraisal,
        final Integer brandId
    ) {
        shouldValidateVehicle(
            plate,
            model,
            chassisNumber,
            type,
            cylinderCapacity,
            appraisal,
            brandId,
            0,
            0
        );
    }

    @ParameterizedTest
    @CsvSource(value = {
        "null,null,null,null,null,null,null",
        ",,,,,,",
        " , , , , , , ",
        "ABC123,null,null,null,0.0,0,null",
        "ABC1234,null,null,null,20.01,100000001,null",
        "ABC-123,null,null,null,null,null,null",
        "null,MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM,null,null,null,null,null",
        "null,null,CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC,null,null,null,null",
        "null,null,null,TTTTTTTTTTTTTTTTTTTTTTTTTT,null,null,null"
    }, nullValues = {"null"})
    public void shouldFailVehicleValidation(
        final String plate,
        final String model,
        final String chassisNumber,
        final String type,
        final BigDecimal cylinderCapacity,
        final Integer appraisal,
        final Integer brandId
    ) {
        shouldValidateVehicle(
            plate,
            model,
            chassisNumber,
            type,
            cylinderCapacity,
            appraisal,
            brandId,
            6,
            7
        );
    }
}
