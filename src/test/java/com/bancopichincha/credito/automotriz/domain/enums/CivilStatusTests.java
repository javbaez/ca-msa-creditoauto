package com.bancopichincha.credito.automotriz.domain.enums;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class CivilStatusTests {
    private static List<String> civilStatuses() {
        return Arrays.stream(CivilStatus.values()).map(CivilStatus::getCivilStatus).toList();
    }

    @ParameterizedTest
    @MethodSource("civilStatuses")
    public void shouldConvertCivilStatus(final String civilStatus) {
        assertDoesNotThrow(() -> CivilStatus.of(civilStatus));
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(strings = {"", " ", "?"})
    public void shouldThrowExceptionWhenConvertCivilStatus(final String civilStatus) {
        assertThrows(IllegalArgumentException.class, () -> CivilStatus.of(civilStatus));
    }
}
