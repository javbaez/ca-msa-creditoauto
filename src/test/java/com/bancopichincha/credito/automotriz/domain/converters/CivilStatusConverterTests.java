package com.bancopichincha.credito.automotriz.domain.converters;

import com.bancopichincha.credito.automotriz.domain.enums.CivilStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class CivilStatusConverterTests {
    private static List<String> civilStatuses() {
        return Arrays.stream(CivilStatus.values()).map(CivilStatus::getCivilStatus).toList();
    }

    private CivilStatusConverter civilStatusConverter;

    @BeforeEach
    public void setup() {
        civilStatusConverter = new CivilStatusConverter();
    }

    @ParameterizedTest
    @EnumSource(CivilStatus.class)
    public void shouldConvertCivilStatusToDatabaseColumn(final CivilStatus civilStatus) {
        assertThat(civilStatusConverter.convertToDatabaseColumn(civilStatus), notNullValue());
    }

    @ParameterizedTest
    @NullSource
    public void shouldReturnNullCivilStatusToDatabaseColumnConversionWhenCivilStatusIsNull(final CivilStatus civilStatus) {
        assertThat(civilStatusConverter.convertToDatabaseColumn(civilStatus), nullValue());
    }

    @ParameterizedTest
    @MethodSource("civilStatuses")
    public void shouldConvertCivilStatusToEntityAttribute(final String civilStatus) {
        assertThat(civilStatusConverter.convertToEntityAttribute(civilStatus), notNullValue());
    }

    @ParameterizedTest
    @NullSource
    public void shouldReturnNullCivilStatusToEntityAttributeConversionWhenCivilStatusIsNull(final String civilStatus) {
        assertThat(civilStatusConverter.convertToEntityAttribute(civilStatus), nullValue());
    }

    @ParameterizedTest
    @ValueSource(strings = {"", " ", "?"})
    public void shouldThrowExceptionWhenConvertCivilStatusToEntityAttribute(final String civilStatus) {
        assertThrows(IllegalArgumentException.class, () -> civilStatusConverter.convertToEntityAttribute(civilStatus));
    }
}
