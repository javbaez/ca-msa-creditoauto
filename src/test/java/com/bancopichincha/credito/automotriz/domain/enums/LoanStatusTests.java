package com.bancopichincha.credito.automotriz.domain.enums;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class LoanStatusTests {
    private static List<String> loanStatuses() {
        return Arrays.stream(LoanStatus.values()).map(LoanStatus::getLoanStatus).toList();
    }

    @ParameterizedTest
    @MethodSource("loanStatuses")
    public void shouldConvertLoanStatus(final String loanStatus) {
        assertDoesNotThrow(() -> LoanStatus.of(loanStatus));
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(strings = {"", " ", "?"})
    public void shouldThrowExceptionWhenConvertLoanStatus(final String loanStatus) {
        assertThrows(IllegalArgumentException.class, () -> LoanStatus.of(loanStatus));
    }
}
