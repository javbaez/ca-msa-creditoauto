package com.bancopichincha.credito.automotriz.domain;

import com.bancopichincha.credito.automotriz.util.ValidatorUtil;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.context.SpringBootTest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@SpringBootTest
public class BrandTests {
    private void shouldValidateBrand(final String name, final int numberOfErrors) {
        final Brand brand = new Brand();
        brand.setName(name);
        assertThat(ValidatorUtil.validate(brand).size(), is(numberOfErrors));
    }

    @ParameterizedTest
    @ValueSource(strings = {"Chevrolet", "KIA", "Hyundai", "Toyota", "Renault"})
    public void shouldPassBrandValidation(final String name) {
        shouldValidateBrand(name, 0);
    }

    @ParameterizedTest
    @NullSource
    @ValueSource(strings = {"", " ", "NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN"})
    public void shouldFailBrandValidation(final String name) {
        shouldValidateBrand(name, 1);
    }
}
