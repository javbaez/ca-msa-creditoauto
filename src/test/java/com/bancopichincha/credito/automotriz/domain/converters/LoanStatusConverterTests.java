package com.bancopichincha.credito.automotriz.domain.converters;

import com.bancopichincha.credito.automotriz.domain.enums.LoanStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
public class LoanStatusConverterTests {
    private static List<String> loanStatuses() {
        return Arrays.stream(LoanStatus.values()).map(LoanStatus::getLoanStatus).toList();
    }

    private LoanStatusConverter loanStatusConverter;

    @BeforeEach
    public void setup() {
        loanStatusConverter = new LoanStatusConverter();
    }

    @ParameterizedTest
    @EnumSource(LoanStatus.class)
    public void shouldConvertLoanStatusToDatabaseColumn(final LoanStatus loanStatus) {
        assertThat(loanStatusConverter.convertToDatabaseColumn(loanStatus), notNullValue());
    }

    @ParameterizedTest
    @NullSource
    public void shouldReturnNullLoanStatusToDatabaseColumnConversionWhenLoanStatusIsNull(final LoanStatus loanStatus) {
        assertThat(loanStatusConverter.convertToDatabaseColumn(loanStatus), nullValue());
    }

    @ParameterizedTest
    @MethodSource("loanStatuses")
    public void shouldConvertLoanStatusToEntityAttribute(final String loanStatus) {
        assertThat(loanStatusConverter.convertToEntityAttribute(loanStatus), notNullValue());
    }

    @ParameterizedTest
    @NullSource
    public void shouldReturnNullLoanStatusToEntityAttributeConversionWhenLoanStatusIsNull(final String loanStatus) {
        assertThat(loanStatusConverter.convertToEntityAttribute(loanStatus), nullValue());
    }

    @ParameterizedTest
    @ValueSource(strings = {"", " ", "?"})
    public void shouldThrowExceptionWhenConvertLoanStatusToEntityAttribute(final String loanStatus) {
        assertThrows(IllegalArgumentException.class, () -> loanStatusConverter.convertToEntityAttribute(loanStatus));
    }
}
