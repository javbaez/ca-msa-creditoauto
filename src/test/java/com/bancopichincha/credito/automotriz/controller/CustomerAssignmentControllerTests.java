package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.domain.CustomerAssignment;
import com.bancopichincha.credito.automotriz.exception.CustomerAssignmentNotFoundException;
import com.bancopichincha.credito.automotriz.service.CustomerAssignmentService;
import com.bancopichincha.credito.automotriz.service.dto.CustomerAssignmentDto;
import com.bancopichincha.credito.automotriz.service.mapper.CustomerAssignmentMapper;
import com.bancopichincha.credito.automotriz.util.CustomerAssignmentUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.ResultMatcher;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class CustomerAssignmentControllerTests extends ControllerTests<CustomerAssignmentController> {
    private static final String BASE_PATH = "/clientePatio";
    private static final Integer ID = 1;
    private static final Integer ID_NOT_FOUND = Integer.MAX_VALUE;
    @MockBean
    private CustomerAssignmentService customerAssignmentService;
    @Autowired
    private CustomerAssignmentMapper customerAssignmentMapper;

    @Override
    protected String getBasePath() {
        return BASE_PATH;
    }

    private CustomerAssignment getCustomerAssignment(final Integer id) {
        return CustomerAssignmentUtil.of(id);
    }

    private CustomerAssignmentDto getCustomerAssignmentDto(final CustomerAssignment customerAssignment) {
        return customerAssignmentMapper.customerAssignmentToCustomerAssignmentDto(customerAssignment);
    }

    private void expectCustomerAssignmentDto(
        final boolean expectId,
        final CustomerAssignmentDto expectedDto,
        final CustomerAssignmentDto responseDto
    ) {
        if (expectId) {
            assertThat(responseDto.getId(), is(expectedDto.getId()));
        }
        assertThat(responseDto.getCustomer().getId(), is(expectedDto.getCustomer().getId()));
        assertThat(responseDto.getCarDealership().getId(), is(expectedDto.getCarDealership().getId()));
        assertThat(responseDto.getAssignmentDate(), is(expectedDto.getAssignmentDate()));
    }

    private String findCustomerAssignment(final Integer id, final ResultMatcher statusMatcher) throws Exception {
        return getMvc().perform(get(buildUrl("/buscar", "id", Optional.of(id)))
            .contentType(APPLICATION_JSON))
            .andExpect(statusMatcher)
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);
    }

    @Test
    public void shouldFindCustomerAssignment() throws Exception {
        final CustomerAssignmentDto dto = getCustomerAssignmentDto(getCustomerAssignment(ID));
        when(customerAssignmentService.findById(any(Integer.class))).thenReturn(dto);
        final CustomerAssignmentDto responseDto = getObjectMapper().readValue(
            findCustomerAssignment(dto.getId(), status().isOk()),
            CustomerAssignmentDto.class
        );
        expectCustomerAssignmentDto(true, dto, responseDto);
    }

    @Test
    public void shouldThrowCustomerAssignmentNotFoundExceptionWhenFindCustomerAssignment() throws Exception {
        final var exception = new CustomerAssignmentNotFoundException(ID_NOT_FOUND);
        when(customerAssignmentService.findById(any(Integer.class))).thenThrow(exception);
        expectError(
            findCustomerAssignment(ID_NOT_FOUND, status().is4xxClientError()),
            exception.getLocalizedMessage()
        );
    }

    @Test
    public void shouldCreateCustomerAssignment() throws Exception {
        final CustomerAssignmentDto inputDto = getCustomerAssignmentDto(getCustomerAssignment(ID));
        when(customerAssignmentService.create(any(CustomerAssignmentDto.class))).thenReturn(inputDto);
        final CustomerAssignmentDto responseDto = getObjectMapper().readValue(
            getMvc().perform(post(buildUrl("/crear"))
                .content(getObjectMapper().writeValueAsString(inputDto))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8),
            CustomerAssignmentDto.class
        );
        expectCustomerAssignmentDto(false, inputDto, responseDto);
    }

    @Test
    public void shouldUpdateCustomerAssignment() throws Exception {
        final CustomerAssignment customerAssignment = getCustomerAssignment(ID);
        customerAssignment.getCustomer().setId(customerAssignment.getCustomer().getId() + 1);
        customerAssignment.getCarDealership().setId(customerAssignment.getCarDealership().getId() + 1);
        customerAssignment.setAssignmentDate(customerAssignment.getAssignmentDate().plusDays(1));
        final CustomerAssignmentDto inputDto = getCustomerAssignmentDto(customerAssignment);
        when(customerAssignmentService.update(any(CustomerAssignmentDto.class))).thenReturn(inputDto);
        final CustomerAssignmentDto responseDto = getObjectMapper().readValue(
            getMvc().perform(put(buildUrl("/actualizar"))
                .content(getObjectMapper().writeValueAsString(inputDto))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8),
            CustomerAssignmentDto.class
        );
        expectCustomerAssignmentDto(true, inputDto, responseDto);
    }

    private String deleteCustomerAssignment(final Integer id, final ResultMatcher statusMatcher) throws Exception {
        return getMvc().perform(delete(buildUrl("/eliminar", "id", Optional.of(id)))
            .contentType(APPLICATION_JSON))
            .andExpect(statusMatcher)
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);
    }

    @Test
    public void shouldDeleteCustomerAssignment() throws Exception {
        final CustomerAssignmentDto dto = getCustomerAssignmentDto(getCustomerAssignment(ID));
        when(customerAssignmentService.delete(any(Integer.class))).thenReturn(dto);
        assertThat(deleteCustomerAssignment(dto.getId(), status().isOk()), is("Eliminado correctamente"));
    }

    @Test
    public void shouldThrowCustomerAssignmentNotFoundExceptionWhenDeleteCustomerAssignment() throws Exception {
        final var exception = new CustomerAssignmentNotFoundException(ID_NOT_FOUND);
        when(customerAssignmentService.delete(any(Integer.class))).thenThrow(exception);
        expectError(
            deleteCustomerAssignment(ID_NOT_FOUND, status().is4xxClientError()),
            exception.getLocalizedMessage()
        );
    }
}
