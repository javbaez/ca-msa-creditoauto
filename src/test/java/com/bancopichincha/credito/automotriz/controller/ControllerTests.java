package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.configuration.ApplicationResponseEntityExceptionHandler;
import com.bancopichincha.credito.automotriz.configuration.ApplicationTestProperties;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Getter;
import org.junit.jupiter.api.BeforeEach;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.Map;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@Getter
abstract class ControllerTests<C> {
    private MockMvc mvc;
    @Autowired
    @InjectMocks
    private C controller;
    @Autowired
    private ApplicationTestProperties config;
    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    public void setup() {
        mvc = MockMvcBuilders
            .standaloneSetup(controller)
            .setControllerAdvice(new ApplicationResponseEntityExceptionHandler())
            .build();
    }

    protected abstract String getBasePath();

    protected String buildUrl(final String path) {
        return buildUrl(path, "", "", Optional.empty());
    }

    protected <ID> String buildUrl(final String path, final ID id) {
        return buildUrl(path, id, "", Optional.empty());
    }

    protected String buildUrl(final String path, final String queryParamName, final Optional<Object> queryParamValue) {
        return buildUrl(path, "", queryParamName, queryParamValue);
    }

    private String buildUrl(
        final String path,
        final Object id,
        final String queryParamName,
        final Optional<Object> queryParamValue
    ) {
        return UriComponentsBuilder
            .fromHttpUrl(config.getBaseUrl())
            .path(getBasePath() + path)
            .pathSegment(id.toString())
            .queryParamIfPresent(queryParamName, queryParamValue)
            .build()
            .toUri()
            .toString();
    }

    protected void expectError(final String jsonString, final String errorMessage) throws Exception {
        final Map<String, String> result = getObjectMapper()
            .readerFor(Map.class)
            .readValue(jsonString);
        assertThat(result.size(), is(1));
        assertThat(result.containsKey("error"), is(true));
        assertThat(result.get("error"), is(errorMessage));
    }
}
