package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.domain.LoanApplication;
import com.bancopichincha.credito.automotriz.domain.enums.LoanStatus;
import com.bancopichincha.credito.automotriz.exception.LoanApplicationExistsException;
import com.bancopichincha.credito.automotriz.exception.LoanApplicationNotFoundException;
import com.bancopichincha.credito.automotriz.exception.SalesExecutiveNotFoundException;
import com.bancopichincha.credito.automotriz.exception.VehicleReservedForAnotherLoanException;
import com.bancopichincha.credito.automotriz.service.LoanApplicationService;
import com.bancopichincha.credito.automotriz.service.dto.LoanApplicationDto;
import com.bancopichincha.credito.automotriz.service.mapper.LoanApplicationMapper;
import com.bancopichincha.credito.automotriz.util.LoanApplicationUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.ResultMatcher;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class LoanApplicationControllerTests extends ControllerTests<LoanApplicationController> {
    private static final String BASE_PATH = "/solicitudCredito";
    private static final Integer ID = 1;
    private static final Integer ID_NOT_FOUND = Integer.MAX_VALUE;
    @MockBean
    private LoanApplicationService loanApplicationService;
    @Autowired
    private LoanApplicationMapper loanApplicationMapper;

    @Override
    protected String getBasePath() {
        return BASE_PATH;
    }

    private LoanApplication getLoanApplication(final Integer id) {
        return LoanApplicationUtil.of(id);
    }

    private LoanApplicationDto getLoanApplicationDto(final LoanApplication loanApplication) {
        return loanApplicationMapper.loanApplicationToLoanApplicationDto(loanApplication);
    }

    private void expectLoanApplicationDto(
        final boolean expectId,
        final LoanApplicationDto expectedDto,
        final LoanApplicationDto responseDto
    ) {
        if (expectId) {
            assertThat(responseDto.getId(), is(expectedDto.getId()));
        }
        assertThat(responseDto.getDate(), is(expectedDto.getDate()));
        assertThat(responseDto.getCustomer().getId(), is(expectedDto.getCustomer().getId()));
        assertThat(responseDto.getCarDealership().getId(), is(expectedDto.getCarDealership().getId()));
        assertThat(responseDto.getSalesExecutive().getId(), is(expectedDto.getSalesExecutive().getId()));
        assertThat(responseDto.getVehicle().getId(), is(expectedDto.getVehicle().getId()));
        assertThat(responseDto.getMonthsTerm(), is(expectedDto.getMonthsTerm()));
        assertThat(responseDto.getInstallments(), is(expectedDto.getInstallments()));
        assertThat(responseDto.getDownPayment(), is(expectedDto.getDownPayment()));
        assertThat(responseDto.getStatus(), is(expectedDto.getStatus()));
    }

    private String findLoanApplication(final Integer id, final ResultMatcher statusMatcher) throws Exception {
        return getMvc().perform(get(buildUrl("/buscar", "id", Optional.of(id)))
            .contentType(APPLICATION_JSON))
            .andExpect(statusMatcher)
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);
    }

    @Test
    public void shouldFindLoanApplication() throws Exception {
        final LoanApplicationDto dto = getLoanApplicationDto(getLoanApplication(ID));
        when(loanApplicationService.findById(any(Integer.class))).thenReturn(dto);
        final LoanApplicationDto responseDto = getObjectMapper().readValue(
            findLoanApplication(dto.getId(), status().isOk()),
            LoanApplicationDto.class
        );
        expectLoanApplicationDto(true, dto, responseDto);
    }

    @Test
    public void shouldThrowLoanApplicationNotFoundExceptionWhenFindLoanApplication() throws Exception {
        final var exception = new LoanApplicationNotFoundException(ID_NOT_FOUND);
        when(loanApplicationService.findById(any(Integer.class))).thenThrow(exception);
        expectError(
            findLoanApplication(ID_NOT_FOUND, status().is4xxClientError()),
            exception.getLocalizedMessage()
        );
    }

    private String createLoanApplication(final LoanApplicationDto inputDto, final ResultMatcher statusMatcher) throws Exception {
        return getMvc().perform(post(buildUrl("/crear"))
            .content(getObjectMapper().writeValueAsString(inputDto))
            .contentType(APPLICATION_JSON))
            .andExpect(statusMatcher)
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);
    }

    @Test
    public void shouldCreateLoanApplication() throws Exception {
        final LoanApplicationDto inputDto = getLoanApplicationDto(getLoanApplication(ID));
        when(loanApplicationService.create(any(LoanApplicationDto.class))).thenReturn(inputDto);
        final LoanApplicationDto responseDto = getObjectMapper().readValue(
            createLoanApplication(inputDto, status().isCreated()),
            LoanApplicationDto.class
        );
        expectLoanApplicationDto(false, inputDto, responseDto);
    }

    private void shouldThrowExceptionWhenCreateLoanApplication(final LoanApplicationDto inputDto, final Exception exception) throws Exception {
        when(loanApplicationService.create(any(LoanApplicationDto.class))).thenThrow(exception);
        expectError(
            createLoanApplication(inputDto, status().is4xxClientError()),
            exception.getLocalizedMessage()
        );
    }

    @Test
    public void shouldThrowLoanApplicationExistsExceptionWhenCreateVehicle() throws Exception {
        shouldThrowExceptionWhenCreateLoanApplication(
            getLoanApplicationDto(getLoanApplication(ID)),
            new LoanApplicationExistsException()
        );
    }

    @Test
    public void shouldThrowVehicleReservedForAnotherLoanExceptionWhenCreateVehicle() throws Exception {
        shouldThrowExceptionWhenCreateLoanApplication(
            getLoanApplicationDto(getLoanApplication(ID)),
            new VehicleReservedForAnotherLoanException()
        );
    }

    @Test
    public void shouldThrowSalesExecutiveNotFoundExceptionWhenCreateVehicle() throws Exception {
        final LoanApplicationDto inputDto = getLoanApplicationDto(getLoanApplication(ID));
        inputDto.getSalesExecutive().setId(ID_NOT_FOUND);
        shouldThrowExceptionWhenCreateLoanApplication(
            inputDto,
            new SalesExecutiveNotFoundException(inputDto.getSalesExecutive().getId())
        );
    }

    private String updateLoanApplication(final LoanApplicationDto inputDto, final ResultMatcher statusMatcher) throws Exception {
        return getMvc().perform(put(buildUrl("/actualizar"))
            .content(getObjectMapper().writeValueAsString(inputDto))
            .contentType(APPLICATION_JSON))
            .andExpect(statusMatcher)
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);
    }

    @Test
    public void shouldUpdateLoanApplication() throws Exception {
        final LoanApplication loanApplication = getLoanApplication(ID);
        loanApplication.setDate(loanApplication.getDate().plusDays(1));
        loanApplication.getCustomer().setId(loanApplication.getCustomer().getId() + 1);
        loanApplication.getCarDealership().setId(loanApplication.getCarDealership().getId() + 1);
        loanApplication.getSalesExecutive().setId(loanApplication.getSalesExecutive().getId() + 1);
        loanApplication.getVehicle().setId(loanApplication.getVehicle().getId() + 1);
        loanApplication.setMonthsTerm((byte) (loanApplication.getMonthsTerm() + 1));
        loanApplication.setInstallments((short) (loanApplication.getInstallments() + 1));
        loanApplication.setDownPayment(loanApplication.getDownPayment().add(BigDecimal.ONE));
        loanApplication.setObservation("Actualizado");
        loanApplication.setStatus(LoanStatus.DISPATCHED);
        final LoanApplicationDto inputDto = getLoanApplicationDto(loanApplication);
        when(loanApplicationService.update(any(LoanApplicationDto.class))).thenReturn(inputDto);
        final LoanApplicationDto responseDto = getObjectMapper().readValue(
            updateLoanApplication(inputDto, status().isOk()),
            LoanApplicationDto.class
        );
        expectLoanApplicationDto(true, inputDto, responseDto);
    }

    private void shouldThrowExceptionWhenUpdateLoanApplication(final LoanApplicationDto inputDto, final Exception exception) throws Exception {
        when(loanApplicationService.update(any(LoanApplicationDto.class))).thenThrow(exception);
        inputDto.setDate(inputDto.getDate().plusDays(1));
        expectError(
            updateLoanApplication(inputDto, status().is4xxClientError()),
            exception.getLocalizedMessage()
        );
    }

    @Test
    public void shouldThrowVehicleReservedForAnotherLoanExceptionWhenUpdateLoanApplication() throws Exception {
        shouldThrowExceptionWhenUpdateLoanApplication(
            getLoanApplicationDto(getLoanApplication(ID)),
            new VehicleReservedForAnotherLoanException()
        );
    }

    @Test
    public void shouldThrowSalesExecutiveNotFoundExceptionWhenUpdateLoanApplication() throws Exception {
        final LoanApplicationDto inputDto = getLoanApplicationDto(getLoanApplication(ID));
        shouldThrowExceptionWhenUpdateLoanApplication(
            inputDto,
            new SalesExecutiveNotFoundException(inputDto.getSalesExecutive().getId())
        );
    }

    private String deleteLoanApplication(final Integer id, final ResultMatcher statusMatcher) throws Exception {
        return getMvc().perform(delete(buildUrl("/eliminar", "id", Optional.of(id)))
            .contentType(APPLICATION_JSON))
            .andExpect(statusMatcher)
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);
    }

    @Test
    public void shouldDeleteLoanApplication() throws Exception {
        final LoanApplicationDto dto = getLoanApplicationDto(getLoanApplication(ID));
        when(loanApplicationService.delete(any(Integer.class))).thenReturn(dto);
        final LoanApplicationDto responseDto = getObjectMapper().readValue(
            deleteLoanApplication(dto.getId(), status().isOk()),
            LoanApplicationDto.class
        );
        expectLoanApplicationDto(true, dto, responseDto);
    }

    @Test
    public void shouldThrowLoanApplicationNotFoundExceptionWhenLoanApplication() throws Exception {
        final var exception = new LoanApplicationNotFoundException(ID_NOT_FOUND);
        when(loanApplicationService.delete(any(Integer.class))).thenThrow(exception);
        expectError(
            deleteLoanApplication(ID_NOT_FOUND, status().is4xxClientError()),
            exception.getLocalizedMessage()
        );
    }
}
