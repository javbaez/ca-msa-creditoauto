package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.domain.Vehicle;
import com.bancopichincha.credito.automotriz.exception.VehicleLoanApplicationExistsException;
import com.bancopichincha.credito.automotriz.exception.VehicleNotFoundException;
import com.bancopichincha.credito.automotriz.exception.VehiclePlateExistsException;
import com.bancopichincha.credito.automotriz.exception.VehicleReservedForLoanException;
import com.bancopichincha.credito.automotriz.service.VehicleService;
import com.bancopichincha.credito.automotriz.service.dto.VehicleDto;
import com.bancopichincha.credito.automotriz.service.mapper.VehicleMapper;
import com.bancopichincha.credito.automotriz.util.VehicleUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.ResultMatcher;

import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class VehicleControllerTests extends ControllerTests<VehicleController> {
    private static final String BASE_PATH = "/vehiculo";
    private static final Integer ID = 1;
    private static final Integer ID_NOT_FOUND = Integer.MAX_VALUE;
    private static final String PLATE = "PBG-1234";
    @MockBean
    private VehicleService vehicleService;
    @Autowired
    private VehicleMapper vehicleMapper;

    @Override
    protected String getBasePath() {
        return BASE_PATH;
    }

    private Vehicle getVehicle(final Integer id, final String plate) {
        return VehicleUtil.of(id, plate);
    }

    private VehicleDto getVehicleDto(final Vehicle vehicle) {
        return vehicleMapper.vehicleToVehicleDto(vehicle);
    }

    private void expectVehicleDto(
        final boolean expectId,
        final VehicleDto expectedDto,
        final VehicleDto responseDto
    ) {
        if (expectId) {
            assertThat(responseDto.getId(), is(expectedDto.getId()));
        }
        assertThat(responseDto.getPlate(), is(expectedDto.getPlate()));
        assertThat(responseDto.getModel(), is(expectedDto.getModel()));
        assertThat(responseDto.getChassisNumber(), is(expectedDto.getChassisNumber()));
        assertThat(responseDto.getType(), is(expectedDto.getType()));
        assertThat(responseDto.getCylinderCapacity(), is(expectedDto.getCylinderCapacity()));
        assertThat(responseDto.getAppraisal(), is(expectedDto.getAppraisal()));
        assertThat(responseDto.getBrand().getId(), is(expectedDto.getBrand().getId()));
    }

    private String findVehicle(final Integer id, final ResultMatcher statusMatcher) throws Exception {
        return getMvc().perform(get(buildUrl("/buscar", "id", Optional.of(id)))
            .contentType(APPLICATION_JSON))
            .andExpect(statusMatcher)
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);
    }

    @Test
    public void shouldFindVehicle() throws Exception {
        final VehicleDto dto = getVehicleDto(getVehicle(ID, PLATE));
        when(vehicleService.findById(any(Integer.class))).thenReturn(dto);
        final VehicleDto responseDto = getObjectMapper().readValue(
            findVehicle(dto.getId(), status().isOk()),
            VehicleDto.class
        );
        expectVehicleDto(true, dto, responseDto);
    }

    @Test
    public void shouldThrowVehicleNotFoundExceptionWhenFindVehicle() throws Exception {
        final var exception = new VehicleNotFoundException(ID_NOT_FOUND);
        when(vehicleService.findById(any(Integer.class))).thenThrow(exception);
        expectError(
            findVehicle(ID_NOT_FOUND, status().is4xxClientError()),
            exception.getLocalizedMessage()
        );
    }

    @Test
    public void shouldFindVehiclesByModel() throws Exception {
        final List<VehicleDto> dtoList = List.of(getVehicleDto(getVehicle(ID, PLATE)));
        when(vehicleService.findByModel(any(String.class))).thenReturn(dtoList);
        final List<VehicleDto> responseDtoList = getObjectMapper().readValue(
            getMvc().perform(get(buildUrl("/consultaPorModelo", "modelo", Optional.of("Rav-4")))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8),
            new TypeReference<>() { }
        );
        assertThat(responseDtoList.size(), is(dtoList.size()));
        for (int i = 0; i < dtoList.size(); ++i) {
            expectVehicleDto(true, dtoList.get(i), responseDtoList.get(i));
        }
    }

    @Test
    public void shouldFindVehiclesByBrandId() throws Exception {
        final VehicleDto vehicleDto = getVehicleDto(getVehicle(ID, PLATE));
        final List<VehicleDto> dtoList = List.of(vehicleDto);
        when(vehicleService.findByBrandId(any(Integer.class))).thenReturn(dtoList);
        final List<VehicleDto> responseDtoList = getObjectMapper().readValue(
            getMvc().perform(get(buildUrl("/consultaPorMarca"))
                .content(getObjectMapper().writeValueAsString(vehicleDto.getBrand()))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8),
            new TypeReference<>() { }
        );
        assertThat(responseDtoList.size(), is(dtoList.size()));
        for (int i = 0; i < dtoList.size(); ++i) {
            expectVehicleDto(true, dtoList.get(i), responseDtoList.get(i));
        }
    }

    private String createVehicle(final VehicleDto inputDto, final ResultMatcher statusMatcher) throws Exception {
        return getMvc().perform(post(buildUrl("/crear"))
            .content(getObjectMapper().writeValueAsString(inputDto))
            .contentType(APPLICATION_JSON))
            .andExpect(statusMatcher)
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);
    }

    @Test
    public void shouldCreateVehicle() throws Exception {
        final VehicleDto inputDto = getVehicleDto(getVehicle(ID, PLATE));
        when(vehicleService.create(any(VehicleDto.class))).thenReturn(inputDto);
        final VehicleDto responseDto = getObjectMapper().readValue(
            createVehicle(inputDto, status().isCreated()),
            VehicleDto.class
        );
        expectVehicleDto(false, inputDto, responseDto);
    }

    @Test
    public void shouldThrowVehiclePlateExistsExceptionWhenCreateVehicle() throws Exception {
        final VehicleDto inputDto = getVehicleDto(getVehicle(ID, PLATE));
        final var exception = new VehiclePlateExistsException(inputDto.getPlate());
        when(vehicleService.create(any(VehicleDto.class))).thenThrow(exception);
        expectError(
            createVehicle(inputDto, status().is4xxClientError()),
            exception.getLocalizedMessage()
        );
    }

    private String updateVehicle(final VehicleDto inputDto, final ResultMatcher statusMatcher) throws Exception {
        return getMvc().perform(put(buildUrl("/actualizar"))
            .content(getObjectMapper().writeValueAsString(inputDto))
            .contentType(APPLICATION_JSON))
            .andExpect(statusMatcher)
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);
    }

    @Test
    public void shouldUpdateVehicle() throws Exception {
        final Vehicle vehicle = getVehicle(ID, PLATE);
        vehicle.setPlate(vehicle.getPlate().substring(0, vehicle.getPlate().length() - 1) + "1");
        vehicle.setModel(vehicle.getModel() + "1");
        vehicle.setChassisNumber(vehicle.getChassisNumber() + "1");
        vehicle.setType(vehicle.getType() + "1");
        vehicle.setCylinderCapacity(vehicle.getCylinderCapacity().add(BigDecimal.ONE));
        vehicle.setAppraisal(vehicle.getAppraisal() + 1);
        vehicle.getBrand().setId(vehicle.getBrand().getId() + 1);
        final VehicleDto inputDto = getVehicleDto(vehicle);
        when(vehicleService.update(any(VehicleDto.class))).thenReturn(inputDto);
        final VehicleDto responseDto = getObjectMapper().readValue(
            updateVehicle(inputDto, status().isOk()),
            VehicleDto.class
        );
        expectVehicleDto(true, inputDto, responseDto);
    }

    private void shouldThrowExceptionWhenUpdateVehicle(final VehicleDto inputDto, final Exception exception) throws Exception {
        inputDto.setModel(inputDto.getModel() + "1");
        when(vehicleService.update(any(VehicleDto.class))).thenThrow(exception);
        expectError(
            updateVehicle(inputDto, status().is4xxClientError()),
            exception.getLocalizedMessage()
        );
    }

    @Test
    public void shouldThrowVehiclePlateExistsExceptionWhenUpdateVehicle() throws Exception {
        final VehicleDto inputDto = getVehicleDto(getVehicle(ID, PLATE));
        shouldThrowExceptionWhenUpdateVehicle(inputDto, new VehiclePlateExistsException(inputDto.getPlate()));
    }

    @Test
    public void shouldThrowVehicleReservedForLoanExceptionWhenUpdateVehicle() throws Exception {
        shouldThrowExceptionWhenUpdateVehicle(
            getVehicleDto(getVehicle(ID, PLATE)),
            new VehicleReservedForLoanException()
        );
    }

    private String deleteVehicle(final Integer id, final ResultMatcher statusMatcher) throws Exception {
        return getMvc().perform(delete(buildUrl("/eliminar", "id", Optional.of(id)))
            .contentType(APPLICATION_JSON))
            .andExpect(statusMatcher)
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);
    }

    @Test
    public void shouldDeleteVehicle() throws Exception {
        final VehicleDto dto = getVehicleDto(getVehicle(ID, PLATE));
        when(vehicleService.delete(any(Integer.class))).thenReturn(dto);
        assertThat(deleteVehicle(dto.getId(), status().isOk()), is("true"));
    }

    @Test
    public void shouldThrowVehicleLoanApplicationExistsExceptionWhenDeleteVehicle() throws Exception {
        final var exception = new VehicleLoanApplicationExistsException();
        when(vehicleService.delete(any(Integer.class))).thenThrow(exception);
        expectError(
            deleteVehicle(ID, status().is4xxClientError()),
            exception.getLocalizedMessage()
        );
    }
}
