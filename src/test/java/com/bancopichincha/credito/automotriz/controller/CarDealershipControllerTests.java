package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.domain.CarDealership;
import com.bancopichincha.credito.automotriz.exception.CarDealershipNotFoundException;
import com.bancopichincha.credito.automotriz.exception.CarDealershipReferenceToCustomerAssignmentException;
import com.bancopichincha.credito.automotriz.exception.CarDealershipReferenceToSalesExecutiveException;
import com.bancopichincha.credito.automotriz.service.CarDealershipService;
import com.bancopichincha.credito.automotriz.service.dto.CarDealershipDto;
import com.bancopichincha.credito.automotriz.service.mapper.CarDealershipMapper;
import com.bancopichincha.credito.automotriz.util.CarDealershipUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.ResultMatcher;

import java.nio.charset.StandardCharsets;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
public class CarDealershipControllerTests extends ControllerTests<CarDealershipController> {
    private static final String BASE_PATH = "/patio";
    private static final Integer ID = 1;
    private static final Integer ID_NOT_FOUND = Integer.MAX_VALUE;
    @MockBean
    private CarDealershipService carDealershipService;
    @Autowired
    private CarDealershipMapper carDealershipMapper;

    @Override
    protected String getBasePath() {
        return BASE_PATH;
    }

    private CarDealership getCarDealership(final Integer id) {
        return CarDealershipUtil.of(id);
    }

    private CarDealershipDto getCarDealershipDto(final CarDealership carDealership) {
        return carDealershipMapper.carDealershipToCarDealershipDto(carDealership);
    }

    private void expectCarDealershipDto(
        final boolean expectId,
        final CarDealershipDto expectedDto,
        final CarDealershipDto responseDto
    ) {
        if (expectId) {
            assertThat(responseDto.getId(), is(expectedDto.getId()));
        }
        assertThat(responseDto.getName(), is(expectedDto.getName()));
        assertThat(responseDto.getAddress(), is(expectedDto.getAddress()));
        assertThat(responseDto.getPhone(), is(expectedDto.getPhone()));
        assertThat(responseDto.getPointOfSaleNumber(), is(expectedDto.getPointOfSaleNumber()));
    }

    private String findCarDealership(final Integer id, final ResultMatcher statusMatcher) throws Exception {
        return getMvc().perform(get(buildUrl("/buscar", "id", Optional.of(id)))
            .contentType(APPLICATION_JSON))
            .andExpect(statusMatcher)
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);
    }

    @Test
    public void shouldFindCarDealership() throws Exception {
        final CarDealershipDto dto = getCarDealershipDto(getCarDealership(ID));
        when(carDealershipService.findById(any(Integer.class))).thenReturn(dto);
        final CarDealershipDto responseDto = getObjectMapper().readValue(
            findCarDealership(dto.getId(), status().isOk()),
            CarDealershipDto.class
        );
        expectCarDealershipDto(true, dto, responseDto);
    }

    @Test
    public void shouldThrowCustomerAssignmentNotFoundExceptionWhenFindCarDealership() throws Exception {
        final var exception = new CarDealershipNotFoundException(ID_NOT_FOUND);
        when(carDealershipService.findById(any(Integer.class))).thenThrow(exception);
        expectError(
            findCarDealership(ID_NOT_FOUND, status().is4xxClientError()),
            exception.getLocalizedMessage()
        );
    }

    @Test
    public void shouldCreateCarDealership() throws Exception {
        final CarDealershipDto inputDto = getCarDealershipDto(getCarDealership(ID));
        when(carDealershipService.create(any(CarDealershipDto.class))).thenReturn(inputDto);
        final CarDealershipDto responseDto = getObjectMapper().readValue(
            getMvc().perform(post(buildUrl("/crear"))
                .content(getObjectMapper().writeValueAsString(inputDto))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8),
            CarDealershipDto.class
        );
        expectCarDealershipDto(false, inputDto, responseDto);
    }

    @Test
    public void shouldUpdateCarDealership() throws Exception {
        final CarDealership carDealership = getCarDealership(ID);
        carDealership.setName(carDealership.getName() + "1");
        carDealership.setAddress(carDealership.getAddress() + "1");
        carDealership.setPhone(carDealership.getPhone().substring(0, carDealership.getPhone().length() - 1) + "1");
        carDealership.setPointOfSaleNumber(carDealership.getPointOfSaleNumber() + 1);
        final CarDealershipDto inputDto = getCarDealershipDto(carDealership);
        when(carDealershipService.update(any(CarDealershipDto.class))).thenReturn(inputDto);
        final CarDealershipDto responseDto = getObjectMapper().readValue(
            getMvc().perform(put(buildUrl("/actualizar"))
                .content(getObjectMapper().writeValueAsString(inputDto))
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString(StandardCharsets.UTF_8),
            CarDealershipDto.class
        );
        expectCarDealershipDto(true, inputDto, responseDto);
    }

    private String deleteCarDealership(final Integer id, final ResultMatcher statusMatcher) throws Exception {
        return getMvc().perform(delete(buildUrl("/eliminar", "id", Optional.of(id)))
            .contentType(APPLICATION_JSON))
            .andExpect(statusMatcher)
            .andReturn()
            .getResponse()
            .getContentAsString(StandardCharsets.UTF_8);
    }

    @Test
    public void shouldDeleteCarDealership() throws Exception {
        final CarDealershipDto dto = getCarDealershipDto(getCarDealership(ID));
        when(carDealershipService.delete(any(Integer.class))).thenReturn(dto);
        assertThat(deleteCarDealership(dto.getId(), status().isOk()), is("true"));
    }

    private void shouldThrowExceptionWhenDeleteCarDealership(final Integer id, final Exception exception) throws Exception {
        when(carDealershipService.delete(any(Integer.class))).thenThrow(exception);
        expectError(
            deleteCarDealership(id, status().is4xxClientError()),
            exception.getLocalizedMessage()
        );
    }

    @Test
    public void shouldThrowCarDealershipReferenceToSalesExecutiveExceptionWhenDeleteCarDealership() throws Exception {
        shouldThrowExceptionWhenDeleteCarDealership(ID, new CarDealershipReferenceToSalesExecutiveException(ID));
    }

    @Test
    public void shouldThrowCarDealershipReferenceToCustomerAssignmentExceptionWhenDeleteCarDealership() throws Exception {
        shouldThrowExceptionWhenDeleteCarDealership(ID, new CarDealershipReferenceToCustomerAssignmentException(ID));
    }
}
