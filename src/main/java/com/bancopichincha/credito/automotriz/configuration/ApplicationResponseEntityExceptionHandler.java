package com.bancopichincha.credito.automotriz.configuration;

import com.bancopichincha.credito.automotriz.exception.ResourceBadRequestException;
import com.bancopichincha.credito.automotriz.exception.ResourceNotFoundException;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.validation.ConstraintViolationException;
import jakarta.validation.UnexpectedTypeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.*;

@ControllerAdvice
public class ApplicationResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
    @Autowired
    private ObjectMapper objectMapper;

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
        final MethodArgumentNotValidException ex,
        final HttpHeaders headers,
        final HttpStatusCode status,
        final WebRequest request
    ) {
        final Class<?> targetClass = Objects.requireNonNull(ex.getBindingResult().getTarget()).getClass();
        final Map<String, List<String>> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach(error -> {
            final String fieldName = ((FieldError) error).getField();
            String field;
            try {
                field = targetClass.getDeclaredField(fieldName).getAnnotation(JsonProperty.class).value();
            } catch (final NoSuchFieldException | SecurityException e) {
                field = fieldName;
            }
            errors
                .computeIfAbsent(field, k -> new ArrayList<>(5))
                .add(error.getDefaultMessage());
        });
        return handleCustomExceptionInternal(
            new Exception("Invalid method argument"),
            request,
            HttpStatus.BAD_REQUEST,
            headers,
            errors
        );
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(
        final MissingServletRequestParameterException ex,
        final HttpHeaders headers,
        final HttpStatusCode status,
        final WebRequest request
    ) {
        return handleCustomExceptionInternal(
            new Exception("Missing servlet request parameter"),
            request,
            HttpStatus.BAD_REQUEST,
            headers,
            Map.of(ex.getParameterName(), List.of("parameter is missing"))
        );
    }

    @ExceptionHandler({ConstraintViolationException.class})
    public ResponseEntity<Object> handleConstraintViolation(
        final ConstraintViolationException ex,
        final WebRequest request
    ) {
        final Map<String, List<String>> errors = new HashMap<>();
        ex.getConstraintViolations().forEach(violation ->
            errors
                .computeIfAbsent(violation.getPropertyPath().toString(), k -> new ArrayList<>(5))
                .add(violation.getMessage())
        );
        return handleCustomExceptionInternal(
            new Exception("Constraint violation"),
            request,
            HttpStatus.BAD_REQUEST,
            new HttpHeaders(),
            errors
        );
    }

    @ExceptionHandler({MethodArgumentTypeMismatchException.class})
    public ResponseEntity<Object> handleMethodArgumentTypeMismatch(
        final MethodArgumentTypeMismatchException ex,
        final WebRequest request
    ) {
        return handleCustomExceptionInternal(
            new Exception("Method argument type mismatch"),
            request,
            HttpStatus.BAD_REQUEST,
            new HttpHeaders(),
            Map.of(
                ex.getName(),
                List.of(String.format(
                    "should be of type %s",
                    Objects.requireNonNull(ex.getRequiredType()).getName()
                ))
            )
        );
    }

    protected <E extends Exception> ResponseEntity<Object> handleCustomExceptionInternal(
        final E ex,
        final WebRequest request,
        final HttpStatusCode status,
        final HttpHeaders headers,
        final Map<String, List<String>> errors
    ) {
        String error;
        try {
            error = errors != null ? objectMapper.writeValueAsString(errors) : ex.getLocalizedMessage();
        } catch (final Exception e) {
            error = ex.getLocalizedMessage();
        }
        return handleExceptionInternal(
            ex,
            new AppProblem(error),
            headers,
            status,
            request
        );
    }

    @ExceptionHandler({
        ResourceBadRequestException.class,
        ResourceNotFoundException.class,
        UnexpectedTypeException.class,
        SQLIntegrityConstraintViolationException.class
    })
    public ResponseEntity<Object> handleCustomException(final Exception ex, final WebRequest request) {
        return handleCustomExceptionInternal(
            ex,
            request,
            ex instanceof ResourceNotFoundException ? HttpStatus.NOT_FOUND : HttpStatus.BAD_REQUEST,
            new HttpHeaders(),
            null
        );
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(
        final Exception ex,
        @Nullable Object body,
        final HttpHeaders headers,
        final HttpStatusCode statusCode,
        final WebRequest request
    ) {
        logger.error("APPLICATION EXCEPTION", ex);
        if (!(body instanceof AppProblem)) {
            body = new AppProblem(ex.getLocalizedMessage());
        }
        return super.handleExceptionInternal(ex, body, headers, statusCode, request);
    }

    record AppProblem(String error) {
    }
}
