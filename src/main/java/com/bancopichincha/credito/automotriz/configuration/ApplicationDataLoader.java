package com.bancopichincha.credito.automotriz.configuration;

import com.bancopichincha.credito.automotriz.domain.Brand;
import com.bancopichincha.credito.automotriz.domain.CarDealership;
import com.bancopichincha.credito.automotriz.domain.Customer;
import com.bancopichincha.credito.automotriz.domain.SalesExecutive;
import com.bancopichincha.credito.automotriz.exception.CsvLoadingException;
import com.bancopichincha.credito.automotriz.repository.BrandRepository;
import com.bancopichincha.credito.automotriz.repository.CarDealershipRepository;
import com.bancopichincha.credito.automotriz.repository.CustomerRepository;
import com.bancopichincha.credito.automotriz.repository.SalesExecutiveRepository;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
@Profile({"!test"})
@RequiredArgsConstructor
@Getter
@Slf4j
public class ApplicationDataLoader implements ApplicationListener<ContextRefreshedEvent> {
    private final ApplicationProperties appProperties;
    private boolean alreadySetup;
    private final BrandRepository brandRepository;
    private final CustomerRepository customerRepository;
    private final CarDealershipRepository carDealershipRepository;
    private final SalesExecutiveRepository salesExecutiveRepository;

    @Override
    @Transactional
    public void onApplicationEvent(final ContextRefreshedEvent event) {
        if (alreadySetup) {
            return;
        }
        if (brandRepository.count() == 0) {
            processBrands();
        }
        if (customerRepository.count() == 0) {
            processCustomers();
        }
        if (carDealershipRepository.count() == 0) {
            processCarDealerships();
        }
        if (salesExecutiveRepository.count() == 0) {
            processSalesExecutives();
        }
        alreadySetup = true;
    }

    @Transactional
    private void processBrands() throws CsvLoadingException {
        loadDataFromCsv(Brand.class, appProperties.getCsv().getFilenames().getBrands())
            .stream()
            .distinct()
            .forEach(brandRepository::save);
    }

    @Transactional
    private void processCustomers() throws CsvLoadingException {
        loadDataFromCsv(Customer.class, appProperties.getCsv().getFilenames().getCustomers())
            .stream()
            .distinct()
            .forEach(customer -> {
                if ("".equals(customer.getSpouseIdNumber())) {
                    customer.setSpouseIdNumber(null);
                }
                if ("".equals(customer.getSpouseNames())) {
                    customer.setSpouseNames(null);
                }
                customerRepository.save(customer);
            });
    }

    @Transactional
    private void processCarDealerships() throws CsvLoadingException {
        loadDataFromCsv(CarDealership.class, appProperties.getCsv().getFilenames().getCarDealerships())
            .forEach(carDealershipRepository::save);
    }

    @Transactional
    private void processSalesExecutives() throws CsvLoadingException {
        final List<SalesExecutive> salesExecutives = loadDataFromCsv(
            SalesExecutive.class,
            appProperties.getCsv().getFilenames().getSalesExecutives()
        );
        final List<Integer> carDealershipIds = salesExecutives
            .stream()
            .distinct()
            .map(SalesExecutive::getCarDealershipId)
            .collect(Collectors.toList());
        final Map<Integer, CarDealership> carDealershipsMap = carDealershipRepository.findByIdInMap(carDealershipIds);
        salesExecutives
            .forEach(salesExecutive -> {
                salesExecutive.setCarDealership(carDealershipsMap.get(salesExecutive.getCarDealershipId()));
                salesExecutiveRepository.save(salesExecutive);
            });
    }

    /**
     * Loads data from CSV file.
     * @param objectType Object type.
     * @param filename CSV filename.
     * @return List of objects.
     * @param <T> Object template.
     * @throws CsvLoadingException If CSV file cannot be processed.
     */
    private <T> List<T> loadDataFromCsv(final Class<T> objectType, final String filename) throws CsvLoadingException {
        try {
            final CsvSchema bootstrapSchema = CsvSchema.emptySchema().withHeader();
            final File file = new ClassPathResource(appProperties.getCsv().getPath() + "/" + filename).getFile();
            final MappingIterator<T> readValues = new CsvMapper()
                .registerModule(new JavaTimeModule())
                .readerFor(objectType)
                .with(bootstrapSchema)
                .readValues(file);
            return readValues.readAll();
        } catch (final IOException e) {
            throw new CsvLoadingException(filename, e);
        }
    }
}
