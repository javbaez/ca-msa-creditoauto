package com.bancopichincha.credito.automotriz.configuration;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import static lombok.AccessLevel.PRIVATE;

@Component("appProperties")
@Data
@FieldDefaults(level = PRIVATE)
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
@Validated
public class ApplicationProperties {
    @NotNull
    Csv csv;

    @Data
    @FieldDefaults(level = PRIVATE)
    @Validated
    public static class Csv {
        @NotBlank
        String path;
        @NotNull
        Filenames filenames;

        @Data
        @FieldDefaults(level = PRIVATE)
        @Validated
        public static class Filenames {
            @NotBlank
            String brands;
            @NotBlank
            String customers;
            @NotBlank
            String carDealerships;
            @NotBlank
            String salesExecutives;
        }
    }
}
