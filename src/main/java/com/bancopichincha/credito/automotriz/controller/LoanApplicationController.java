package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.service.LoanApplicationService;
import com.bancopichincha.credito.automotriz.service.dto.LoanApplicationDto;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.CreateLoan;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.UpdateLoan;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/solicitudCredito")
@RequiredArgsConstructor
@Slf4j
@Validated
public class LoanApplicationController {
    private final LoanApplicationService loanApplicationService;

    @GetMapping("/buscar")
    public LoanApplicationDto findById(@RequestParam("id") @NotNull @Min(1) final Integer id) {
        log.debug("findById() - START: id = {}", id);
        return loanApplicationService.findById(id);
    }

    @PostMapping("/crear")
    public ResponseEntity<LoanApplicationDto> create(
        @RequestBody @NotNull @Validated(CreateLoan.class) final LoanApplicationDto dto
    ) {
        log.debug("create() - START");
        return new ResponseEntity<>(loanApplicationService.create(dto), HttpStatus.CREATED);
    }

    @PutMapping("/actualizar")
    public LoanApplicationDto update(@RequestBody @NotNull @Validated(UpdateLoan.class) final LoanApplicationDto dto) {
        log.debug("update() - START: id = {}", dto.getId());
        return loanApplicationService.update(dto);
    }

    @DeleteMapping("/eliminar")
    public LoanApplicationDto delete(@RequestParam("id") @NotNull @Min(1) final Integer id) {
        log.debug("delete() - START: id = {}", id);
        return loanApplicationService.delete(id);
    }
}
