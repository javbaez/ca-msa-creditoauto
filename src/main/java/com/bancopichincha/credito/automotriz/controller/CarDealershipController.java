package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.service.CarDealershipService;
import com.bancopichincha.credito.automotriz.service.dto.CarDealershipDto;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.Create;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.Update;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/patio")
@RequiredArgsConstructor
@Slf4j
@Validated
public class CarDealershipController {
    private final CarDealershipService carDealershipService;

    @GetMapping("/buscar")
    public CarDealershipDto findById(@RequestParam("id") @NotNull @Min(1) final Integer id) {
        log.debug("findById() - START: id = {}", id);
        return carDealershipService.findById(id);
    }

    @PostMapping("/crear")
    public ResponseEntity<CarDealershipDto> create(@RequestBody @NotNull @Validated(Create.class) final CarDealershipDto dto) {
        log.debug("create() - START");
        return new ResponseEntity<>(carDealershipService.create(dto), HttpStatus.CREATED);
    }

    @PutMapping("/actualizar")
    public CarDealershipDto update(@RequestBody @NotNull @Validated(Update.class) final CarDealershipDto dto) {
        log.debug("update() - START: id = {}", dto.getId());
        return carDealershipService.update(dto);
    }

    @DeleteMapping("/eliminar")
    public boolean delete(@RequestParam("id") @NotNull @Min(1) final Integer id) {
        log.debug("delete() - START: id = {}", id);
        return carDealershipService.delete(id) != null;
    }
}
