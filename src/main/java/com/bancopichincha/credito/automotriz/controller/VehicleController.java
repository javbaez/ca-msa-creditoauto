package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.service.VehicleService;
import com.bancopichincha.credito.automotriz.service.dto.BrandDto;
import com.bancopichincha.credito.automotriz.service.dto.VehicleDto;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.CreateVehicle;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.Find;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.UpdateVehicle;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/vehiculo")
@RequiredArgsConstructor
@Slf4j
@Validated
public class VehicleController {
    private final VehicleService vehicleService;

    @GetMapping("/buscar")
    public VehicleDto findById(@RequestParam("id") @NotNull @Min(1) final Integer id) {
        log.debug("findById() - START: id = {}", id);
        return vehicleService.findById(id);
    }

    @GetMapping("/consultaPorModelo")
    public List<VehicleDto> findByModel(@RequestParam("modelo") @NotBlank @Size(max = 50) final String model) {
        log.debug("findByModel() - START: model = {}", model);
        return vehicleService.findByModel(model);
    }

    @GetMapping("/consultaPorMarca")
    public List<VehicleDto> findByBrandId(@RequestBody @NotNull @Validated(Find.class) final BrandDto dto) {
        log.debug("findByBrandId() - START: id = {}", dto.getId());
        return vehicleService.findByBrandId(dto.getId());
    }

    @PostMapping("/crear")
    public ResponseEntity<VehicleDto> create(@RequestBody @NotNull @Validated(CreateVehicle.class) final VehicleDto dto) {
        log.debug("create() - START");
        return new ResponseEntity<>(vehicleService.create(dto), HttpStatus.CREATED);
    }

    @PutMapping("/actualizar")
    public VehicleDto update(@RequestBody @NotNull @Validated(UpdateVehicle.class) final VehicleDto dto) {
        log.debug("update() - START: id = {}", dto.getId());
        return vehicleService.update(dto);
    }

    @DeleteMapping("/eliminar")
    public boolean delete(@RequestParam("id") @NotNull @Min(1) final Integer id) {
        log.debug("delete() - START: id = {}", id);
        return vehicleService.delete(id) != null;
    }
}
