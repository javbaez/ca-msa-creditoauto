package com.bancopichincha.credito.automotriz.controller;

import com.bancopichincha.credito.automotriz.service.CustomerAssignmentService;
import com.bancopichincha.credito.automotriz.service.dto.CustomerAssignmentDto;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.Create;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.Update;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/clientePatio")
@RequiredArgsConstructor
@Slf4j
@Validated
public class CustomerAssignmentController {
    private final CustomerAssignmentService customerAssignmentService;

    @GetMapping("/buscar")
    public CustomerAssignmentDto findById(@RequestParam("id") @NotNull @Min(1) final Integer id) {
        log.debug("findById() - START: id = {}", id);
        return customerAssignmentService.findById(id);
    }

    @PostMapping("/crear")
    public ResponseEntity<CustomerAssignmentDto> create(@RequestBody @NotNull @Validated(Create.class) final CustomerAssignmentDto dto) {
        log.debug("create() - START");
        return new ResponseEntity<>(customerAssignmentService.create(dto), HttpStatus.CREATED);
    }

    @PutMapping("/actualizar")
    public CustomerAssignmentDto update(@RequestBody @NotNull @Validated(Update.class) final CustomerAssignmentDto dto) {
        log.debug("update() - START: id = {}", dto.getId());
        return customerAssignmentService.update(dto);
    }

    @DeleteMapping("/eliminar")
    public String delete(@RequestParam("id") @NotNull @Min(1) final Integer id) {
        log.debug("delete() - START: id = {}", id);
        return customerAssignmentService.delete(id) != null ? "Eliminado correctamente" : "No eliminado";
    }
}
