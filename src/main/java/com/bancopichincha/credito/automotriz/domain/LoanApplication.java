package com.bancopichincha.credito.automotriz.domain;

import com.bancopichincha.credito.automotriz.domain.enums.LoanStatus;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Entity
@Table(name = "solicitud_credito")
@Data
public class LoanApplication {
    @Column(name = "id", nullable = false)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "fecha", nullable = false)
    @NotNull
    @FutureOrPresent
    private LocalDate date;
    @JoinColumn(
        name = "cliente_id",
        referencedColumnName = "id",
        nullable = false,
        foreignKey = @ForeignKey(name = "CF_SOLICITUD_CREDITO_A_CLIENTE")
    )
    @ManyToOne
    @NotNull
    private Customer customer;
    @JoinColumn(
        name = "patio_id",
        referencedColumnName = "id",
        nullable = false,
        foreignKey = @ForeignKey(name = "CF_SOLICITUD_CREDITO_A_PATIO")
    )
    @ManyToOne
    @NotNull
    private CarDealership carDealership;
    @JoinColumn(
        name = "ejecutivo_ventas_id",
        referencedColumnName = "id",
        nullable = false,
        foreignKey = @ForeignKey(name = "CF_SOLICITUD_CREDITO_A_EJECUTIVO_VENTAS")
    )
    @ManyToOne
    @NotNull
    private SalesExecutive salesExecutive;
    @JoinColumn(
        name = "vehiculo_id",
        referencedColumnName = "id",
        nullable = false,
        foreignKey = @ForeignKey(name = "CF_SOLICITUD_CREDITO_A_VEHICULO")
    )
    @ManyToOne
    @NotNull
    private Vehicle vehicle;
    @Column(name = "meses_plazo", nullable = false)
    @NotNull
    @Min(3)
    @Max(60)
    private Byte monthsTerm;
    @Column(name = "cuotas", nullable = false)
    @NotNull
    @Min(3)
    @Max(240)
    private Short installments;
    @Column(name = "entrada", nullable = false)
    @NotNull
    @DecimalMin("500.00")
    private BigDecimal downPayment;
    @Column(name = "observacion", length = 250)
    @Size(max = 250)
    private String observation;
    @Column(name = "estado", nullable = false)
    @NotNull
    private LoanStatus status;
}
