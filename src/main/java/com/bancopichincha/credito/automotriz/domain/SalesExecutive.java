package com.bancopichincha.credito.automotriz.domain;

import com.bancopichincha.credito.automotriz.service.dto.validation.annotation.Age;
import com.bancopichincha.credito.automotriz.service.dto.validation.annotation.IdNumber;
import com.bancopichincha.credito.automotriz.service.dto.validation.annotation.Phone;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.util.Objects;

@Entity
@Table(
    name = "ejecutivo_ventas",
    uniqueConstraints = {@UniqueConstraint(name = "EVT_IDENTIFICACION_INDICE", columnNames = {"identificacion"})}
)
@Data
public class SalesExecutive {
    @Column(name = "id", nullable = false)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("id")
    private Integer id;
    @Column(name = "identificacion", nullable = false, length = 10)
    @NotBlank
    @IdNumber
    @JsonProperty("identificacion")
    private String idNumber;
    @Column(name = "nombres", nullable = false, length = 100)
    @NotBlank
    @Size(max = 100)
    @JsonProperty("nombres")
    private String names;
    @Column(name = "apellidos", nullable = false, length = 100)
    @NotBlank
    @Size(max = 100)
    @JsonProperty("apellidos")
    private String lastNames;
    @Column(name = "edad", nullable = false)
    @NotNull
    @Age
    @JsonProperty("edad")
    private Byte age;
    @Column(name = "direccion", nullable = false, length = 200)
    @NotBlank
    @Size(max = 200)
    @JsonProperty("direccion")
    private String address;
    @Column(name = "telefono_convencional", nullable = false, length = 10)
    @NotBlank
    @Phone(max = 9)
    @JsonProperty("telefono_convencional")
    private String landline;
    @Column(name = "celular", nullable = false, length = 10)
    @NotBlank
    @Phone(min = 10)
    @JsonProperty("celular")
    private String mobile;
    @JoinColumn(
        name = "patio_id",
        referencedColumnName = "id",
        nullable = false,
        foreignKey = @ForeignKey(name = "CF_EJECUTIVO_VENTAS_A_PATIO")
    )
    @ManyToOne
    @NotNull
    private CarDealership carDealership;
    @Transient
    @JsonProperty("patio_id")
    private Integer carDealershipId;

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof final SalesExecutive salesExecutive)) {
            return false;
        }
        return Objects.equals(idNumber, salesExecutive.getIdNumber());
    }

    @Override
    public int hashCode() {
        return Objects.hash(idNumber);
    }
}
