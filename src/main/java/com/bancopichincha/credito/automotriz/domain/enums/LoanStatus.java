package com.bancopichincha.credito.automotriz.domain.enums;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@RequiredArgsConstructor
@Getter
public enum LoanStatus {
    @JsonProperty("R")
    REGISTERED("R"),
    @JsonProperty("D")
    DISPATCHED("D"),
    @JsonProperty("C")
    CANCELLED("C");

    private final String loanStatus;

    /**
     * Converts custom loan status value to load status enum.
     * @param loanStatus Custom loan status value.
     * @return Loan status enum.
     * @throws IllegalArgumentException If custom loan status value is not found.
     */
    public static LoanStatus of(final String loanStatus) throws IllegalArgumentException {
        return Stream.of(LoanStatus.values())
            .filter(value -> value.getLoanStatus().equals(loanStatus))
            .findFirst()
            .orElseThrow(IllegalArgumentException::new);
    }
}
