package com.bancopichincha.credito.automotriz.domain;

import com.bancopichincha.credito.automotriz.service.dto.validation.annotation.Phone;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import lombok.Data;

import java.util.Objects;

@Entity
@Table(name = "patio")
@Data
public class CarDealership {
    @Column(name = "id", nullable = false)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("id")
    private Integer id;
    @Column(name = "nombre", nullable = false, length = 100)
    @NotBlank
    @Size(max = 100)
    @JsonProperty("nombre")
    private String name;
    @Column(name = "direccion", nullable = false)
    @NotBlank
    @Size(max = 255)
    @JsonProperty("direccion")
    private String address;
    @Column(name = "telefono", nullable = false, length = 10)
    @NotBlank
    @Phone
    @JsonProperty("telefono")
    private String phone;
    @Column(name = "numero_punto_venta", nullable = false, length = 10)
    @NotNull
    @Min(1)
    @Max(1_000_000)
    @JsonProperty("numero_punto_venta")
    private Integer pointOfSaleNumber;

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof final CarDealership carDealership)) {
            return false;
        }
        return Objects.equals(id, carDealership.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
