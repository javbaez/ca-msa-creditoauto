package com.bancopichincha.credito.automotriz.domain.enums;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.stream.Stream;

@RequiredArgsConstructor
@Getter
public enum CivilStatus {
    @JsonProperty("S")
    SINGLE("S"),
    @JsonProperty("C")
    MARRIED("C"),
    @JsonProperty("D")
    DIVORCED("D"),
    @JsonProperty("V")
    WIDOWED("V");

    private final String civilStatus;

    /**
     * Converts custom civil status value to civil status enum.
     * @param civilStatus Custom civil status value.
     * @return Civil status enum.
     * @throws IllegalArgumentException If custom civil status value is not found.
     */
    public static CivilStatus of(final String civilStatus) throws IllegalArgumentException {
        return Stream.of(CivilStatus.values())
            .filter(value -> value.getCivilStatus().equals(civilStatus))
            .findFirst()
            .orElseThrow(IllegalArgumentException::new);
    }
}
