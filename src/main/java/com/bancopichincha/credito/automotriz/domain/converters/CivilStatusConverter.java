package com.bancopichincha.credito.automotriz.domain.converters;

import com.bancopichincha.credito.automotriz.domain.enums.CivilStatus;
import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

@Converter(autoApply = true)
public class CivilStatusConverter implements AttributeConverter<CivilStatus, String> {
    @Override
    public String convertToDatabaseColumn(final CivilStatus civilStatus) {
        return civilStatus != null ? civilStatus.getCivilStatus() : null;
    }

    @Override
    public CivilStatus convertToEntityAttribute(final String civilStatus) {
        return civilStatus != null ? CivilStatus.of(civilStatus) : null;
    }
}
