package com.bancopichincha.credito.automotriz.domain.converters;

import com.bancopichincha.credito.automotriz.domain.enums.LoanStatus;
import jakarta.persistence.AttributeConverter;
import jakarta.persistence.Converter;

@Converter(autoApply = true)
public class LoanStatusConverter implements AttributeConverter<LoanStatus, String> {
    @Override
    public String convertToDatabaseColumn(final LoanStatus loanStatus) {
        return loanStatus != null ? loanStatus.getLoanStatus() : null;
    }

    @Override
    public LoanStatus convertToEntityAttribute(final String loanStatus) {
        return loanStatus != null ? LoanStatus.of(loanStatus) : null;
    }
}
