package com.bancopichincha.credito.automotriz.domain;

import com.bancopichincha.credito.automotriz.domain.enums.CivilStatus;
import com.bancopichincha.credito.automotriz.service.dto.validation.annotation.Age;
import com.bancopichincha.credito.automotriz.service.dto.validation.annotation.IdNumber;
import com.bancopichincha.credito.automotriz.service.dto.validation.annotation.Phone;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import lombok.Data;

import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(
    name = "cliente",
    uniqueConstraints = {@UniqueConstraint(name = "CLI_IDENTIFICACION_INDICE", columnNames = {"identificacion"})}
)
@Data
public class Customer {
    @Column(name = "id", nullable = false)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("id")
    private Integer id;
    @Column(name = "identificacion", nullable = false, length = 10)
    @NotBlank
    @IdNumber
    @JsonProperty("identificacion")
    private String idNumber;
    @Column(name = "nombres", nullable = false, length = 100)
    @NotBlank
    @Size(max = 100)
    @JsonProperty("nombres")
    private String names;
    @Column(name = "apellidos", nullable = false, length = 100)
    @NotBlank
    @Size(max = 100)
    @JsonProperty("apellidos")
    private String lastNames;
    @Column(name = "edad", nullable = false)
    @NotNull
    @Age
    @JsonProperty("edad")
    private Byte age;
    @Column(name = "fecha_nacimiento", nullable = false)
    @NotNull
    @Past
    @JsonProperty("fecha_nacimiento")
    private LocalDate birthday;
    @Column(name = "direccion", nullable = false, length = 200)
    @NotBlank
    @Size(max = 200)
    @JsonProperty("direccion")
    private String address;
    @Column(name = "telefono", nullable = false, length = 10)
    @NotBlank
    @Phone
    @JsonProperty("telefono")
    private String phone;
    @Column(name = "estado_civil", nullable = false, length = 2)
    @NotNull
    @JsonProperty("estado_civil")
    private CivilStatus civilStatus;
    @Column(name = "identificacion_conyugue", length = 10)
    @IdNumber
    @JsonProperty("identificacion_conyugue")
    private String spouseIdNumber;
    @Column(name = "nombre_conyugue", length = 200)
    @Size(max = 200)
    @JsonProperty("nombre_conyugue")
    private String spouseNames;
    @Column(name = "sujeto_credito", nullable = false)
    @NotNull
    @JsonProperty("sujeto_credito")
    private Boolean creditSubject;

    @AssertTrue(message = "Spouse data is incorrect")
    public boolean isSpouseDataValid() {
        if (civilStatus == CivilStatus.MARRIED) {
            return spouseIdNumber != null && spouseNames != null;
        }
        return spouseIdNumber == null && spouseNames == null;
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof final Customer customer)) {
            return false;
        }
        return Objects.equals(idNumber, customer.getIdNumber());
    }

    @Override
    public int hashCode() {
        return Objects.hash(idNumber);
    }
}
