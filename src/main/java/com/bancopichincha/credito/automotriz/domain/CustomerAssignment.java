package com.bancopichincha.credito.automotriz.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PastOrPresent;
import lombok.Data;

import java.time.LocalDate;

@Entity
@Table(
    name = "asignacion_cliente",
    uniqueConstraints = {@UniqueConstraint(name = "ACL_CLIENTE_PATIO_INDICE", columnNames = {"cliente_id", "patio_id"})}
)
@Data
public class CustomerAssignment {
    @Column(name = "id", nullable = false)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("id")
    private Integer id;
    @JoinColumn(
        name = "cliente_id",
        referencedColumnName = "id",
        nullable = false,
        foreignKey = @ForeignKey(name = "CF_ASIGNACION_CLIENTE_A_CLIENTE")
    )
    @ManyToOne
    @NotNull
    private Customer customer;
    @JoinColumn(
        name = "patio_id",
        referencedColumnName = "id",
        nullable = false,
        foreignKey = @ForeignKey(name = "CF_ASIGNACION_CLIENTE_A_PATIO")
    )
    @ManyToOne
    @NotNull
    private CarDealership carDealership;
    @Column(name = "fecha_asignacion", nullable = false)
    @NotNull
    @PastOrPresent
    private LocalDate assignmentDate;
}
