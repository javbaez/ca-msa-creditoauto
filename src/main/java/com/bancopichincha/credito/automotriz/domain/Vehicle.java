package com.bancopichincha.credito.automotriz.domain;

import com.bancopichincha.credito.automotriz.service.dto.validation.annotation.Plate;
import jakarta.persistence.*;
import jakarta.validation.constraints.*;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(
    name = "vehiculo",
    uniqueConstraints = {@UniqueConstraint(name = "VCL_PLACA_INDICE", columnNames = {"placa"})},
    indexes = {@Index(name = "VCL_MODELO_INDICE", columnList = "modelo")}
)
@Data
public class Vehicle {
    @Column(name = "id", nullable = false)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "placa", nullable = false, length = 8)
    @NotBlank
    @Plate
    private String plate;
    @Column(name = "modelo", nullable = false, length = 50)
    @NotBlank
    @Size(max = 50)
    private String model;
    @Column(name = "numero_chasis", nullable = false, length = 50)
    @NotBlank
    @Size(max = 50)
    private String chassisNumber;
    @Column(name = "tipo", length = 25)
    @Size(max = 25)
    private String type;
    @Column(name = "cilindraje", nullable = false)
    @NotNull
    @DecimalMin("0.1")
    @DecimalMax("20.0")
    private BigDecimal cylinderCapacity;
    @Column(name = "avaluo", nullable = false)
    @NotNull
    @Min(1)
    @Max(100_000_000)
    private Integer appraisal;
    @JoinColumn(
        name = "marca_id",
        referencedColumnName = "id",
        nullable = false,
        foreignKey = @ForeignKey(name = "CF_VEHICULO_A_MARCA")
    )
    @OneToOne
    @NotNull
    private Brand brand;

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof final Vehicle vehicle)) {
            return false;
        }
        return Objects.equals(plate, vehicle.getPlate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(plate);
    }
}
