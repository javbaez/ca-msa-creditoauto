package com.bancopichincha.credito.automotriz.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;

import java.util.Objects;

@Entity
@Table(
    name = "marca",
    uniqueConstraints = {@UniqueConstraint(name = "MRC_NOMBRE_INDICE", columnNames = {"nombre"})}
)
@Data
public class Brand {
    @Column(name = "id", nullable = false)
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonProperty("id")
    private Integer id;
    @Column(name = "nombre", nullable = false, length = 50)
    @NotBlank
    @Size(max = 50)
    @JsonProperty("nombre")
    private String name;

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (!(object instanceof final Brand brand)) {
            return false;
        }
        return Objects.equals(name, brand.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
