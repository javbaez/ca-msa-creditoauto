package com.bancopichincha.credito.automotriz.exception;

public class CarDealershipReferenceToSalesExecutiveException extends ResourceBadRequestException {
    public CarDealershipReferenceToSalesExecutiveException(final Integer carDealershipId) {
        super(String.format(
            "No se puede eliminar porque el patio con id:%s tiene una referencia con ejecutivo",
            carDealershipId
        ));
    }
}
