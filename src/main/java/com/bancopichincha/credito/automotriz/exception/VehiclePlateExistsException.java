package com.bancopichincha.credito.automotriz.exception;

import lombok.Getter;

@Getter
public class VehiclePlateExistsException extends ResourceBadRequestException {
    private final String plate;

    public VehiclePlateExistsException(final String plate) {
        super(String.format("Ya existe un vehículo con placa: %s", plate));
        this.plate = plate;
    }
}
