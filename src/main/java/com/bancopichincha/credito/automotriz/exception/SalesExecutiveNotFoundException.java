package com.bancopichincha.credito.automotriz.exception;

public class SalesExecutiveNotFoundException extends ResourceNotFoundException {
    public SalesExecutiveNotFoundException(final Integer salesExecutiveId) {
        super(String.format("Ejecutivo de ventas [%s] no encontrado", salesExecutiveId));
    }
}
