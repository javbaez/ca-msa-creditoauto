package com.bancopichincha.credito.automotriz.exception;

public class VehicleNotFoundException extends ResourceNotFoundException {
    public VehicleNotFoundException(final Integer vehicleId) {
        super(String.format("Vehículo [%s] no encontrado", vehicleId));
    }
}
