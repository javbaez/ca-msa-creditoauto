package com.bancopichincha.credito.automotriz.exception;

public class VehicleReservedForLoanException extends ResourceBadRequestException {
    public VehicleReservedForLoanException() {
        super("No se puede actualizar el vehículo porque ya está reservado para un crédito");
    }
}
