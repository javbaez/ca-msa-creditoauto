package com.bancopichincha.credito.automotriz.exception;

public class CarDealershipNotFoundException extends ResourceNotFoundException {
    public CarDealershipNotFoundException(final Integer carDealershipId) {
        super(String.format("Patio [%s] no encontrado", carDealershipId));
    }
}
