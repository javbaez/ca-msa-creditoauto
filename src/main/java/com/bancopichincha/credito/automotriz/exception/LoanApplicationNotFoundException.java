package com.bancopichincha.credito.automotriz.exception;

public class LoanApplicationNotFoundException extends ResourceNotFoundException {
    public LoanApplicationNotFoundException(final Integer loadApplicationId) {
        super(String.format("Solicitud de crédito [%s] no encontrada", loadApplicationId));
    }
}
