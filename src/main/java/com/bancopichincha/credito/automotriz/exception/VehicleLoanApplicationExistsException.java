package com.bancopichincha.credito.automotriz.exception;

public class VehicleLoanApplicationExistsException extends ResourceBadRequestException {
    public VehicleLoanApplicationExistsException() {
        super("Existe una solicitud de crédito con el vehículo que desea eliminar");
    }
}
