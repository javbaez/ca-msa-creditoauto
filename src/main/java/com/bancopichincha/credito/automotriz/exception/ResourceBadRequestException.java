package com.bancopichincha.credito.automotriz.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ResourceBadRequestException extends RuntimeException {
    public ResourceBadRequestException() {
        this("Solicitud incorrecta");
    }

    public ResourceBadRequestException(final String message) {
        super(message);
    }
}
