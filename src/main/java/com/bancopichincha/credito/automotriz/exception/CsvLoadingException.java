package com.bancopichincha.credito.automotriz.exception;

public class CsvLoadingException extends RuntimeException {
    public CsvLoadingException(final String filename, final Exception exception) {
        super(String.format(
            "Error occurred while loading data from file = %s, error = %s",
            filename,
            exception
        ));
    }

    public CsvLoadingException(final Exception exception) {
        super(exception);
    }
}
