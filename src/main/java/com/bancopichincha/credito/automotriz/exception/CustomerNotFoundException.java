package com.bancopichincha.credito.automotriz.exception;

public class CustomerNotFoundException extends ResourceNotFoundException {
    public CustomerNotFoundException(final Integer customerId) {
        super(String.format("Cliente [%s] no encontrado", customerId));
    }
}
