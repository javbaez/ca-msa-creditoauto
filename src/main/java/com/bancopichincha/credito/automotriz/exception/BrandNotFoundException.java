package com.bancopichincha.credito.automotriz.exception;

public class BrandNotFoundException extends ResourceNotFoundException {
    public BrandNotFoundException(final Integer brandId) {
        super(String.format("Marca [%s] no encontrada", brandId));
    }
}
