package com.bancopichincha.credito.automotriz.exception;

public class VehicleReservedForAnotherLoanException extends ResourceBadRequestException {
    public VehicleReservedForAnotherLoanException() {
        super("El auto que envió para la solicitud de crédito ya está reservado para otro");
    }
}
