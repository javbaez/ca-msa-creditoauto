package com.bancopichincha.credito.automotriz.exception;

public class CustomerAssignmentNotFoundException extends ResourceNotFoundException {
    public CustomerAssignmentNotFoundException(final Integer customerAssignment) {
        super(String.format("Asignación de cliente [%s] no encontrada", customerAssignment));
    }
}
