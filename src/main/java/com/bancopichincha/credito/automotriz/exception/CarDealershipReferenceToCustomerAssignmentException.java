package com.bancopichincha.credito.automotriz.exception;

public class CarDealershipReferenceToCustomerAssignmentException extends ResourceBadRequestException {
    public CarDealershipReferenceToCustomerAssignmentException(final Integer carDealershipId) {
        super(String.format(
            "No se puede eliminar porque el patio con id:%s tiene una referencia con asignación de cliente",
            carDealershipId
        ));
    }
}
