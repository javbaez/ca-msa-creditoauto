package com.bancopichincha.credito.automotriz.exception;

public class LoanApplicationExistsException extends ResourceBadRequestException {
    public LoanApplicationExistsException() {
        super("Ya existe una solicitud de el cliente en la fecha enviada");
    }
}
