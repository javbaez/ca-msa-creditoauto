package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.CarDealership;
import com.bancopichincha.credito.automotriz.domain.SalesExecutive;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SalesExecutiveRepository extends JpaRepository<SalesExecutive, Integer> {
    /**
     * Finds sales executive by ID and car dealership.
     * @param id Sales executive ID.
     * @param carDealership Car dealership.
     * @return Sales executive.
     */
    Optional<SalesExecutive> findByIdAndCarDealership(Integer id, CarDealership carDealership);

    /**
     * Finds sales executives by car dealership.
     * @param carDealership Car dealership.
     * @return List of sales executives.
     */
    List<SalesExecutive> findByCarDealership(CarDealership carDealership);

    /**
     * Checks if sales executives exist by car dealership ID.
     * @param carDealershipId Car dealership ID.
     * @return Returns TRUE if sales executives exist.
     */
    @Query(
        value = "SELECT COUNT(*) > 0 FROM ejecutivo_ventas WHERE patio_id = :carDealershipId",
        nativeQuery = true
    )
    boolean checkIfExistsByCarDealershipId(@Param("carDealershipId") Integer carDealershipId);
}
