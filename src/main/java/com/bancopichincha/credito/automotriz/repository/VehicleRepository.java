package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.Vehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface VehicleRepository extends JpaRepository<Vehicle, Integer> {
    /**
     * Finds a vehicle by plate.
     * @param plate Vehicle plate.
     * @return Vehicle.
     */
    Optional<Vehicle> findByPlate(String plate);

    /**
     * Finds vehicles by model.
     * @param model Vehicle model.
     * @return List of vehicles.
     */
    List<Vehicle> findByModel(String model);

    /**
     * Finds vehicles by brand ID.
     * @param brandId Brand ID.
     * @return List of vehicles.
     */
    List<Vehicle> findByBrandId(Integer brandId);
}
