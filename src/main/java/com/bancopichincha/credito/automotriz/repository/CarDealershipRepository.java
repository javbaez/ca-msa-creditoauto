package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.CarDealership;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public interface CarDealershipRepository extends JpaRepository<CarDealership, Integer> {
    /**
     * Finds car dealerships by IDs.
     * @param ids Car dealership IDs.
     * @return List of car dealerships.
     */
    List<CarDealership> findByIdIn(List<Integer> ids);

    /**
     * Finds car dealerships by IDs and maps the results.
     * @param ids Car dealership IDs.
     * @return Map of car dealerships (key = Car dealership ID, value = Car dealership).
     */
    default Map<Integer, CarDealership> findByIdInMap(final List<Integer> ids) {
        return findByIdIn(ids)
            .stream()
            .collect(Collectors.toMap(CarDealership::getId, value -> value));
    }
}
