package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.LoanApplication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface LoanApplicationRepository extends JpaRepository<LoanApplication, Integer> {
    /**
     * Checks if customer has valid applications by date.
     * @param customerId Customer ID.
     * @param date Date.
     * @return Returns TRUE if customer has valid applications.
     */
    @Query(
        value = "SELECT COUNT(*) > 0 FROM solicitud_credito WHERE cliente_id = :customerId AND fecha = :date AND estado = 'R'",
        nativeQuery = true
    )
    boolean checkIfCustomerHasValidApplicationByDate(
        @Param("customerId") Integer customerId,
        @Param("date") LocalDate date
    );

    /**
     * Checks if vehicle is reserved.
     * @param vehicleId Vehicle ID.
     * @return Returns TRUE if vehicle is reserved.
     */
    @Query(
        value = "SELECT COUNT(*) > 0 FROM solicitud_credito WHERE vehiculo_id = :vehicleId AND estado = 'R'",
        nativeQuery = true
    )
    boolean checkIfVehicleIsReserved(@Param("vehicleId") Integer vehicleId);
}
