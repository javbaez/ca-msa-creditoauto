package com.bancopichincha.credito.automotriz.repository;

import com.bancopichincha.credito.automotriz.domain.CustomerAssignment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CustomerAssignmentRepository extends JpaRepository<CustomerAssignment, Integer> {
    /**
     * Checks if car dealership has assignments.
     * @param carDealershipId Car dealership ID.
     * @return Returns TRUE if car dealership has assignments.
     */
    @Query(
        value = "SELECT COUNT(*) > 0 FROM asignacion_cliente WHERE patio_id = :carDealershipId",
        nativeQuery = true
    )
    boolean checkIfCarDealershipHasAssignments(@Param("carDealershipId") Integer carDealershipId);
}
