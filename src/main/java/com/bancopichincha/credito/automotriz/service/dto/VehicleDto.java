package com.bancopichincha.credito.automotriz.service.dto;

import com.bancopichincha.credito.automotriz.service.dto.validation.annotation.Plate;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.CreateLoan;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.CreateVehicle;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.UpdateLoan;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.UpdateVehicle;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.*;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class VehicleDto {
    @NotNull(groups = {UpdateVehicle.class, CreateLoan.class, UpdateLoan.class})
    @Min(value = 1, groups = {UpdateVehicle.class, CreateLoan.class, UpdateLoan.class})
    @JsonProperty("id")
    private Integer id;
    @NotBlank(groups = {CreateVehicle.class, UpdateVehicle.class})
    @Plate(groups = {CreateVehicle.class, UpdateVehicle.class})
    @JsonProperty("placa")
    private String plate;
    @NotBlank(groups = {CreateVehicle.class, UpdateVehicle.class})
    @Size(max = 50, groups = {CreateVehicle.class, UpdateVehicle.class})
    @JsonProperty("modelo")
    private String model;
    @NotBlank(groups = {CreateVehicle.class, UpdateVehicle.class})
    @Size(max = 50, groups = {CreateVehicle.class, UpdateVehicle.class})
    @JsonProperty("numeroChasis")
    private String chassisNumber;
    @NotBlank(groups = {CreateVehicle.class, UpdateVehicle.class})
    @Size(max = 25, groups = {CreateVehicle.class, UpdateVehicle.class})
    @JsonProperty("tipo")
    private String type;
    @NotNull(groups = {CreateVehicle.class, UpdateVehicle.class})
    @DecimalMin(value = "0.1", groups = {CreateVehicle.class, UpdateVehicle.class})
    @DecimalMax(value = "20.0", groups = {CreateVehicle.class, UpdateVehicle.class})
    @JsonProperty("cilindraje")
    private BigDecimal cylinderCapacity;
    @NotNull(groups = {CreateVehicle.class, UpdateVehicle.class})
    @Min(value = 1, groups = {CreateVehicle.class, UpdateVehicle.class})
    @Max(value = 100_000_000, groups = {CreateVehicle.class, UpdateVehicle.class})
    @JsonProperty("avaluo")
    private Integer appraisal;
    @NotNull(groups = {CreateVehicle.class, UpdateVehicle.class})
    @JsonProperty("marcaDto")
    private BrandDto brand;
}