package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.service.dto.CustomerAssignmentDto;

public interface CustomerAssignmentService extends CrudService<Integer, CustomerAssignmentDto, CustomerAssignmentDto> {
}
