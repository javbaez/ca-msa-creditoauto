package com.bancopichincha.credito.automotriz.service.dto;

import com.bancopichincha.credito.automotriz.domain.enums.LoanStatus;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.CreateLoan;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.UpdateLoan;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.*;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

@Data
public class LoanApplicationDto {
    @NotNull(groups = {UpdateLoan.class})
    @Min(value = 1, groups = {UpdateLoan.class})
    @JsonProperty("id")
    private Integer id;
    @NotNull(groups = {CreateLoan.class, UpdateLoan.class})
    @JsonProperty("clienteDto")
    private CustomerDto customer;
    @NotNull(groups = {CreateLoan.class, UpdateLoan.class})
    @JsonProperty("patioDto")
    private CarDealershipDto carDealership;
    @NotNull(groups = {CreateLoan.class, UpdateLoan.class})
    @JsonProperty("ejecutivoDto")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private SalesExecutiveDto salesExecutive;
    @NotNull(groups = {CreateLoan.class, UpdateLoan.class})
    @JsonProperty("vehiculoDto")
    private VehicleDto vehicle;
    @NotNull(groups = {CreateLoan.class, UpdateLoan.class})
    @FutureOrPresent(groups = {CreateLoan.class, UpdateLoan.class})
    @JsonProperty("fechaElaboracion")
    private LocalDate date;
    @NotNull(groups = {CreateLoan.class, UpdateLoan.class})
    @Min(value = 3, groups = {CreateLoan.class, UpdateLoan.class})
    @Max(value = 60, groups = {CreateLoan.class, UpdateLoan.class})
    @JsonProperty("mesesPlazo")
    private Byte monthsTerm;
    @NotNull(groups = {CreateLoan.class, UpdateLoan.class})
    @Min(value = 3, groups = {CreateLoan.class, UpdateLoan.class})
    @Max(value = 240, groups = {CreateLoan.class, UpdateLoan.class})
    @JsonProperty("cuotas")
    private Short installments;
    @NotNull(groups = {CreateLoan.class, UpdateLoan.class})
    @DecimalMin(value = "500.00", groups = {CreateLoan.class, UpdateLoan.class})
    @JsonProperty("entrada")
    private BigDecimal downPayment;
    @Size(max = 250, groups = {CreateLoan.class, UpdateLoan.class})
    @JsonProperty("observacion")
    private String observation;
    @NotNull(groups = {CreateLoan.class, UpdateLoan.class})
    @JsonProperty("estadoSolicitudCredito")
    private LoanStatus status;
    @JsonProperty("ejecutivosDto")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<SalesExecutiveDto> salesExecutives;
}
