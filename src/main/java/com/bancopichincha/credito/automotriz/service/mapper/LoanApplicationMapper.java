package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.LoanApplication;
import com.bancopichincha.credito.automotriz.service.dto.LoanApplicationDto;
import org.mapstruct.*;

@Mapper(
    componentModel = "spring",
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE
)
public interface LoanApplicationMapper {
    /**
     * Maps loan application entity to loan application DTO.
     * @param loanApplication Loan application entity.
     * @return Loan application DTO.
     */
    @Mapping(target = "salesExecutives", ignore = true)
    LoanApplicationDto loanApplicationToLoanApplicationDto(LoanApplication loanApplication);

    /**
     * Maps loan application DTO to loan application entity.
     * @param loanApplicationDto Loan application DTO.
     * @return Loan application entity.
     */
    LoanApplication loanApplicationDtoToLoanApplication(LoanApplicationDto loanApplicationDto);

    /**
     * Updates loan application entity from loan application DTO.
     * @param loanApplication Loan application entity.
     * @param loanApplicationDto Loan application DTO.
     */
    @Mapping(target = "customer", ignore = true)
    @Mapping(target = "carDealership", ignore = true)
    @Mapping(target = "salesExecutive", ignore = true)
    @Mapping(target = "vehicle", ignore = true)
    void updateLoanApplicationFromLoanApplicationDto(
        @MappingTarget LoanApplication loanApplication,
        LoanApplicationDto loanApplicationDto
    );
}
