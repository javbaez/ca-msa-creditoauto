package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.CustomerAssignment;
import com.bancopichincha.credito.automotriz.service.dto.CustomerAssignmentDto;
import org.mapstruct.*;

@Mapper(
    componentModel = "spring",
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE
)
public interface CustomerAssignmentMapper {
    /**
     * Maps customer assignment entity to customer assignment DTO.
     * @param customerAssignment Customer assignment entity.
     * @return Customer assignment DTO.
     */
    CustomerAssignmentDto customerAssignmentToCustomerAssignmentDto(CustomerAssignment customerAssignment);

    /**
     * Maps customer assignment DTO to vehicle customer assignment entity.
     * @param customerAssignmentDto Customer assignment DTO.
     * @return Customer assignment entity.
     */
    CustomerAssignment customerAssignmentDtoToCustomerAssignment(CustomerAssignmentDto customerAssignmentDto);

    /**
     * Updates customer assignment entity from customer assignment DTO.
     * @param customerAssignment Customer assignment entity.
     * @param customerAssignmentDto Customer assignment DTO.
     */
    @Mapping(target = "customer", ignore = true)
    @Mapping(target = "carDealership", ignore = true)
    void updateCustomerAssignmentFromCustomerAssignmentDto(
        @MappingTarget CustomerAssignment customerAssignment,
        CustomerAssignmentDto customerAssignmentDto
    );
}
