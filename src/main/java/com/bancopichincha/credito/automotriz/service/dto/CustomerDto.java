package com.bancopichincha.credito.automotriz.service.dto;

import com.bancopichincha.credito.automotriz.domain.enums.CivilStatus;
import com.bancopichincha.credito.automotriz.service.dto.validation.annotation.Age;
import com.bancopichincha.credito.automotriz.service.dto.validation.annotation.IdNumber;
import com.bancopichincha.credito.automotriz.service.dto.validation.annotation.Phone;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.*;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CustomerDto {
    @NotNull(groups = {Update.class, CreateAssignment.class, UpdateAssignment.class, CreateLoan.class, UpdateLoan.class})
    @Min(value = 1, groups = {Update.class, CreateAssignment.class, UpdateAssignment.class, CreateLoan.class, UpdateLoan.class})
    @JsonProperty("id")
    private Integer id;
    @NotBlank(groups = {Create.class, Update.class})
    @IdNumber(groups = {Create.class, Update.class})
    @JsonProperty("identificacion")
    private String idNumber;
    @NotBlank(groups = {Create.class, Update.class})
    @Size(max = 100, groups = {Create.class, Update.class})
    @JsonProperty("nombres")
    private String names;
    @NotBlank(groups = {Create.class, Update.class})
    @Size(max = 100, groups = {Create.class, Update.class})
    @JsonProperty("apellidos")
    private String lastNames;
    @NotNull(groups = {Create.class, Update.class})
    @Age(groups = {Create.class, Update.class})
    @JsonProperty("edad")
    private Byte age;
    @NotNull(groups = {Create.class, Update.class})
    @Past(groups = {Create.class, Update.class})
    @JsonProperty("fechaNacimiento")
    private LocalDate birthday;
    @NotBlank(groups = {Create.class, Update.class})
    @Size(max = 200, groups = {Create.class, Update.class})
    @JsonProperty("direccion")
    private String address;
    @NotBlank(groups = {Create.class, Update.class})
    @Phone(groups = {Create.class, Update.class})
    @JsonProperty("telefono")
    private String phone;
    @NotNull(groups = {Create.class, Update.class})
    @JsonProperty("estadoCivil")
    private CivilStatus civilStatus;
    @IdNumber(groups = {Create.class, Update.class})
    @JsonProperty("identificacionConyugue")
    private String spouseIdNumber;
    @Size(max = 200, groups = {Create.class, Update.class})
    @JsonProperty("nombreConyugue")
    private String spouseNames;
    @NotNull(groups = {Create.class, Update.class})
    @JsonProperty("sujetoCredito")
    private Boolean creditSubject;

    @AssertTrue(message = "Spouse data is incorrect")
    @JsonIgnore
    public boolean isSpouseDataCorrect() {
        if (civilStatus == CivilStatus.MARRIED) {
            return spouseIdNumber != null && spouseNames != null;
        }
        return spouseIdNumber == null && spouseNames == null;
    }
}
