package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.Vehicle;
import com.bancopichincha.credito.automotriz.service.dto.VehicleDto;
import org.mapstruct.*;

@Mapper(
    componentModel = "spring",
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE
)
public interface VehicleMapper {
    /**
     * Maps vehicle entity to vehicle DTO.
     * @param vehicle Vehicle entity.
     * @return Vehicle DTO.
     */
    VehicleDto vehicleToVehicleDto(Vehicle vehicle);

    /**
     * Maps vehicle DTO to vehicle entity.
     * @param vehicleDto Vehicle DTO.
     * @return Vehicle entity.
     */
    Vehicle vehicleDtoToVehicle(VehicleDto vehicleDto);

    /**
     * Updates vehicle entity from vehicle DTO.
     * @param vehicle Vehicle entity.
     * @param vehicleDto Vehicle DTO.
     */
    @Mapping(target = "brand", ignore = true)
    void updateVehicleFromVehicleDto(@MappingTarget Vehicle vehicle, VehicleDto vehicleDto);
}
