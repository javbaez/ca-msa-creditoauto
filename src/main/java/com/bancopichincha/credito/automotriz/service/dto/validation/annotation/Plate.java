package com.bancopichincha.credito.automotriz.service.dto.validation.annotation;

import com.bancopichincha.credito.automotriz.service.dto.validation.validator.PlateValidator;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = PlateValidator.class)
public @interface Plate {
    String message() default "must be a valid plate e.g. ABC-1234 or AB123C";

    Class<?>[] groups() default { };

    Class<? extends Payload>[] payload() default { };
}
