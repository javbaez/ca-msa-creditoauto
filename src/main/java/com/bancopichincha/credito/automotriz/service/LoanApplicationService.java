package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.service.dto.LoanApplicationDto;

/**
 * Loan application service interface.
 * @author Javier Báez
 * @version 1.0.0
 */
public interface LoanApplicationService extends CrudService<Integer, LoanApplicationDto, LoanApplicationDto> {
    /**
     * Creates a loan application.
     * @param loanApplicationDto Loan application DTO.
     * @return Loan application DTO.
     */
    LoanApplicationDto create(LoanApplicationDto loanApplicationDto);
}
