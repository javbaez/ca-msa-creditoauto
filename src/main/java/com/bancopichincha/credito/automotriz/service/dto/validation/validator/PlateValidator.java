package com.bancopichincha.credito.automotriz.service.dto.validation.validator;

import com.bancopichincha.credito.automotriz.service.dto.validation.annotation.Plate;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.regex.Pattern;

public class PlateValidator implements ConstraintValidator<Plate, String> {
    private static final String PLATE_REGEX = "^([A-Z]{3}\\-\\d{4})|([A-Z]{2}\\d{3}[A-Z])$";

    @Override
    public void initialize(final Plate annotation) {
    }

    @Override
    public boolean isValid(final String value, final ConstraintValidatorContext context) {
        return value == null || Pattern.matches(PLATE_REGEX, value);
    }
}
