package com.bancopichincha.credito.automotriz.service.dto;

import com.bancopichincha.credito.automotriz.service.dto.validation.group.*;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class BrandDto {
    @NotNull(groups = {Update.class, Find.class, CreateVehicle.class, UpdateVehicle.class, CreateLoan.class, UpdateLoan.class})
    @Min(value = 1, groups = {Update.class, Find.class, CreateVehicle.class, UpdateVehicle.class, CreateLoan.class, UpdateLoan.class})
    @JsonProperty("id")
    private Integer id;
    @NotBlank(groups = {Create.class, Update.class})
    @Size(max = 50, groups = {Create.class, Update.class})
    @JsonProperty("nombre")
    private String name;
}
