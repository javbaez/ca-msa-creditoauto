package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.service.dto.VehicleDto;

import java.util.List;

public interface VehicleService extends CrudService<Integer, VehicleDto, VehicleDto> {
    /**
     * Finds vehicles by model.
     * @param model Vehicle model.
     * @return List of vehicles.
     */
    List<VehicleDto> findByModel(String model);

    /**
     * Finds vehicles by brand ID.
     * @param brandId Brand ID.
     * @return List of vehicles.
     */
    List<VehicleDto> findByBrandId(final Integer brandId);
}
