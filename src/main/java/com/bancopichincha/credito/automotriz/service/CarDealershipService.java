package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.service.dto.CarDealershipDto;

public interface CarDealershipService extends CrudService<Integer, CarDealershipDto, CarDealershipDto> {
}
