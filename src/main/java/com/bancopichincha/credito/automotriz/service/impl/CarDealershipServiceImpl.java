package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.domain.CarDealership;
import com.bancopichincha.credito.automotriz.exception.CarDealershipNotFoundException;
import com.bancopichincha.credito.automotriz.exception.CarDealershipReferenceToCustomerAssignmentException;
import com.bancopichincha.credito.automotriz.exception.CarDealershipReferenceToSalesExecutiveException;
import com.bancopichincha.credito.automotriz.repository.CarDealershipRepository;
import com.bancopichincha.credito.automotriz.repository.CustomerAssignmentRepository;
import com.bancopichincha.credito.automotriz.repository.SalesExecutiveRepository;
import com.bancopichincha.credito.automotriz.service.CarDealershipService;
import com.bancopichincha.credito.automotriz.service.dto.CarDealershipDto;
import com.bancopichincha.credito.automotriz.service.mapper.CarDealershipMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CarDealershipServiceImpl implements CarDealershipService {
    private final CarDealershipMapper carDealershipMapper;
    private final CarDealershipRepository carDealershipRepository;
    private final SalesExecutiveRepository salesExecutiveRepository;
    private final CustomerAssignmentRepository customerAssignmentRepository;

    /**
     * Finds car dealership entity by ID.
     * @param id Car dealership ID.
     * @return Car dealership entity.
     * @throws CarDealershipNotFoundException If car dealership entity is not found.
     * @see CarDealershipNotFoundException
     */
    protected CarDealership findCarDealershipById(final Integer id) throws CarDealershipNotFoundException {
        return carDealershipRepository.findById(id).orElseThrow(() -> new CarDealershipNotFoundException(id));
    }

    @Override
    public CarDealershipDto findById(final Integer id) throws CarDealershipNotFoundException {
        return carDealershipMapper.carDealershipToCarDealershipDto(findCarDealershipById(id));
    }

    @Override
    public CarDealershipDto create(final CarDealershipDto carDealershipDto) {
        final CarDealership carDealership = carDealershipMapper.carDealershipDtoToCarDealership(carDealershipDto);
        return carDealershipMapper.carDealershipToCarDealershipDto(carDealershipRepository.save(carDealership));
    }

    @Override
    public CarDealershipDto update(final CarDealershipDto carDealershipDto) throws CarDealershipNotFoundException {
        final CarDealership carDealership = findCarDealershipById(carDealershipDto.getId());
        carDealershipMapper.updateCarDealershipFromCarDealershipDto(carDealership, carDealershipDto);
        return carDealershipMapper.carDealershipToCarDealershipDto(carDealershipRepository.save(carDealership));
    }

    @Override
    public CarDealershipDto delete(final Integer id) throws
        CarDealershipNotFoundException,
        CarDealershipReferenceToSalesExecutiveException,
        CarDealershipReferenceToCustomerAssignmentException {
        final CarDealership carDealership = findCarDealershipById(id);
        if (salesExecutiveRepository.checkIfExistsByCarDealershipId(id)) {
            throw new CarDealershipReferenceToSalesExecutiveException(id);
        }
        if (customerAssignmentRepository.checkIfCarDealershipHasAssignments(id)) {
            throw new CarDealershipReferenceToCustomerAssignmentException(id);
        }
        carDealershipRepository.deleteById(id);
        return carDealershipMapper.carDealershipToCarDealershipDto(carDealership);
    }
}
