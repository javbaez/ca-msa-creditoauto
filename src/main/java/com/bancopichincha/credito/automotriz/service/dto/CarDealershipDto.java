package com.bancopichincha.credito.automotriz.service.dto;

import com.bancopichincha.credito.automotriz.service.dto.validation.annotation.Phone;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.Create;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.CreateLoan;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.Update;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.UpdateLoan;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.*;
import lombok.Data;

@Data
public class CarDealershipDto {
    @NotNull(groups = {Update.class, CreateLoan.class, UpdateLoan.class})
    @Min(value = 1, groups = {Update.class, CreateLoan.class, UpdateLoan.class})
    @JsonProperty("id")
    private Integer id;
    @NotBlank(groups = {Create.class, Update.class})
    @Size(max = 100, groups = {Create.class, Update.class})
    @JsonProperty("nombre")
    private String name;
    @NotBlank(groups = {Create.class, Update.class})
    @Size(max = 255, groups = {Create.class, Update.class})
    @JsonProperty("direccion")
    private String address;
    @NotBlank(groups = {Create.class, Update.class})
    @Phone(groups = {Create.class, Update.class})
    @JsonProperty("telefono")
    private String phone;
    @NotNull(groups = {Create.class, Update.class})
    @Min(value = 1, groups = {Create.class, Update.class})
    @Max(value = 1_000_000, groups = {Create.class, Update.class})
    @JsonProperty("numeroPuntoVenta")
    private Integer pointOfSaleNumber;
}
