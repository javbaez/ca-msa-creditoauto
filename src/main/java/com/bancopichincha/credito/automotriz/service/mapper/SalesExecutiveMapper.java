package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.SalesExecutive;
import com.bancopichincha.credito.automotriz.service.dto.SalesExecutiveDto;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(
    componentModel = "spring",
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE
)
public interface SalesExecutiveMapper {
    /**
     * Maps sales executive entity to sales executive DTO.
     * @param salesExecutive Sales executive entity.
     * @return Sales executive DTO.
     */
    SalesExecutiveDto salesExecutiveToSalesExecutiveDto(SalesExecutive salesExecutive);
}
