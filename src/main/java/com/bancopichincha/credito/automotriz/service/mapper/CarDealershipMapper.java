package com.bancopichincha.credito.automotriz.service.mapper;

import com.bancopichincha.credito.automotriz.domain.CarDealership;
import com.bancopichincha.credito.automotriz.service.dto.CarDealershipDto;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValueCheckStrategy;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(
    componentModel = "spring",
    nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
    nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE
)
public interface CarDealershipMapper {
    /**
     * Maps car dealership entity to car dealership DTO.
     * @param carDealership Car dealership entity.
     * @return Car dealership DTO.
     */
    CarDealershipDto carDealershipToCarDealershipDto(CarDealership carDealership);

    /**
     * Maps car dealership DTO to car dealership entity.
     * @param carDealershipDto Car dealership DTO.
     * @return Car dealership entity.
     */
    CarDealership carDealershipDtoToCarDealership(CarDealershipDto carDealershipDto);

    /**
     * Updates car dealership entity from car dealership DTO.
     * @param carDealership Car dealership entity.
     * @param carDealershipDto Car dealership DTO.
     */
    void updateCarDealershipFromCarDealershipDto(@MappingTarget CarDealership carDealership, CarDealershipDto carDealershipDto);
}
