package com.bancopichincha.credito.automotriz.service;

import com.bancopichincha.credito.automotriz.exception.ResourceNotFoundException;

/**
 * CRUD service interface.
 * @param <ID> Key type.
 * @param <DTO> DTO type.
 * @param <RDTO> Response DTO type.
 */
public interface CrudService<ID, DTO, RDTO> {
    /**
     * Finds data by ID.
     * @param id ID.
     * @return Response DTO.
     * @throws ResourceNotFoundException If entity is not found.
     */
    RDTO findById(ID id);

    /**
     * Creates data.
     * @param dto DTO.
     * @return Response DTO.
     */
    RDTO create(DTO dto);

    /**
     * Updates data.
     * @param dto DTO.
     * @return Response DTO.
     * @throws ResourceNotFoundException If entity is not found.
     */
    RDTO update(DTO dto);

    /**
     * Deletes data.
     * @param id ID.
     * @return Response DTO.
     * @throws ResourceNotFoundException If entity is not found.
     */
    RDTO delete(ID id);
}
