package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.domain.*;
import com.bancopichincha.credito.automotriz.exception.*;
import com.bancopichincha.credito.automotriz.repository.*;
import com.bancopichincha.credito.automotriz.service.LoanApplicationService;
import com.bancopichincha.credito.automotriz.service.dto.LoanApplicationDto;
import com.bancopichincha.credito.automotriz.service.dto.SalesExecutiveDto;
import com.bancopichincha.credito.automotriz.service.mapper.LoanApplicationMapper;
import com.bancopichincha.credito.automotriz.service.mapper.SalesExecutiveMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class LoanApplicationServiceImpl implements LoanApplicationService {
    private final LoanApplicationMapper loanApplicationMapper;
    private final SalesExecutiveMapper salesExecutiveMapper;
    private final LoanApplicationRepository loanApplicationRepository;
    private final CustomerRepository customerRepository;
    private final CarDealershipRepository carDealershipRepository;
    private final SalesExecutiveRepository salesExecutiveRepository;
    private final VehicleRepository vehicleRepository;

    /**
     * Finds loan application entity by ID.
     * @param id Loan application ID.
     * @return Loan application entity.
     * @throws LoanApplicationNotFoundException If loan application entity is not found.
     * @see LoanApplicationNotFoundException
     */
    protected LoanApplication findLoanApplicationById(final Integer id) throws LoanApplicationNotFoundException {
        return loanApplicationRepository.findById(id).orElseThrow(() -> new LoanApplicationNotFoundException(id));
    }

    /**
     * Finds sales executives by car dealership.
     * @param carDealership Car dealership.
     * @return Lost of sales executives.
     */
    protected List<SalesExecutiveDto> findSalesExecutivesByCarDealership(final CarDealership carDealership) {
        return salesExecutiveRepository
            .findByCarDealership(carDealership)
            .stream()
            .map(salesExecutiveMapper::salesExecutiveToSalesExecutiveDto)
            .collect(Collectors.toList());
    }

    @Override
    public LoanApplicationDto findById(final Integer id) throws LoanApplicationNotFoundException {
        final LoanApplication loanApplication = findLoanApplicationById(id);
        final LoanApplicationDto loanApplicationDto = loanApplicationMapper
            .loanApplicationToLoanApplicationDto(loanApplication);
        loanApplicationDto.setSalesExecutives(findSalesExecutivesByCarDealership(loanApplication.getCarDealership()));
        return loanApplicationDto;
    }

    @Override
    public LoanApplicationDto create(final LoanApplicationDto loanApplicationDto) throws
        CustomerNotFoundException, LoanApplicationExistsException, VehicleNotFoundException,
        VehicleReservedForAnotherLoanException, CarDealershipNotFoundException, SalesExecutiveNotFoundException {
        final Customer customer = customerRepository
            .findById(loanApplicationDto.getCustomer().getId())
            .orElseThrow(() -> new CustomerNotFoundException(loanApplicationDto.getCustomer().getId()));
        if (loanApplicationRepository.checkIfCustomerHasValidApplicationByDate(customer.getId(), loanApplicationDto.getDate())) {
            throw new LoanApplicationExistsException();
        }
        final Vehicle vehicle = vehicleRepository
            .findById(loanApplicationDto.getVehicle().getId())
            .orElseThrow(() -> new VehicleNotFoundException(loanApplicationDto.getVehicle().getId()));
        if (loanApplicationRepository.checkIfVehicleIsReserved(loanApplicationDto.getVehicle().getId())) {
            throw new VehicleReservedForAnotherLoanException();
        }
        final CarDealership carDealership = carDealershipRepository
            .findById(loanApplicationDto.getCarDealership().getId())
            .orElseThrow(() -> new CarDealershipNotFoundException(loanApplicationDto.getCarDealership().getId()));
        final SalesExecutive salesExecutive = salesExecutiveRepository.findByIdAndCarDealership(
            loanApplicationDto.getSalesExecutive().getId(),
            carDealership
        ).orElseThrow(() -> new SalesExecutiveNotFoundException(loanApplicationDto.getSalesExecutive().getId()));
        final LoanApplication loanApplication = loanApplicationMapper.loanApplicationDtoToLoanApplication(loanApplicationDto);
        loanApplication.setCustomer(customer);
        loanApplication.setCarDealership(carDealership);
        loanApplication.setSalesExecutive(salesExecutive);
        loanApplication.setVehicle(vehicle);
        final LoanApplicationDto result = loanApplicationMapper.loanApplicationToLoanApplicationDto(loanApplicationRepository.save(loanApplication));
        result.setSalesExecutives(findSalesExecutivesByCarDealership(carDealership));
        return result;
    }

    @Override
    public LoanApplicationDto update(final LoanApplicationDto loanApplicationDto) throws
        LoanApplicationNotFoundException, CustomerNotFoundException, VehicleReservedForAnotherLoanException,
        VehicleNotFoundException, CarDealershipNotFoundException, SalesExecutiveNotFoundException {
        final LoanApplication loanApplication = findLoanApplicationById(loanApplicationDto.getId());
        if (!Objects.equals(loanApplication.getCustomer().getId(), loanApplicationDto.getCustomer().getId())) {
            loanApplication.setCustomer(
                customerRepository
                    .findById(loanApplicationDto.getCustomer().getId())
                    .orElseThrow(() -> new CustomerNotFoundException(loanApplicationDto.getCustomer().getId()))
            );
        }
        if (!Objects.equals(loanApplication.getVehicle().getId(), loanApplicationDto.getVehicle().getId())) {
            if (loanApplicationRepository.checkIfVehicleIsReserved(loanApplicationDto.getVehicle().getId())) {
                throw new VehicleReservedForAnotherLoanException();
            }
            loanApplication.setVehicle(
                vehicleRepository
                    .findById(loanApplicationDto.getVehicle().getId())
                    .orElseThrow(() -> new VehicleNotFoundException(loanApplicationDto.getVehicle().getId()))
            );
        }
        if (!Objects.equals(loanApplication.getCarDealership().getId(), loanApplicationDto.getCarDealership().getId())) {
            loanApplication.setCarDealership(
                carDealershipRepository
                    .findById(loanApplicationDto.getCarDealership().getId())
                    .orElseThrow(() -> new CarDealershipNotFoundException(loanApplicationDto.getCarDealership().getId()))
            );
        }
        if (!Objects.equals(loanApplication.getSalesExecutive().getId(), loanApplicationDto.getSalesExecutive().getId())) {
            loanApplication.setSalesExecutive(
                salesExecutiveRepository.findByIdAndCarDealership(
                    loanApplicationDto.getSalesExecutive().getId(),
                    loanApplication.getCarDealership()
                ).orElseThrow(() -> new SalesExecutiveNotFoundException(loanApplicationDto.getSalesExecutive().getId()))
            );
        }
        loanApplicationMapper.updateLoanApplicationFromLoanApplicationDto(loanApplication, loanApplicationDto);
        final LoanApplicationDto result = loanApplicationMapper
            .loanApplicationToLoanApplicationDto(loanApplicationRepository.save(loanApplication));
        result.setSalesExecutives(findSalesExecutivesByCarDealership(loanApplication.getCarDealership()));
        return result;
    }

    @Override
    public LoanApplicationDto delete(final Integer id) throws LoanApplicationNotFoundException {
        final LoanApplication loanApplication = findLoanApplicationById(id);
        loanApplicationRepository.deleteById(id);
        final LoanApplicationDto loanApplicationDto = loanApplicationMapper
            .loanApplicationToLoanApplicationDto(loanApplication);
        loanApplicationDto.setSalesExecutives(findSalesExecutivesByCarDealership(loanApplication.getCarDealership()));
        return loanApplicationDto;
    }
}
