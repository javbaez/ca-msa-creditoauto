package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.domain.Brand;
import com.bancopichincha.credito.automotriz.domain.Vehicle;
import com.bancopichincha.credito.automotriz.exception.*;
import com.bancopichincha.credito.automotriz.repository.BrandRepository;
import com.bancopichincha.credito.automotriz.repository.LoanApplicationRepository;
import com.bancopichincha.credito.automotriz.repository.VehicleRepository;
import com.bancopichincha.credito.automotriz.service.VehicleService;
import com.bancopichincha.credito.automotriz.service.dto.VehicleDto;
import com.bancopichincha.credito.automotriz.service.mapper.VehicleMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class VehicleServiceImpl implements VehicleService {
    private final VehicleMapper vehicleMapper;
    private final VehicleRepository vehicleRepository;
    private final BrandRepository brandRepository;
    private final LoanApplicationRepository loanApplicationRepository;

    /**
     * Finds vehicle entity by ID.
     * @param id Vehicle ID.
     * @return Vehicle entity.
     * @throws VehicleNotFoundException If vehicle entity is not found.
     * @see VehicleNotFoundException
     */
    protected Vehicle findVehicleById(final Integer id) throws VehicleNotFoundException {
        return vehicleRepository.findById(id).orElseThrow(() -> new VehicleNotFoundException(id));
    }

    /**
     * Finds brand entity by ID.
     * @param id Brand ID.
     * @return Brand entity.
     * @throws BrandNotFoundException If brand entity is not found.
     * @see BrandNotFoundException
     */
    protected Brand findBrandById(final Integer id) throws BrandNotFoundException {
        return brandRepository
            .findById(id)
            .orElseThrow(() -> new BrandNotFoundException(id));
    }

    @Override
    public VehicleDto findById(final Integer id) throws VehicleNotFoundException {
        return vehicleMapper.vehicleToVehicleDto(findVehicleById(id));
    }

    @Override
    public List<VehicleDto> findByModel(final String model) {
        final List<Vehicle> vehicles = vehicleRepository.findByModel(model);
        return vehicles.stream().map(vehicleMapper::vehicleToVehicleDto).collect(Collectors.toList());
    }

    @Override
    public List<VehicleDto> findByBrandId(final Integer brandId) {
        final List<Vehicle> vehicles = vehicleRepository.findByBrandId(brandId);
        return vehicles.stream().map(vehicleMapper::vehicleToVehicleDto).collect(Collectors.toList());
    }

    @Override
    public VehicleDto create(final VehicleDto vehicleDto) throws BrandNotFoundException, VehiclePlateExistsException {
        final Brand brand = findBrandById(vehicleDto.getBrand().getId());
        vehicleRepository
            .findByPlate(vehicleDto.getPlate())
            .ifPresent(vehicleFound -> {
                throw new VehiclePlateExistsException(vehicleFound.getPlate());
            });
        final Vehicle vehicle = vehicleMapper.vehicleDtoToVehicle(vehicleDto);
        vehicle.setBrand(brand);
        return vehicleMapper.vehicleToVehicleDto(vehicleRepository.save(vehicle));
    }

    @Override
    public VehicleDto update(final VehicleDto vehicleDto) throws
        VehicleNotFoundException, BrandNotFoundException,
        VehiclePlateExistsException, VehicleReservedForLoanException {
        final Vehicle vehicle = findVehicleById(vehicleDto.getId());
        vehicleRepository
            .findByPlate(vehicleDto.getPlate())
            .ifPresent(vehicleFound -> {
                if (!vehicleFound.getId().equals(vehicleDto.getId())) {
                    throw new VehiclePlateExistsException(vehicleFound.getPlate());
                }
            });
        if (loanApplicationRepository.checkIfVehicleIsReserved(vehicleDto.getId())) {
            throw new VehicleReservedForLoanException();
        }
        if (!Objects.equals(vehicle.getBrand().getId(), vehicleDto.getBrand().getId())) {
            vehicle.setBrand(findBrandById(vehicleDto.getBrand().getId()));
        }
        vehicleMapper.updateVehicleFromVehicleDto(vehicle, vehicleDto);
        return vehicleMapper.vehicleToVehicleDto(vehicleRepository.save(vehicle));
    }

    @Override
    public VehicleDto delete(final Integer id) throws VehicleNotFoundException, VehicleLoanApplicationExistsException {
        final Vehicle vehicle = findVehicleById(id);
        if (loanApplicationRepository.checkIfVehicleIsReserved(id)) {
            throw new VehicleLoanApplicationExistsException();
        }
        vehicleRepository.deleteById(id);
        return vehicleMapper.vehicleToVehicleDto(vehicle);
    }
}
