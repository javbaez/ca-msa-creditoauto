package com.bancopichincha.credito.automotriz.service.impl;

import com.bancopichincha.credito.automotriz.domain.CarDealership;
import com.bancopichincha.credito.automotriz.domain.Customer;
import com.bancopichincha.credito.automotriz.domain.CustomerAssignment;
import com.bancopichincha.credito.automotriz.exception.CarDealershipNotFoundException;
import com.bancopichincha.credito.automotriz.exception.CustomerAssignmentNotFoundException;
import com.bancopichincha.credito.automotriz.exception.CustomerNotFoundException;
import com.bancopichincha.credito.automotriz.repository.CarDealershipRepository;
import com.bancopichincha.credito.automotriz.repository.CustomerAssignmentRepository;
import com.bancopichincha.credito.automotriz.repository.CustomerRepository;
import com.bancopichincha.credito.automotriz.service.CustomerAssignmentService;
import com.bancopichincha.credito.automotriz.service.dto.CustomerAssignmentDto;
import com.bancopichincha.credito.automotriz.service.mapper.CustomerAssignmentMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@RequiredArgsConstructor
public class CustomerAssignmentServiceImpl implements CustomerAssignmentService {
    private final CustomerAssignmentMapper customerAssignmentMapper;
    private final CustomerAssignmentRepository customerAssignmentRepository;
    private final CustomerRepository customerRepository;
    private final CarDealershipRepository carDealershipRepository;

    /**
     * Finds customer assignment entity by ID.
     * @param id Customer assignment ID.
     * @return Customer assignment entity.
     * @throws CustomerAssignmentNotFoundException If customer assignment entity is not found.
     * @see CustomerAssignmentNotFoundException
     */
    protected CustomerAssignment findCustomerAssignmentById(final Integer id) throws CustomerAssignmentNotFoundException {
        return customerAssignmentRepository
            .findById(id)
            .orElseThrow(() -> new CustomerAssignmentNotFoundException(id));
    }

    /**
     * Finds customer by ID.
     * @param id Customer ID.
     * @return Customer entity.
     * @throws CustomerNotFoundException If customer entity is not found.
     * @see CustomerNotFoundException
     */
    protected Customer findCustomerById(final Integer id) throws CustomerNotFoundException {
        return customerRepository
            .findById(id)
            .orElseThrow(() -> new CustomerNotFoundException(id));
    }

    /**
     * Finds car dealership by ID.
     * @param id Car dealership ID.
     * @return Car dealership entity.
     * @throws CarDealershipNotFoundException If car dealership entity is not found.
     * @see CarDealershipNotFoundException
     */
    protected CarDealership findCarDealershipById(final Integer id) throws CarDealershipNotFoundException {
        return carDealershipRepository
            .findById(id)
            .orElseThrow(() -> new CarDealershipNotFoundException(id));
    }

    @Override
    public CustomerAssignmentDto findById(final Integer id) throws CustomerAssignmentNotFoundException {
        return customerAssignmentMapper.customerAssignmentToCustomerAssignmentDto(findCustomerAssignmentById(id));
    }

    @Override
    public CustomerAssignmentDto create(final CustomerAssignmentDto customerAssignmentDto) throws
        CustomerNotFoundException, CarDealershipNotFoundException {
        final Customer customer = findCustomerById(customerAssignmentDto.getCustomer().getId());
        final CarDealership carDealership = findCarDealershipById(customerAssignmentDto.getCarDealership().getId());
        final CustomerAssignment customerAssignment = customerAssignmentMapper
            .customerAssignmentDtoToCustomerAssignment(customerAssignmentDto);
        customerAssignment.setCustomer(customer);
        customerAssignment.setCarDealership(carDealership);
        customerAssignmentRepository.save(customerAssignment);
        return customerAssignmentMapper.customerAssignmentToCustomerAssignmentDto(customerAssignment);
    }

    @Override
    public CustomerAssignmentDto update(final CustomerAssignmentDto customerAssignmentDto) throws
        CustomerAssignmentNotFoundException, CustomerNotFoundException, CarDealershipNotFoundException {
        final CustomerAssignment customerAssignment = findCustomerAssignmentById(customerAssignmentDto.getId());
        if (!Objects.equals(customerAssignment.getCustomer().getId(), customerAssignmentDto.getCustomer().getId())) {
            customerAssignment.setCustomer(findCustomerById(customerAssignmentDto.getCustomer().getId()));
        }
        if (!Objects.equals(customerAssignment.getCarDealership().getId(), customerAssignmentDto.getCarDealership().getId())) {
            customerAssignment.setCarDealership(findCarDealershipById(customerAssignmentDto.getCarDealership().getId()));
        }
        customerAssignmentMapper.updateCustomerAssignmentFromCustomerAssignmentDto(customerAssignment, customerAssignmentDto);
        return customerAssignmentMapper
            .customerAssignmentToCustomerAssignmentDto(customerAssignmentRepository.save(customerAssignment));
    }

    @Override
    public CustomerAssignmentDto delete(final Integer id) throws CustomerAssignmentNotFoundException {
        final CustomerAssignment customerAssignment = findCustomerAssignmentById(id);
        customerAssignmentRepository.deleteById(id);
        return customerAssignmentMapper.customerAssignmentToCustomerAssignmentDto(customerAssignment);
    }
}
