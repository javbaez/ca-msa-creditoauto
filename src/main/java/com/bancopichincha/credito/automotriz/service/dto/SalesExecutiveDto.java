package com.bancopichincha.credito.automotriz.service.dto;

import com.bancopichincha.credito.automotriz.service.dto.validation.annotation.Age;
import com.bancopichincha.credito.automotriz.service.dto.validation.annotation.IdNumber;
import com.bancopichincha.credito.automotriz.service.dto.validation.annotation.Phone;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.Create;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.CreateLoan;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.Update;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.UpdateLoan;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class SalesExecutiveDto {
    @NotNull(groups = {Update.class, CreateLoan.class, UpdateLoan.class})
    @Min(value = 1, groups = {Update.class, CreateLoan.class, UpdateLoan.class})
    @JsonProperty("id")
    private Integer id;
    @NotBlank(groups = {Create.class, Update.class})
    @IdNumber(groups = {Create.class, Update.class})
    @JsonProperty("identificacion")
    private String idNumber;
    @NotBlank(groups = {Create.class, Update.class})
    @Size(max = 100, groups = {Create.class, Update.class})
    @JsonProperty("nombres")
    private String names;
    @NotBlank(groups = {Create.class, Update.class})
    @Size(max = 100, groups = {Create.class, Update.class})
    @JsonProperty("apellidos")
    private String lastNames;
    @NotNull(groups = {Create.class, Update.class})
    @Age(groups = {Create.class, Update.class})
    @JsonProperty("edad")
    private Byte age;
    @NotBlank(groups = {Create.class, Update.class})
    @Size(max = 200, groups = {Create.class, Update.class})
    @JsonProperty("direccion")
    private String address;
    @NotBlank(groups = {Create.class, Update.class})
    @Phone(max = 9, groups = {Create.class, Update.class})
    @JsonProperty("telefono")
    private String landline;
    @NotBlank(groups = {Create.class, Update.class})
    @Phone(min = 10, groups = {Create.class, Update.class})
    @JsonProperty("celular")
    private String mobile;
    @NotNull(groups = {Create.class, Update.class})
    @Min(value = 1, groups = {Create.class, Update.class})
    @JsonProperty("patio_id")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Integer carDealershipId;
    @JsonProperty("patioDto")
    private CarDealershipDto carDealership;
}
