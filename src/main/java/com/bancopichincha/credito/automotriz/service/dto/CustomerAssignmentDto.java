package com.bancopichincha.credito.automotriz.service.dto;

import com.bancopichincha.credito.automotriz.service.dto.validation.group.CreateAssignment;
import com.bancopichincha.credito.automotriz.service.dto.validation.group.UpdateAssignment;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PastOrPresent;
import lombok.Data;

import java.time.LocalDate;

@Data
public class CustomerAssignmentDto {
    @NotNull(groups = {UpdateAssignment.class})
    @Min(value = 1, groups = {UpdateAssignment.class})
    @JsonProperty("id")
    private Integer id;
    @NotNull(groups = {CreateAssignment.class, UpdateAssignment.class})
    @JsonProperty("clienteDto")
    private CustomerDto customer;
    @NotNull(groups = {CreateAssignment.class, UpdateAssignment.class})
    @JsonProperty("patioDto")
    private CarDealershipDto carDealership;
    @NotNull(groups = {CreateAssignment.class, UpdateAssignment.class})
    @PastOrPresent(groups = {CreateAssignment.class, UpdateAssignment.class})
    @JsonProperty("fechaAsignacion")
    private LocalDate assignmentDate;
}
